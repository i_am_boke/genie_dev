(function(angular) { 
	'use strict';
	var genieApp = angular.module('genieApp', ['ionic', 'LocalStorageModule', 'ionic-datepicker', 'ngCordova', 'chart.js', 'genie.controllers', 
	                                           'ui.bootstrap','ui.rCalendar','ngMessages','ngTagsInput','ngTouch','ngProgress']);
	genieApp.run(function($ionicPlatform, $ionicPopup, $cordovaSplashscreen, $timeout, $cordovaNetwork) {
	  $ionicPlatform.ready(function(device) {
	    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
	    // for form inputs)
	    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
	      	cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
	      	cordova.plugins.Keyboard.disableScroll(true);
	    }
	    if(!ionic.Platform.isIOS() && !ionic.Platform.isAndroid()){
	    	$('.platform-browser').addClass('platform-desktop')
	    }
	    if(window.cordova){
	    	navigator.splashscreen.hide();
	    }
	    if (window.StatusBar) {
	      // org.apache.cordova.statusbar required
	      StatusBar.styleDefault();
	      StatusBar.overlaysWebView(false);
	      StatusBar.backgroundColorByHexString('#924972')
	      StatusBar.styleLightContent();
	    }
	    var network = false;
	    if(window.cordova){
	    	var network = $cordovaNetwork.getNetwork()
	    	if(network!='none'){
	    		window.userOnline = true
	    		network = true
	    	}
	    }else if(!window.cordova && window.userOnline){
		    network = true
	    }
	    if(!network){
	    	window.userOnline = false
	    }
	});
});
genieApp.config(['$stateProvider','$urlRouterProvider','localStorageServiceProvider','ChartJsProvider',
                 function($stateProvider, $urlRouterProvider, localStorageServiceProvider, ChartJsProvider) {
		// Local storage service config
		var filePath = ""
		if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
			filePath = cordova.file.applicationDirectory + 'www/'
		}
		localStorageServiceProvider.setPrefix('genieApp').setStorageType('localStorage').setNotify(true, true);
		ChartJsProvider.setOptions({ colours : [ '#ABD47F', '#E38994'] });
		$urlRouterProvider.otherwise('/login');
		$stateProvider
		.state('user-login', {
			url: '/login',
			cache: false,
			templateUrl: filePath + 'templates/login.html',
			controller: 'AuthenticationController'
		}).state('activity-stream', {
			url: '/activity-stream',
			templateUrl: filePath + 'templates/activity-stream.html',
			controller: 'ActivityStreamController',
			params:{
				refresh:false
			}
		}).state('activity-details', {
			url: '/activity-details',
			templateUrl: filePath + 'templates/activity-details.html',
			controller: 'ActivityDetailsController'
		}).state('user-calendar', {
			url: '/my-calendar',
			templateUrl: filePath + 'templates/user-calendar.html',
			controller: 'UserCalendarController',
		}).state('create-activity', {
			url: '/create-activity',
			templateUrl: filePath + 'templates/create-activity.html',
			controller: 'CreateActivityController',
		}).state('edit-activity', {
			url: '/edit-activity',
			templateUrl: filePath + 'templates/edit-activity.html',
			controller: 'EditActivityController',
		}).state('user-roles', {
			url: '/switch-roles',
			cache: false,
			templateUrl: filePath + 'templates/user-roles.html',
			controller: 'UserRolesController'
	    }).state('activity-read-users', {
			url: '/activity-read-users',
			templateUrl: filePath + 'templates/activity-read-users.html',
			controller: 'ActivityReadUsersController'
	    })
	}]);
	genieApp.factory('ApplicationFactory',['$rootScope', 'localStorageService', '$state', '$ionicLoading', '$ionicHistory', '$q', '$sce','$timeout', '$ionicPopup','$cordovaNetwork',
			function($rootScope, localStorageService, $state, $ionicLoading, $ionicHistory, $q, $sce, $timeout, $ionicPopup, $cordovaNetwork){
		return {
			setAppState : function(param){
				$rootScope.$broadcast('SetApplicationState', {'data':param});
				$rootScope.$broadcast('CheckApplicationFooter', {'data':param});
			},
			navigateCalendar: function(param){
				$rootScope.$broadcast('NavigateCalendar', {'direction':param});
			},
			showUserMessage: function(type, message, position, time){
				var alertPopup = $ionicPopup.alert({
			    	template: message
				});
				$timeout(function() {
					alertPopup.close()
				}, time)
			},
			showFileType: function(fileType){
				var filePath = ""
				if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
					filePath = cordova.file.applicationDirectory + 'www/img/attahement-types/'
				}else{
					filePath = "/app/img/attahement-types/"
				}
				//var fileName = getImageFileName(fileType)
				return filePath + getImageFileName(fileType);
			},
			setApplicationData : function(){
				var userData = this.getApplicationData('genieApp.appUserData');
				this.applicationData = {};
				if(checkObjectItemNull(userData)){
					this.applicationData.userLoginData = userData.userLoginData;
					this.applicationData.userRoles = userData.userRoles;
					this.applicationData.userRolesData = userData.userRolesData;
					this.applicationData.userGroupsData = userData.userGroupsData;
					this.applicationData.userActivityData = userData.userActivityData;
					this.applicationData.userCalendarData = userData.userCalendarData;
					this.applicationData.activityDetailItem = userData.activityDetailItem;
					this.applicationData.childrensData = userData.childrensData;
					this.applicationData.childrenActivity = userData.childrenActivity;
					this.applicationData.childrenCalendar = userData.childrenCalendar;
					this.applicationData.readUsersList = userData.readUsersList;
					this.applicationData.userStates = userData.userStates;
					this.applicationData.editActivityDetails = userData.editActivityDetails
					this.applicationData.capturedData = null;
				}else{
					this.applicationData = {};
					this.applicationData.userLoginData = null;
					this.applicationData.childrensData = [];
					this.applicationData.userRolesData = {};
					this.applicationData.userGroupsData = [];
					this.applicationData.userActivityData = {};
					this.applicationData.userCalendarData = {};
					this.applicationData.childrenActivity = {};
					this.applicationData.childrenCalendar = {};
					this.applicationData.activityDetailItem = null
					this.applicationData.editActivityDetails = null;
					this.applicationData.readUsersList = {'id':null,'userList':[]};
					this.applicationData.userStates = {'previous':null, 'current':null, 'next':null}
					this.applicationData.capturedData = null;
					$state.go('user-login');
				}
			},
			setAppInitialData: function(){
				this.applicationData = {};
				this.applicationData.userLoginData = null;
				this.applicationData.childrensData = [];
				this.applicationData.userRolesData = {};
				this.applicationData.userGroupsData = [];
				this.applicationData.userActivityData = {};
				this.applicationData.userCalendarData = {};
				this.applicationData.childrenActivity = {};
				this.applicationData.childrenCalendar = {};
				this.applicationData.activityDetailItem = null
				this.applicationData.editActivityDetails = null;
				this.applicationData.readUsersList = {'id':null,'userList':[]};
				this.applicationData.userStates = {'previous':null, 'current':null, 'next':null}
				this.applicationData.capturedData = null;
				return this.applicationData;
			},
			getApplicationData : function(){
				var appData = localStorage.getItem('genieApp.appUserData');
				appData = JSON.parse(appData)
				if(checkObjectItemNull(appData)){
					this.applicationData = appData;
					return this.applicationData;
				}else{
					this.applicationData = this.setAppInitialData()
					return this.applicationData;
				}
				
			},
			setApplicationStates: function(previous, current, next){
				var history = $ionicHistory.viewHistory()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				this.applicationData.userStates.previous = history.backView;
				this.applicationData.userStates.current = history.currentView;
				this.applicationData.userStates.next = history.forwardView;
			},
			getApplicationStates: function(){
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				return this.applicationData.userStates;
			},
			showProgressBarLoader: function(displayText){
				$('.progress-bar-loader').find('span').text(displayText)
				$('.progress-bar-loader').show()
			},
			hideProgressBarLoader: function(){
				$('.progress-bar-loader').find('span').text("")
				$('.progress-bar-loader').hide()
			},
			showIonicLoader: function(flag){
				$("#gn-app-loader").show();
				if(flag!=false){
					$timeout(function(){
						$("#gn-app-loader").hide();
					},30000)
				}
			},
			hideIonicLoader: function(){
				$("#gn-app-loader").hide();
			},
			setEditActivityDetails: function(param){
				this.applicationData.editActivityDetails = param;
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getEditActivityDetails: function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData('genieApp.appUserData');
				if(checkObjectItemNull(userData)){
					this.applicationData.editActivityDetails = userData.editActivityDetails
				}else{
					this.applicationData.editActivityDetails = [];
				}
				return this.applicationData.editActivityDetails;
			},
			setActivityReadUsersList: function(param){
				this.applicationData.readUsersList = param;
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getActivityReadUsersList: function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData('genieApp.appUserData');
				if(checkObjectItemNull(userData)){
					this.applicationData.readUsersList = userData.readUsersList
				}else{
					this.applicationData.readUsersList = [];
				}
				return this.applicationData.readUsersList;
			},
			setChindrensData : function(param){
				if(!checkObjectItemNull(param)){
					param = [];
				}
				this.applicationData.childrensData = param;
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getChindrensData : function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData();
				if(checkObjectItemNull(userData)){
					this.applicationData.childrensData = userData.childrensData
				}else{
					this.applicationData.childrensData = [];
				}
				return this.applicationData.childrensData;
			},
			setUserLoginData : function(param){
				this.applicationData.userLoginData = param;
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getUserLoginData : function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData();
				if(checkObjectItemNull(userData)){
					this.applicationData.userLoginData = userData.userLoginData;
				}else{
					this.applicationData.userLoginData = null;
				}
				return this.applicationData.userLoginData;
			},
			setUserGroupsData : function(param){
				this.applicationData.userGroupsData = param
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getUserGroupsData : function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				if(!checkObjectItemNull(this.applicationData.userGroupsData)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData();
				if(checkObjectItemNull(userData)){
					this.applicationData.userGroupsData = userData.userGroupsData;
				}else{
					this.applicationData.userGroupsData = [];
				}
				return this.applicationData.userGroupsData;
			},
			setUserRolesData : function(param){
				this.applicationData.userRolesData = param;
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getUserRolesData : function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				if(!checkObjectItemNull(this.applicationData.userRolesData)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData();
				if(checkObjectItemNull(userData)){
					this.applicationData.userRolesData = userData.userRolesData;
				}else{
					this.applicationData.userRolesData = null;
				}
				return this.applicationData.userRolesData;
			},
			setActivityStreamData: function(param){
				if(!checkObjectItemNull(param)){
					param = [];
				}
				this.applicationData.userActivityData = param;
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getActivityStreamData: function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				if(!checkObjectItemNull(this.applicationData.userActivityData)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData();
				if(checkObjectItemNull(userData)){
					this.applicationData.userActivityData = userData.userActivityData;
				}else{
					this.applicationData.userActivityData = {};
				}
				return this.applicationData.userActivityData;
			},
			setUserCalendarData: function(param){
				this.applicationData.userCalendarData = param;
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getUserCalendarData: function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				if(!checkObjectItemNull(this.applicationData.userCalendarData)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData();
				if(checkObjectItemNull(userData)){
					this.applicationData.userCalendarData = userData.userCalendarData;
				}else{
					this.applicationData.userCalendarData = {};
				}
				return this.applicationData.userCalendarData;
			},
			setActivityDetailItem: function(param){
				this.applicationData.activityDetailItem = param;
				localStorage.setItem('genieApp.appUserData', JSON.stringify(this.applicationData));
			},
			getActivityDetailItem: function(){
				this.applicationData = this.getApplicationData()
				if(!checkObjectItemNull(this.applicationData)){
					this.setApplicationData()
				}
				if(!checkObjectItemNull(this.applicationData.activityDetailItem)){
					this.setApplicationData()
				}
				var userData = this.getApplicationData();
				if(checkObjectItemNull(userData)){
					this.applicationData.activityDetailItem = userData.activityDetailItem;
				}else{
					this.applicationData.activityDetailItem = null;
				}
				return this.applicationData.activityDetailItem;
			},
			generateFileAttachment: function(attachmentURL){
				//return attachmentURL;
				if(window.location.host.search('localhost')==-1){
					return $sce.trustAsResourceUrl(attachmentURL);
				}else{
					var URLsplit = attachmentURL.split('//');
			        var url = URLsplit[1].split("/");
			        var finalUrl = ""
			        for(var i=1; i<url.length; i++){
			            finalUrl = finalUrl + "/" + url[i];
			        }
			        return $sce.trustAsResourceUrl(finalUrl);
				}
			},
			deleteActivity : function(item){
				var deferred = $q.defer();
				gapi.client.activitycalendar.deleteActivity(item).execute(function(resp) {
		        	if (!resp.code){
		            	deferred.resolve(resp.result)
		            }else{
		            	deferred.reject(resp.message);
		            }
		        })
		        return deferred.promise;
			},
			saveLikeUnlikeAnnotations: function(item){
				var deferred = $q.defer();
				gapi.client.activitycalendar.likeUnlikeActivity(item).execute(function(resp) {
		        	if (!resp.code){
		            	deferred.resolve(resp.result)
		            }else{
		            	deferred.reject(resp.message);
		            }
		        })
		        return deferred.promise;
			},
			saveCompleteUncompleteAnnotations: function(item){
				var deferred = $q.defer();
				gapi.client.activitycalendar.completeUncompleteActivity(item).execute(function(resp) {
		        	if (!resp.code){
		            	deferred.resolve(resp.result)
		            }else{
		            	deferred.reject(resp.message);
		            }
		        })
		        return deferred.promise;
			},
			getActivityChildrenNames: function(activity){
				var childs = [];
		        var childData = this.getChindrensData();
		        var groupIds = activity.groupIdList
		        angular.forEach(childData, function(value, key){
		            var found = _.intersection(value.readGroups, groupIds)
		            if(found.length>0){
		                childs.push({'id':value.id, 'firstName':value.firstName})
		            }
		        })
		        return childs;
			},
			getActivityChildInstituteNames : function(activity){
				var inst = [];
		        var childData = this.getChindrensData();
		        angular.forEach(childData, function(value, key){
					var found = _.findWhere(inst, {'id':activity.instituteId});
		            if(activity.instituteId==value.instituteWrapper.id && !checkObjectItemNull(found)){
		                inst.push({'id':value.instituteWrapper.id, 'displayName':value.instituteWrapper.displayName})
		            }
		        })
		        return inst;
			},
			getActivityTeacherInstituteNames : function(activity){
				var inst = [];
				var userRolesData = this.getUserRolesData();
				angular.forEach(userRolesData, function(value, key){
					if(activity.instituteId==value.instituteWrapper.id && value.userRole=='teacher'){
						inst.push({'id':value.instituteWrapper.id, 'displayName':value.instituteWrapper.displayName})
					}
				})
				return inst;
			},
			checkInternetConnection : function(){
				if(window.cordova){
				    $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
				    	if(window.gapi===undefined){
				    		window.userOnline = true;
					        var jsElm = document.createElement("script");
					        jsElm.src = "https://apis.google.com/js/client.js?onload=app_init";
					        document.body.appendChild(jsElm);
				    	}
				    	var alertPopup = $ionicPopup.alert({
					    	template: window.userMessages.USER_ONLINE
						});
						$timeout(function() {
							alertPopup.close()
						}, 3000)
				    })
				    $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
				    	var alertPopup = $ionicPopup.alert({
					    	template: window.userMessages.USER_OFFLINE
						});
						$timeout(function() {
							alertPopup.close()
						}, 3000)
				    })
				}else if(!window.cordova){
					window.addEventListener('online',  function(){
						if(window.gapi===undefined){
				    		window.userOnline = true;
					        var jsElm = document.createElement("script");
					        jsElm.src = "https://apis.google.com/js/client.js?onload=app_init";
					        document.body.appendChild(jsElm);
				    	}
				    	var alertPopup = $ionicPopup.alert({
					    	template: window.userMessages.USER_ONLINE
						});
						$timeout(function() {
							alertPopup.close()
						}, 3000)
					});
  					window.addEventListener('offline',  function(){
						var alertPopup = $ionicPopup.alert({
					    	template: window.userMessages.USER_OFFLINE
						});
						$timeout(function() {
							alertPopup.close()
						}, 3000)
					});
  				}
			}
		}
	}]);
	genieApp.service('CalendarService',['$rootScope', 'localStorageService', '$state', 'ApplicationFactory', 
	                                    function($rootScope, localStorageService, $state, ApplicationFactory){
		return {
			filterUserGroups : function(groupIds){
				var temp = [];
				var userGroupsData = ApplicationFactory.getUserGroupsData()
	        	angular.forEach(groupIds,function(value){
	        		var group = _.findWhere(userGroupsData,{'id':value})
	        		if(checkObjectItemNull(group)){
	        			temp.push(group)
	        		}
	        	})
	        	return temp;
			}
		}
	}])
	genieApp.directive('numericFieldOnly', function() {
		return {
			restrict : 'A',
			require : '?ngModel',
			link : function (scope, elem, attrs, ngModel) {
					var s = scope.input_value;
					elem.bind("keyup", function(event, value) {
						var myregexp = new RegExp("/^[0-9]*$/");
						if(isWindowsBrowser()){
							if(checkStringItemNull(elem.val()) && !myregexp.test(parseInt(elem.val()))){
								ngModel.$setViewValue((elem.val()).replace(/[^\d]/g, ''));
								ngModel.$render();	
							};
						}else{
							if(!myregexp.test(parseInt(ngModel.$viewValue)) && ngModel.$viewValue!==undefined){
								ngModel.$setViewValue((ngModel.$viewValue).replace(/[^\d]/g, ''));
								ngModel.$render();	
							};
						}
					})
					elem.bind("blur", function(event, value) {
						var myregexp = new RegExp("/^[0-9]*$/");
						if(isWindowsBrowser()){
							if(checkStringItemNull(elem.val()) && !myregexp.test(parseInt(elem.val()))){
								ngModel.$setViewValue((elem.val()).replace(/[^\d]/g, ''));
								ngModel.$render();	
							};
						}else{
							if(!myregexp.test(parseInt(ngModel.$viewValue)) && ngModel.$viewValue!==undefined){
								ngModel.$setViewValue((ngModel.$viewValue).replace(/[^\d]/g, ''));
								ngModel.$render();	
							};
						}
					})
				},
			scope : {
				ngModel:'=',
			}
		}
	})
	genieApp.directive('autofocus', function() {
		return {
			restrict : 'A',
			require : '?ngModel',
			link : function (scope, elem, attrs, ngModel) {
					var s = scope.input_value;
					elem.ready(function() {
						setTimeout(function(){
							$(elem[0]).focus();
						},500)
					})
				},
			scope : {
				ngModel:'=',
			}
		}
	})
	genieApp.filter('localDateFormat', function myDateFormat($filter){
		return function(date){
			//var  tempdate= new Date(text.replace(/-/g,"/"));
		    return $filter('date')(date, "MMM-dd-yyyy");
		}
	});
	genieApp.directive('uploadFile', ['$compile','ApplicationFactory', '$cordovaCamera', '$cordovaCapture', '$cordovaFile', '$cordovaFileTransfer', '$cordovaFileOpener2', '$timeout',
			function($compile, ApplicationFactory, $cordovaCamera, $cordovaCapture, $cordovaFile, $cordovaFileTransfer, $cordovaFileOpener2, $timeout) {
		return {
	    	restrict: 'A',
	        require: 'ngModel',
	        link: function(scope, element, attributes, controller) {
	        	element.bind("change", function(changeEvent) {
		        		if(element[0].value==''){
	        				return true;
	        			}
		                var fileObject = null;
		                var fileType = null;
		                for(var i=0; i<element[0].files.length; i++){
		                	if(element[0].files[0].size<=(10485760 * 3) && !window.cordova){
		                		fileObject = element[0].files[0];
		                		if(!checkStringItemNull(fileObject.type)){
		                			ApplicationFactory.showUserMessage("", window.userMessages.INCOMPATIBLE_FILETYPE, "", 3000)
		                			return true;
		                		}
		                		var restrict = scope.$parent.item.accept
		                		var flag = true;
		                		if(restrict=='image' && fileObject.type.indexOf('image')===-1){
		                			flag = false
		                			ApplicationFactory.showUserMessage("", window.userMessages.ONLY_IMAGES_ALLOWED, "", 3000)
		                			return true
		                		}else if(restrict=='video' && fileObject.type.indexOf('video')===-1){
		                			flag = false
		                			ApplicationFactory.showUserMessage("", window.userMessages.ONLY_VIDEOS_ALLOWED, "", 3000)
		                			return true
		                		}else if(restrict=='*'){
		                			if(fileObject.size>(10485760 * 3)){
		                				flag = false
		                			}else{
		                				flag = true
		                			}
		                		}
		                		if(flag==false){
		                			ApplicationFactory.showUserMessage("", window.userMessages.FILE_UPLOAD_LIMIT_EXCEED, "", 3000)
		                			$(element[0]).val("")
		                			return true
		                		}
		                	}else if(window.cordova){
		                		fileObject = element[0].files[0];
		                		if(!checkStringItemNull(fileObject.type)){
		                			ApplicationFactory.showUserMessage("", window.userMessages.INCOMPATIBLE_FILETYPE, "", 3000)
		                			return true;
		                		}
		                		var restrict = scope.$parent.item.accept
		                		var flag = true;
		                		if(restrict=='image' && fileObject.type.indexOf('image')===-1){
		                			flag = false
		                			ApplicationFactory.showUserMessage("", window.userMessages.ONLY_IMAGES_ALLOWED, "", 3000)
		                			return true
		                		}else if(restrict=='video' && fileObject.type.indexOf('video')===-1){
		                			flag = false
		                			ApplicationFactory.showUserMessage("", window.userMessages.ONLY_VIDEOS_ALLOWED, "", 3000)
		                			return true
		                		}else if(restrict=='*'){
		                			if(fileObject.size>(10485760 * 3)){
		                				flag = false
		                			}else{
		                				flag = true	
		                			}
		                		}
		                		if(flag==false){
		                			ApplicationFactory.showUserMessage("", window.userMessages.FILE_UPLOAD_LIMIT_EXCEED, "", 3000)
		                			$(element[0]).val("")
		                			return true
		                		}
		                	}else{
		             			ApplicationFactory.showUserMessage("", window.userMessages.FILE_UPLOAD_LIMIT_EXCEED, "", 3000)
		                		$(element[0]).val("")
		                		return true
		                	}
		                }
		           	if($(element[0]).val()!=""){
		                if(window.cordova && window.cordova.plugins){
		                	//ApplicationFactory.showIonicLoader(false)
		                	ApplicationFactory.showProgressBarLoader("Optimizing and Attaching ...")
		                	scope.$parent.progressbar.start()
		                	var fileObjectName = fileObject.name.replace(/^.*[\\\/]/, '')
    						var errorString = "Something went wrong. Please again try later."
    						var storagePath = ""
    						if(ionic.Platform.isIOS()){
    							storagePath	= cordova.file.documentsDirectory
    						}else if(ionic.Platform.isAndroid()){
    							storagePath = cordova.file.externalRootDirectory
    						}
    						var tempFileName = new Date().getTime() + "_" + fileObjectName
			                if(checkImageExist(fileObject.type)){
			                	window.resolveLocalFileSystemURL(storagePath, function success(dirEntry) {
									dirEntry.getFile(tempFileName, { create: true, exclusive: false }, function (fileEntry) {
										fileEntry.file(function (fileResolved) {
											$cordovaFile.writeFile(storagePath, tempFileName, fileObject, true).then(function(result, writeSuccess){
												window.imageResizer.resizeImage(function(data) {
													dirEntry.getFile(data.filename, { create: true, exclusive: false }, function (fileEntryResized) {
														fileEntryResized.file(function (fileResized) {
															if(fileObject.size<fileResized.size){
																scope.$apply(function(){
													                var tempItem = {}
													            	tempItem.fileAdded = true;
													            	tempItem.fileItem = fileObject;
													            	scope.$parent.pushAttachmentItem(tempItem)
													            	$(element[0]).val("")
													            	scope.$parent.completeProgressBarAction()
													        	});
															}else{
																if(fileResized.size<=(10485760 * 3)){
																	var fileItem = fileResized;
															    	var item = {}
															    	item.fileAdded = true;
															    	item.fileItem = fileResized
															    	item.device = true;
															    	if(item.fileItem.type===undefined || item.fileItem.type==null){
				        												item.fileItem.type="image/jpeg"
															        }
															       	if(scope.checkApplyInProgress()==false){
															        	scope.$apply(function(){
															            	scope.pushAttachmentItem(item)
															        	})
															    	}else{
															        	scope.pushAttachmentItem(item)
															    	}
															        $(element[0]).val("")
															   		//ApplicationFactory.hideIonicLoader()
															  		$cordovaFile.removeFile(storagePath, tempFileName).then(function (success) {
																		//ApplicationFactory.hideIonicLoader()
																		$(element[0]).val("")
																		scope.$parent.completeProgressBarAction()
																	}, function (error) {
																		//ApplicationFactory.hideIonicLoader()
																		$(element[0]).val("")
																		scope.$parent.completeProgressBarAction()																	});
																}else{
												             		ApplicationFactory.showUserMessage("", window.userMessages.FILE_UPLOAD_LIMIT_EXCEED, "", 3000)
												                	$(element[0]).val("")
												                	scope.$parent.completeProgressBarAction()
												                }
															}
														})
													}, function(err){
								                        ApplicationFactory.showUserMessage("",err,"",3000)
								                        //ApplicationFactory.hideIonicLoader()
								                        $cordovaFile.removeFile(storagePath, tempFileName).then(function (success) {
															//ApplicationFactory.hideIonicLoader()
															$(element[0]).val("")
															scope.$parent.completeProgressBarAction()
														}, function (error) {
															//ApplicationFactory.hideIonicLoader()
															$(element[0]).val("")
															scope.$parent.completeProgressBarAction()
														});
								            		})
												}, function (error) {
													//ApplicationFactory.hideIonicLoader()
													$(element[0]).val("")
													$cordovaFile.removeFile(storagePath, tempFileName).then(function (success) {
														//ApplicationFactory.hideIonicLoader()
														$(element[0]).val("")
														scope.$parent.completeProgressBarAction()
													}, function (error) {
														//ApplicationFactory.hideIonicLoader()
														$(element[0]).val("")
														scope.$parent.completeProgressBarAction()
													});
												}, fileEntry.toURL(), 0.5, 0.5, {
													resizeType: ImageResizer.RESIZE_TYPE_FACTOR,
													format: ImageResizer.FORMAT_JPG,
													imageDataType : ImageResizer.IMAGE_DATA_TYPE_URL,
													storeImage : true,
													quality : 50,
													filename:tempFileName + '-resized',
													directory : storagePath
												});
											}, function(err) {
												ApplicationFactory.showUserMessage("",err,"",3000)
												$cordovaFile.removeFile(storagePath, fileObjectName).then(function (success) {
													//ApplicationFactory.hideIonicLoader()
													$(element[0]).val("")
													scope.$parent.completeProgressBarAction()
												}, function (error) {
													//ApplicationFactory.hideIonicLoader()
													$(element[0]).val("")
													scope.$parent.completeProgressBarAction()
												})
											});
										})	
				                	}, function(err){
				                    	ApplicationFactory.showUserMessage("",err,"",3000)
				                    	//ApplicationFactory.hideIonicLoader()
				                    	scope.$parent.completeProgressBarAction()
				                    	$(element[0]).val("")
				                	})
				            	}, function(err){
				                	ApplicationFactory.showUserMessage("",err,"",3000)
				                	//ApplicationFactory.hideIonicLoader()
				                	scope.$parent.completeProgressBarAction()
				                	$(element[0]).val("")
				            	});
			                }else if(checkVideoExist(fileObject.type)){
			                	window.resolveLocalFileSystemURL(storagePath, function success(dirEntry) {
									dirEntry.getFile(fileObjectName, { create: true, exclusive: false }, function (fileEntry) {
										fileEntry.file(function (fileResolved) {
											$cordovaFile.writeFile(storagePath, fileObjectName, fileObject, true).then(function(result, writeSuccess){
												var videoFileName = ""
											    var lastDotPosition = fileResolved.name.lastIndexOf(".");
												if (lastDotPosition === -1){
											    	videoFileName = fileResolved.name;
												}else{ 
											    	videoFileName =  fileResolved.name.substr(0, lastDotPosition);
												}
												var videoOptions = {}
												videoOptions.fileUri = fileEntry.toURL();
												videoOptions.outputFileName = new Date().getTime() + "_" + videoFileName
												videoOptions.outputFileType = VideoEditorOptions.OutputFileType.MPEG4
												videoOptions.optimizeForNetworkUse = VideoEditorOptions.OptimizeForNetworkUse.YES
												videoOptions.saveToLibrary = false
												videoOptions.maintainAspectRatio = true
												videoOptions.width = 640
												videoOptions.height = 640
												videoOptions.videoBitrate = 1000000 // 1 megabit
												videoOptions.audioChannels = 2
												videoOptions.audioSampleRate = 44100
												videoOptions.audioBitrate = 128000 // 128 kilobits
											   	videoOptions.progress = function(info){
											    	/*var item = $("#gn-app-loader")
							                    	if(checkStringItemNull(item)){
							                        	if($("#gn-app-loader").css("display").toLowerCase()=="none"){
							                            	ApplicationFactory.showIonicLoader(false)
							                        	}
							                    	}*/
							                    	if(scope.$parent.progressbar.status()<=0){
						                                scope.$parent.progressbar.start() 
						                            }
											    }
												VideoEditor.transcodeVideo(function(result){
											    	window.resolveLocalFileSystemURL("file://" + result, function success(fileEntry) {
														fileEntry.file(function (fileCompressed) {
															if(fileObject.size<fileCompressed.size){
																scope.$apply(function(){
													                var tempItem = {}
													                tempItem.fileAdded = true;
													                tempItem.fileItem = fileObject;
													            	scope.$parent.pushAttachmentItem(tempItem)
													               	//ApplicationFactory.hideIonicLoader()
													                scope.$parent.completeProgressBarAction()
													            	$(element[0]).val("")
													        	});
															}else{
																if(fileCompressed.size<=(10485760 * 3)){
													            	var fileItem = fileCompressed;
													            	var item = {}
													            	item.fileAdded = true;
													            	item.fileItem = fileCompressed
													            	item.device = true;
													           		if(item.fileItem.type===undefined || item.fileItem.type==null){
		        														item.fileItem.type="video/quicktime"
													            	}
													           		if(scope.checkApplyInProgress()==false){
													                	scope.$apply(function(){
													                   		scope.pushAttachmentItem(item)
													                	})
													            	}else{
													                	scope.pushAttachmentItem(item)
													            	}
													           		$cordovaFile.removeFile(storagePath, fileObjectName).then(function (success) {
																		//ApplicationFactory.hideIonicLoader()
																		scope.$parent.completeProgressBarAction()
																		$(element[0]).val("")
																	}, function (error) {
																		//ApplicationFactory.hideIonicLoader()
																		scope.$parent.completeProgressBarAction()
																		$(element[0]).val("")
																	});
																}else{
																	//ApplicationFactory.hideIonicLoader()
																	scope.$parent.completeProgressBarAction()
												             		ApplicationFactory.showUserMessage("", window.userMessages.FILE_UPLOAD_LIMIT_EXCEED, "", 3000)
												                	$(element[0]).val("")
												               	}
												        	}
											            });
											        }, function (err) {
											            window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function success(dirEntry) {
											                dirEntry.getFile(result.replace(/^.*[\\\/]/, ''), { create: true, exclusive: false }, function (fileEntry) {
											                   	fileEntry.file(function (fileCompressed) {
											                   		if(fileObject.size<fileCompressed.size){
																		scope.$apply(function(){
															            	var tempItem = {}
															            	tempItem.fileAdded = true;
															            	tempItem.fileItem = fileObject;
															            	scope.$parent.pushAttachmentItem(tempItem)
															            	$(element[0]).val("")
															            	//ApplicationFactory.hideIonicLoader()
															            	scope.$parent.completeProgressBarAction()
															        	});
																	}else{
												                   		if(fileCompressed.size<=(10485760 * 3)){
													                        var fileItem = fileCompressed;
													                        var item = {}
													                    	item.fileAdded = true;
													                       	item.fileItem = fileCompressed
													                    	if(item.fileItem.type===undefined || item.fileItem.type==null){
													                       		item.fileItem.type="video/quicktime"
													                    	}
													                    	item.device = true;
													                        if(scope.checkApplyInProgress()==false){
													                            scope.$apply(function(){
													                           		scope.pushAttachmentItem(item)
													                        	})
													                        }else{
													                            scope.pushAttachmentItem(item)
													                        }
													                        $cordovaFile.removeFile(storagePath, fileObjectName).then(function (success) {
																				//ApplicationFactory.hideIonicLoader()
																				scope.$parent.completeProgressBarAction()
																				$(element[0]).val("")
																			}, function (error) {
																				//ApplicationFactory.hideIonicLoader()
																				scope.$parent.completeProgressBarAction()
																				$(element[0]).val("")
																			});
																		}else{
																			//ApplicationFactory.hideIonicLoader()
																			scope.$parent.completeProgressBarAction()														             		
																			ApplicationFactory.showUserMessage("", window.userMessages.FILE_UPLOAD_LIMIT_EXCEED, "", 3000)
														                	$(element[0]).val("")
														                }
														        	}
											              		});
											               	}, function(err){
											                    ApplicationFactory.showUserMessage("",err,"",3000)
											                    $cordovaFile.removeFile(storagePath, fileObjectName).then(function (success) {
																	//ApplicationFactory.hideIonicLoader()
																	scope.$parent.completeProgressBarAction()
																	$(element[0]).val("")
																}, function (error) {
																	//ApplicationFactory.hideIonicLoader()
																	scope.$parent.completeProgressBarAction()																	
																	$(element[0]).val("")
																});
											            	})
											        	}, function(err){
											            	ApplicationFactory.showUserMessage("",err,"",3000)
															$cordovaFile.removeFile(storagePath, fileObjectName).then(function (success) {
																//ApplicationFactory.hideIonicLoader()
																scope.$parent.completeProgressBarAction()
																$(element[0]).val("")
															}, function (error) {
																//ApplicationFactory.hideIonicLoader()
																scope.$parent.completeProgressBarAction()
																$(element[0]).val("")
															});											                        
														});
											    	});
												}, function(err){
											    	ApplicationFactory.showUserMessage("",err,"",3000)
											    	$cordovaFile.removeFile(storagePath, fileObjectName).then(function (success) {
														//ApplicationFactory.hideIonicLoader()
														scope.$parent.completeProgressBarAction()														
														$(element[0]).val("")
													}, function (error) {
														//ApplicationFactory.hideIonicLoader()
														scope.$parent.completeProgressBarAction()														
														$(element[0]).val("")
													});
												},videoOptions)
											}, function(err) {
												ApplicationFactory.showUserMessage("",err,"",3000)
												$cordovaFile.removeFile(storagePath, fileObjectName).then(function (success) {
													//ApplicationFactory.hideIonicLoader()
													scope.$parent.completeProgressBarAction()
													$(element[0]).val("")
												}, function (error) {
													//ApplicationFactory.hideIonicLoader()
													scope.$parent.completeProgressBarAction()
													$(element[0]).val("")
												});
											})
										})
				                	}, function(err){
				                    	ApplicationFactory.showUserMessage("",err,"",3000)
				                    	//ApplicationFactory.hideIonicLoader()
				                    	scope.$parent.completeProgressBarAction()
				                    	$(element[0]).val("")
				                	})
				                }, function(err){
				                	ApplicationFactory.showUserMessage("",err,"",3000)
				                	//ApplicationFactory.hideIonicLoader()
				                	scope.$parent.completeProgressBarAction()
				                	$(element[0]).val("")
				            	});
			            	}else{
			                	scope.$apply(function(){
				                	var tempItem = {}
					            	tempItem.fileAdded = true;
					            	tempItem.fileItem = fileObject;
					            	//tempItem.device = true;
					            	scope.$parent.pushAttachmentItem(tempItem)
					            	$(element[0]).val("")
					            	//ApplicationFactory.hideIonicLoader()
					            	scope.$parent.completeProgressBarAction()
					        	})
			                }
		               	}else{
			                scope.$apply(function(){
				                var tempItem = {}
				            	tempItem.fileAdded = true;
				            	tempItem.fileItem = fileObject;
				            	scope.$parent.pushAttachmentItem(tempItem)
				            	//ApplicationFactory.hideIonicLoader()
				            	scope.$parent.completeProgressBarAction()
				            	$(element[0]).val("")
				            });
			        	}
		        	}
	        	});
	        }
	     };
	}]);
	genieApp.filter('sortByUpdatedDate', function () {
		return function (events) {
			if (!events) {
				return true;
			}
			if (events.length == 0) {
				return events;
			}
			for(var i=0; i<events.length; i++){
				for(var j=i+1; j<events.length; j++){
					if(parseInt(events[i].updatedTime)>parseInt(events[j].updatedTime)){
						var temp = events[i]
						events[i] = events[j]
						events[j] = temp;
					}
				}
			}
			return events;
		};
	});
	genieApp.filter('sortByUpdatedTime', function () {
		return function (events) {
			if (!events) {
				return true;
			}
			if (events.length == 0) {
				return events;
			}
			for(var i=0; i<events.length; i++){
				for(var j=i+1; j<events.length; j++){
					var t1 = new Date(events[i].updatedTime).getTime()
					var t2 = new Date(events[j].updatedTime).getTime()
					if(t1 < t2){
						var temp = events[i]
						events[i] = events[j]
						events[j] = temp;
					}
				}
			}
			return events;
		};
	});
	genieApp.filter('showAvailableParentFilters', function () {
		return function (parentGroups, filterCriteria) {
			var temp = [];
			var flag = false;
			if (!checkObjectItemNull(parentGroups) || !checkObjectItemNull(filterCriteria)) {
				return parentGroups;
			}
			angular.forEach(parentGroups, function(value){
				var found = _.findWhere(filterCriteria, {'id':value.id})
				if(!checkObjectItemNull(found)){
					temp.push(value)
				}
			})
			return temp;
		}
	})
	genieApp.filter('showParentFilteredGroupActivity', function () {
		return function (events, filterGroups) {
			var temp = [];
			var flag = false;

			if (!$.isArray(events) || !checkObjectItemNull(filterGroups)) {
				events = []
				return events;
			}
			var selectedChilds = [];
			var selectedInstitutes = [];
			angular.forEach(filterGroups[0], function(value){
				if(value.type=='student'){
					selectedChilds.push(value)
				}else if(value.type=='institute'){
					selectedInstitutes.push(value)
				}
			})
			if(!checkObjectItemNull(selectedChilds)){
				return events;
			}
			angular.forEach(events, function(event, key){
				var flag = true;
				angular.forEach(selectedChilds, function(child, key){
					var commonGroups = _.intersection(child.readGroups, event.groupIdList)
					if(commonGroups.length>0){
						var found = _.findWhere(temp,{'id':event.id})
						if(!checkObjectItemNull()){
							temp.push(event)
						}
					}
				});
			});
			return temp;
		};
	});
/*	genieApp.filter('showParentFilteredGroupActivity', function () {
		return function (events, filterGroups) {
			var temp = [];
			var flag = false;
			if (!checkObjectItemNull(events) || !checkObjectItemNull(filterGroups)) {
				return events;
			}
			var selectedChilds = [];
			var selectedInstitutes = [];
			angular.forEach(filterGroups[0], function(value){
				if(value.id=='all'){
					flag = true;
				}
			})
			angular.forEach(filterGroups[0], function(value){
				if(value.type=='student'){
					selectedChilds.push(value)
				}else if(value.type=='institute'){
					selectedInstitutes.push(value)
				}
			})	
			angular.forEach(events, function(event, key){
				var flag = true;
				angular.forEach(selectedChilds, function(child, key){
					var commonGroups = null
					if(filterGroups[1]=='calendar'){
						commonGroups = _.intersection(child.readGroups, event.event.groupIdList)
					}else if(filterGroups[1]=='activity-stream'){
						commonGroups = _.intersection(child.readGroups, event.groupIdList)
					}
					if(commonGroups.length>0 && flag==true){
						flag = true;
					}else{
						flag = false;
					}
				});
				if(flag==true){
					temp.push(event)
				}
			});
			if(selectedInstitutes.length<1){
				return temp;
			}
			var finalArray = [];
			angular.forEach(temp, function(event, key){
				flag = true;
				var instituteId = null
				if(filterGroups[1]=='calendar'){
					instituteId = event.event.instituteId
				}else if(filterGroups[1]=='activity-stream'){
					instituteId = event.instituteId
				}
				angular.forEach(selectedInstitutes, function(institute, key){
					if(flag==true && instituteId==institute.instituteId){
						flag = true;
					}else{
						flag = false;
					}
				});
				if(flag==true){
					finalArray.push(event)
				}
			})
			return finalArray;
		};
	});*/
	genieApp.filter('showTeacherFilteredGroupActivity', function () {
		return function (events, filterGroups) {
			var temp = [];
			var flag = false;
			if (!events) {
				return temp;
			}
			if(events.length == 0) {
				return temp;
			}
			if(!checkObjectItemNull(filterGroups[0])){
				return events;
			}
			angular.forEach(filterGroups[0], function(value){
				if(value.id=='all'){
					flag = true;
				}
			})
			if(flag==true){
				angular.forEach(events, function(event){
					var activityOwner = null;
					if(filterGroups[3]=='calendar'){
						activityOwner = event.event.activityOwner
					}else if(filterGroups[3]=='activity-stream'){
						activityOwner = event.activityOwner
					}
					if(activityOwner==filterGroups[2]){
						temp.push(event)
					}
				})
				return temp;
			}
			var availableUserGroupList = filterGroups[1]
			var parentGroupIds = [];
			var level1GroupIds = [];
			var level2GroupIds = [];
			angular.forEach(filterGroups[0], function(value){
				parentGroupIds.push(value.id)
			})
			angular.forEach(parentGroupIds, function(value){
				angular.forEach(availableUserGroupList, function(group, key){
					if(group.parentGroupId==value){
						level1GroupIds.push(group.id)
					}
				});
			})
			angular.forEach(level1GroupIds, function(value){
				angular.forEach(availableUserGroupList, function(group, key){
					if(group.parentGroupId==value){
						level2GroupIds.push(group.id)
					}
				});
			})
			parentGroupIds = _.union(parentGroupIds, level1GroupIds, level2GroupIds)
			//parentGroupIds.concat(level2GroupIds)
			//parentGroupIds = _.uniq(parentGroupIds)
			angular.forEach(events, function(event, key){
				var eventGroups = null;
				if(filterGroups[3]=='calendar'){
					eventGroups = event.event.groupIdList
				}else if(filterGroups[3]=='activity-stream'){
					eventGroups = event.groupIdList
				}
				var foundGroups = _.intersection(eventGroups, parentGroupIds)
            	if(foundGroups.length>0){
                	temp.push(event)
                }
			});
			return temp;
		};
	});
	genieApp.filter('capitalizeMonth', function () {
		return function (weekName) {
			if(checkStringItemNull(weekName)){
				return weekName.charAt(0).toUpperCase();
			}
		};
	});
	genieApp.filter('portraitWeekName', function () {
		return function (weekName, deviceOrientation) {
			if(!checkStringItemNull(weekName) || !checkStringItemNull(deviceOrientation)){
				return "";
			}
			if(deviceOrientation=='portrait' && checkUserOnMobileBrowser()){
				return weekName.charAt(0) 
			}else{
				return weekName;
			}
		};
	});
	genieApp.filter('pollingAnswers', function () {
		return function (pollAnswers) {
			if(checkStringItemNull(pollAnswers)){
				return pollAnswers.join(', ');
			}
		};
	});
	genieApp.filter('ImagesOnly', function () {
		return function (attachments) {
			if(!checkObjectItemNull(attachments)){
				return attachments
			}
			var temp = [];
			angular.forEach(attachments, function(value, key){
				if(checkImageExist(value.fileType)){
					temp.push(value)
				}
			});
			return temp;
		};
	});
	genieApp.filter('NonImageFiles', function () {
		return function (attachments) {
			if(!checkObjectItemNull(attachments)){
				return attachments
			}
			var temp = [];
			angular.forEach(attachments, function(value, key){
				if(!checkImageExist(value.fileType)){
					temp.push(value)
				}
			});
			return temp;
		};
	});
	genieApp.filter('generateRandomId', function () {
		return function (randomId) {
			var temp = [];
			for(var i=0;i<200;i++){
				temp.push(i)
			}
			return temp;
		};
	});
})(window.angular);