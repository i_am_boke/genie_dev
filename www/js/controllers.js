var genieControllers = angular.module('genie.controllers', []);

genieControllers.controller('AuthenticationController',['$scope', '$state', 'ApplicationFactory', 'localStorageService', '$ionicHistory','$timeout', 
                                        function($scope, $state, ApplicationFactory, localStorageService, $ionicHistory, $timeout) {
    ApplicationFactory.setApplicationData();
    var startDate = new Date()
    var endDate = new Date().setDate(new Date().getDate()+7)
    $scope.sentUserOTP = false;
    $scope.lockOTP = false;
    $scope.loginButtons = {'next':false}
    $scope.userLoginForm = {    
                                userName:'', userPassword:'', userType:'', 
                                instituteId:'', genieId:'', startDate:convertToDate(startDate), endDate:convertToDate(endDate), 
                                loggedIn:false, loginDate:null, 
                                calendarStartDate:convertToDate(startDate), calendarEndDate:convertToDate(startDate),
                                userGroupList : [], dayRefreshTime: null, weekRefreshTime: null,
                                monthRefreshTime: null, activityRefreshTime: null
                            };
    var userProfile = ApplicationFactory.getUserLoginData();
    if(checkObjectItemNull(userProfile)){
        if(userProfile.loggedIn){
            $state.go('activity-stream',{'refresh':true})
        }
    }
    $scope.loginFormError = function(authorizationForm){
        if(authorizationForm.$invalid){
            return true;
        }else{
            return false;
        }
    }
    $scope.getLogoUrl = function(fileName){
        var filePath = ""
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            filePath = cordova.file.applicationDirectory + 'www/img/logo/'
        }else{
            filePath = "img/logo/"
        }
        return filePath + fileName;
    }
    $scope.signIn = function() {
        if(!window.userOnline || !navigator.onLine){
            ApplicationFactory.showUserMessage("",window.userMessages.USER_OFFLINE,"",3000)
            return true
        }
        ApplicationFactory.showIonicLoader()
        if(!checkObjectItemNull(userProfile)){  
            $scope.getUserRoles()
        }else{
            if(!userProfile.loggedIn){
                $scope.getUserRoles()
            }else{
                ApplicationFactory.hideIonicLoader()
                $state.go('activity-stream',{'refresh':true});
            }
        }
    };
    $scope.validateMobile = function(){
        if(!window.userOnline || !navigator.onLine){
            ApplicationFactory.showUserMessage("",window.userMessages.USER_OFFLINE,"",3000)
            return true
        }
        if(gapi.client.usermaster===undefined){
            gapi.client.load('usermaster', 'v1', function() {
                $scope.getUserOTP();
            })
        }else{
            $scope.getUserOTP();
        }
    }
    $scope.goToMobileScreen = function(){
        $scope.loginButtons.next = false
    }
    if((window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) || checkUserOnMobileBrowser()){
        $scope.$watch('userLoginForm.userName', function(newValue, oldValue, scope) {
            if(checkStringItemNull($scope.userLoginForm.userName)){
                $('.user-name-field').blur();
                if(window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard){
                    cordova.plugins.Keyboard.close();
                }
            }
        });
    }
    $scope.validateUserName = function(){
        if(checkStringItemNull($scope.userLoginForm.userName)){
            return true
        }else{
            return false;
        }
    }
    $scope.showOTPScreen = function(){
        return $scope.loginButtons.next
    }
    $scope.validateUserOTP = function(){
        if(checkStringItemNull($scope.userLoginForm.userPassword)){
            return true
        }else{
            return false;
        }
    }
    $scope.getUserOTP = function(){
        ApplicationFactory.showIonicLoader()
        $scope.sentUserOTP = false;
        gapi.client.usermaster.getUserOTP($scope.userLoginForm).execute(function(resp) {
            $scope.$apply(function(){
                if(!resp.code){
                    $scope.loginButtons.next = true
                    $scope.sentUserOTP = true;
                }else{
                    $scope.sentUserOTP = false;
                    if(!window.userOnline || !navigator.onLine){
                        ApplicationFactory.showUserMessage("",window.userMessages.USER_OFFLINE,"",3000) 
                    }else{
                        ApplicationFactory.showUserMessage("", window.userMessages.MOBILE_NOT_REGISTERED,"",3000)
                    }
                }
                ApplicationFactory.hideIonicLoader()
            })
        })
    }
    $scope.getUserRoles = function(){
        gapi.client.usermaster.getUserRoles($scope.userLoginForm).execute(function(resp) {
            //$timeout(function(){
                $scope.$apply(function(){
                    if(!resp.code){
                        var userData = resp.result.items;
                        ApplicationFactory.setUserLoginData($scope.userLoginForm);
                        ApplicationFactory.setUserRolesData(resp.result.items)
                        $state.go('user-roles');
                    }else{
                        $scope.userLoginForm.userPassword = undefined;
                        if(window.userOnline || navigator.onLine){
                            ApplicationFactory.showUserMessage("", window.userMessages.PASSWORD_NOT_MATCHED,"",3000)
                        }
                    }
                    ApplicationFactory.hideIonicLoader()
                })
            //}, 1000)
        })
    }
    $scope.$on("$ionicView.leave", function( scopes, states ) {
        $scope.userLoginForm.userName = undefined;
        $scope.loginButtons.next = false
        $scope.userLoginForm.userPassword = undefined;
    })
    $scope.clearCache = function(){
        localStorageService.clearAll();
        localStorage.clear()
        $ionicHistory.clearCache().then(function () {
            $ionicHistory.clearHistory();
        });
    }
    $scope.$on("$ionicView.enter", function( scopes, states ) {
        userProfile = ApplicationFactory.getUserLoginData();
        if(checkObjectItemNull(userProfile)){
            if(userProfile.loggedIn==true && checkStringItemNull(userProfile.genieId)){
                var activityState = ApplicationFactory.getApplicationStates()
                if(activityState.backView==null){
                    $state.go('activity-stream')
                }else{
                    $ionicHistory.forwardView().stateName
                }
            }else{
                $scope.clearCache()
            }
        }else{
            $scope.clearCache   
            ApplicationFactory.setAppState('user-login');
        }
        ApplicationFactory.setApplicationStates();
    });
}])

genieControllers.controller('UserRolesController',['$scope', 'ApplicationFactory', '$state', '$ionicPopup', 'localStorageService', 
                '$ionicPlatform', '$ionicHistory', '$timeout',
            function($scope, ApplicationFactory, $state, $ionicPopup, localStorageService, $ionicPlatform, $ionicHistory, $timeout){
    $scope.selectedUserType = "";
    var userProfile = ApplicationFactory.getUserLoginData();
    $scope.parentRole = false;
    $scope.selectedInstitute = null
    $scope.emptyArray = [];
    $scope.selectParentRole = function(){
        if($scope.selectedUserType!='parent'){
            userProfile.userType = "parent";
            $scope.selectedUserType = "parent";
            userProfile.instituteId = "";
            ApplicationFactory.setActivityStreamData($scope.emptyArray)
            ApplicationFactory.setUserCalendarData($scope.emptyArray)
            ApplicationFactory.setActivityDetailItem($scope.emptyArray)
            userProfile = $scope.refreshContentRefreshTimers(userProfile);
            ApplicationFactory.setUserLoginData(userProfile)
            $scope.setSelectedRole()
            if(userProfile.loginDate==null ||  !checkDateEqualsToday(userProfile.loginDate)){
                ApplicationFactory.showIonicLoader()
                $scope.getActivityStream(userProfile);
            }else{
                $state.go('activity-stream',{'refresh':false});
            }
        }else{
            $state.go('activity-stream',{'refresh':false});
        }
        $("body.genieApp").addClass("gn-parent-view").removeClass("gn-teacher-view");
    }
    $scope.selectTeacherRole = function(userRole){
        if(!checkObjectItemNull($scope.selectedInstitute) || ($scope.selectedInstitute.id != userRole.instituteWrapper.id 
                    || $scope.selectedUserType!='teacher') || userProfile.loginDate==null){
            $scope.selectedUserType = "teacher";
            $scope.selectedInstitute = userRole.instituteWrapper;
            userProfile.userType = "teacher";
            userProfile.instituteId = userRole.instituteWrapper.id;
            userProfile = $scope.refreshContentRefreshTimers(userProfile);
            ApplicationFactory.setUserLoginData(userProfile)
            ApplicationFactory.setActivityStreamData($scope.emptyArray)
            ApplicationFactory.setUserCalendarData($scope.emptyArray)
            ApplicationFactory.setActivityDetailItem($scope.emptyArray)
            $scope.setSelectedRole()
            if(userProfile.loginDate==null ||  !checkDateEqualsToday(userProfile.loginDate)){
                ApplicationFactory.showIonicLoader()
                $scope.getActivityStream(userProfile);
            }else{
                $state.go('activity-stream',{'refresh':false});
            }
        }else{
            $state.go('activity-stream',{'refresh':false});
        }
        $("body.genieApp").removeClass("gn-parent-view").addClass("gn-teacher-view");
    }
    $scope.refreshContentRefreshTimers = function(userProfile){
        userProfile.activityRefreshTime = null;
        userProfile.dayRefreshTime = null;
        userProfile.weekRefreshTime = null;
        userProfile.monthRefreshTime = null;
        return userProfile;
    }
    $scope.getImageUrl = function(fileName){
        var filePath = ""
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            filePath = cordova.file.applicationDirectory + 'www/img/'
        }else{
            filePath = "/app/img/"
        }
        return filePath + fileName;
    }
    $scope.checkUserLogged = function(){
        userProfile = ApplicationFactory.getUserLoginData();
        if(userProfile.loggedIn==true){
            return true;
        }else{
            return false;
        }
    }
    $scope.logout = function(){
        var confirmPopup = $ionicPopup.confirm({
            template: window.userMessages.LOGOUT_CONFIRM
        });
        confirmPopup.then(function(res) {
            if(res){
                localStorageService.clearAll();
                localStorage.clear()
                $ionicHistory.clearCache().then(function () {
                    $ionicHistory.clearHistory();
                });
                $state.go('user-login')
            }
        });
    }
    $scope.getParentName = function(){
        if(checkObjectItemNull($scope.userRolesData)){
            var parentProfile = _.findWhere($scope.userRolesData, {'userRole':'parent'})
            if(checkObjectItemNull(parentProfile)){
                return parentProfile.firstName;
            }
        }else{
            return "";
        }
    }
    $scope.setSelectedRole = function(){
        userProfile = ApplicationFactory.getUserLoginData();
        $scope.userRolesData = ApplicationFactory.getUserRolesData();
        if(userProfile.userType=='parent'){
            var parentProfile = _.findWhere($scope.userRolesData, {'userRole':'parent'})
            $scope.selectedUserType = "parent"
            userProfile.genieId = parentProfile.id
            $scope.selectedParentName = userProfile.firstName;
        }else if(userProfile.userType=='teacher'){
            $scope.selectedUserType = "teacher"
            angular.forEach($scope.userRolesData, function(role, key){
                if(role.userRole=='teacher'){
                    if(role.instituteWrapper.id==userProfile.instituteId){
                        userProfile.genieId = role.id;
                        $scope.selectedInstitute = role.instituteWrapper
                    }
                }
            });
        }
        ApplicationFactory.setUserLoginData(userProfile)
    }
    $scope.$on("$ionicView.enter", function( scopes, states ) {
        ApplicationFactory.setAppState('user-roles');
        userProfile = ApplicationFactory.getUserLoginData();
        $scope.userRolesData = ApplicationFactory.getUserRolesData();
        if(checkObjectItemNull($scope.userRolesData)){
            angular.forEach($scope.userRolesData, function(value, key){
                if(value.userRole=='parent'){
                    $scope.parentRole = true;
                }
            });
        }
        if(userProfile.loggedIn==false && userProfile.userType!=''){
            userProfile.loggedIn = true;
            ApplicationFactory.setUserLoginData(userProfile)
        }
        if(userProfile.loggedIn==true && checkStringItemNull(userProfile.userType)){
            $scope.selectedUserType = userProfile.userType;
            $scope.setSelectedRole();
        }
        $('.gn-header-right-action').removeClass('not-single-icon')
        ApplicationFactory.setApplicationStates();
    });
    $scope.cancelLogin = function(){
        $ionichistory.goback();
    }
    $scope.getActivityStream = function(userProfile){
        ApplicationFactory.showIonicLoader()
        if(gapi.client.usermaster===undefined){
            gapi.client.load('usermaster', 'v1', function() {
                $scope.doUserLogin(userProfile)
            })
        }else{
            $scope.doUserLogin(userProfile)
        }
    }
    $scope.doUserLogin = function(){
        var temp = [];
        var genieId = ""
        if(userProfile.userType=='parent'){
            if(!checkObjectItemNull(ApplicationFactory.setChindrensData())){
                userProfile.userGroupList = getChildGroups(ApplicationFactory.setChindrensData());
            }else{
                userProfile.userGroupList = temp;
            }
        }else{
            var userRolesData = ApplicationFactory.getUserRolesData()
            userProfile.userGroupList = getLoggedUserGroupList(userRolesData, userProfile.instituteId, userProfile.userType)
        }
        gapi.client.usermaster.doUserLogin(userProfile).execute(function(resp) {
            //$timeout(function(){
                $scope.$apply(function(){
                    ApplicationFactory.hideIonicLoader()
                    if(!resp.code){
                        userProfile.loginDate = new Date(setStartDate(new Date()))
                        userProfile.loggedIn = true
                        ApplicationFactory.setUserGroupsData(resp.result.groupMasterWrapper)
                        ApplicationFactory.setChindrensData(resp.result.childrensRolesData)
                        $scope.setActivityStreamData(resp.result.calendarActivity)
                        $scope.setUserCalendarData(resp.result.calendarActivity)
                        ApplicationFactory.setUserRolesData(resp.result.userRolesData);
                        ApplicationFactory.setUserLoginData(userProfile);
                        setUser(userProfile)
                        $scope.subScribeToGCM()
                        $state.go('activity-stream');
                    }else{
                        console.log(resp.message)
                    }
                })
            //}, 1000)
        })
    }
    $scope.isGCMSubscribed = function(){
        return isGCMSubscribed();
    }
    $scope.subScribeToGCM = function(){
        if(!chromeOniOS() && !ionic.Platform.isIOS() && !ionic.Platform.isAndroid() && isChromeBrowser()){
            subscribeToGCM()
        }
    }
    $scope.unsubScribeFromGCM = function(){
        unsubScribeFromGCM()
    }
    $scope.setUserCalendarData = function(calendarData){
        var oldCalendarData = ApplicationFactory.getActivityStreamData();
        calendarData = pushItemsToArray(oldCalendarData, calendarData)
        ApplicationFactory.setUserCalendarData(calendarData)
    }
    $scope.setActivityStreamData = function(activityData){
        var oldActivityData = ApplicationFactory.getActivityStreamData();
        activityData = pushItemsToArray(oldActivityData, activityData)
        ApplicationFactory.setActivityStreamData(activityData)
    }
}]);

genieControllers.controller('ApplicationHeaderController',['$scope', '$state', 'ApplicationFactory', '$ionicHistory',
        '$cordovaPush', '$ionicPlatform','$cordovaToast', '$cordovaDialogs', '$cordovaMedia',
         function($scope, $state, ApplicationFactory, $ionicHistory, $cordovaPush, $ionicPlatform, $cordovaToast,
                  $cordovaDialogs, $cordovaMedia){
    $scope.userLoggedIn = false;
    var userData = ApplicationFactory.getUserLoginData()
    var gcmKey = null;
    var gcmEnabled = null;
    $scope.userLoggedIn = false;
    ApplicationFactory.checkInternetConnection()
    $scope.$on('SetApplicationState', function(event, param){
        userData = ApplicationFactory.getUserLoginData()
        if(userData!=null && userData.loggedIn && param.data!='user-login'){
            $scope.stateName = param.data;
            $scope.userType = userData.userType;
            $scope.userLoggedIn = userData.loggedIn
            if ((ionic.Platform.isIOS() || ionic.Platform.isAndroid()) && $scope.userLoggedIn==true && window.cordova && window.cordova.plugins) {
                $scope.registerGCM()
            }
        }else{
            ApplicationFactory.hideIonicLoader()
            $scope.stateName = "user-login"
        }
        if($scope.userLoggedIn){
            var userRolesData = ApplicationFactory.getUserRolesData()
            var userGroupsData = ApplicationFactory.getUserGroupsData()
            if((!checkObjectItemNull(userRolesData) || !checkObjectItemNull(userGroupsData)) && param.data!='user-login' && param.data!='user-roles'){
                removeLocalstorage()
                $state.go('user-login')
            }
        }
    })
    $scope.getImageUrl = function(fileName){
        var filePath = ""
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            filePath = cordova.file.applicationDirectory + 'www/img/'
        }else{
            filePath = "/app/img/"
        }
        return filePath + fileName;
    }
    $scope.getLogoUrl = function(fileName){
        var filePath = ""
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            filePath = cordova.file.applicationDirectory + 'www/img/logo/'
        }else{
            filePath = "/app/img/logo/"
        }
        return filePath + fileName;
    }
    $scope.applyHeaderClasses = function(){
        userProfile = ApplicationFactory.getUserLoginData();
        if(checkObjectItemNull(userProfile) && userProfile.userType=='teacher'){
            return 'not-single-icon'
        }else if(checkObjectItemNull(userProfile) && userProfile.userType=='parent'){
            return ''
        }else{
            return 'not-single-icon'
        }
    }
    $scope.getApplicationState = function(){
        return $scope.stateName
    }
    $scope.checkUserLogged = function(){
        userData = ApplicationFactory.getUserLoginData()
        if(checkObjectItemNull(userData) && userData.loggedIn==true){
            return true
        }else{
            return false
        }
    }
    $scope.subScribeToGCM = function(){
        if(!chromeOniOS() && !ionic.Platform.isIOS() && !ionic.Platform.isAndroid() && isChromeBrowser()){
            subscribeToGCM()
        }
    }
    $scope.goPreviousState = function(){
        var activityState = ApplicationFactory.getApplicationStates()
        if(activityState.previous==null){
            $state.go('activity-stream')
        }else{
            $ionicHistory.goBack()
        }
    }
    $scope.goToCalendar = function(){
        if(!checkObjectItemNull(userData)){
            $scope.userLoggedIn = false
            $state.go('user-login')
            return true;
        }
        if(userData.userType=='parent' || userData.userType=='teacher'){
            $scope.userLoggedIn = true
            $state.go('user-calendar')
        }else{
            $scope.userLoggedIn = false
            $state.go('user-login')
        }
    }
    $scope.registerGCM = function(){
        if((ionic.Platform.isIOS() || ionic.Platform.isAndroid()) && window.cordova && window.cordova.plugins && $scope.userLoggedIn){
            try{
                var config = null;
                if (ionic.Platform.isAndroid()){
                    config = {
                        "senderID": "731396128624", // REPLACE THIS WITH YOURS FROM GCM CONSOLE - also in the project URL like: https://console.developers.google.com/project/434205989073
                        "iconColor":"#753a6a"
                    };
                }else if (ionic.Platform.isIOS()) {
                    config = {
                        "badge": "true",
                        "sound": "true",
                        "alert": "true"
                    }
                }
                $cordovaPush.register(config).then(function (result) {
                    //$cordovaToast.showShortCenter('Registered for push notifications');
                    $scope.registerDisabled=true;
                        // ** NOTE: Android regid result comes back in the pushNotificationReceived, only iOS returned here
                        if (ionic.Platform.isIOS()) {
                            gcmKey = localStorage.getItem("GenieApp.gcmKey");
                            gcmEnabled = localStorage.getItem("GenieApp.gcmEnabled");
                            $scope.regId = result;
                            $scope.regId = result;
                            if(gcmEnabled!="true"  && gcmKey!="" && gcmKey!=$scope.regId){
                                $scope.storeDeviceToken("ios");
                            }
                        }
                    },function (err) {
                        console.log("Register error " + err)
                });
            }catch(err) {
                alert(err.message);
            }    
        }
    }
    // Notification Received
    $scope.$on('$cordovaPush:notificationReceived', function (event, notification) {
        //$cordovaToast.showShortCenter(JSON.stringify([notification]));
        if (ionic.Platform.isAndroid()) {
            $scope.handleAndroid(notification);
        }
        else if (ionic.Platform.isIOS()) {
            $scope.handleIOS(notification);
            /*$scope.$apply(function () {
                $scope.notifications.push(JSON.stringify(notification.alert));
            })*/
        }
    });

    // Android Notification Received Handler
    $scope.handleAndroid = function(notification) {
        // ** NOTE: ** You could add code for when app is in foreground or not, or coming from coldstart here too
        //             via the console fields as shown.
        if (notification.event == "registered") {
            $scope.regId = notification.regid;
            gcmKey = localStorage.getItem("GenieApp.gcmKey");
            gcmEnabled = localStorage.getItem("GenieApp.gcmEnabled");
            if(gcmEnabled!="true"  && gcmKey!="" && gcmKey!=$scope.regId){
                $scope.storeDeviceToken("android");
            }
        }else if (notification.event == "message") {
            var messageString = JSON.stringify(notification.message)
            ApplicationFactory.showUserMessage("",messageString,"",3000)
            /*var soundfile = notification.soundname || notification.payload.sound;
            var my_media = new Media("/android_asset/www/"+ soundfile);
            my_media.play();*/
        }else if (notification.event == "error")
            $cordovaDialogs.alert(notification.msg, "Push notification error event");
        else $cordovaDialogs.alert(notification.event, "Push notification handler - Unprocessed Event");
    }

    // IOS Notification Received Handler
    $scope.handleIOS = function(notification) {
        if (notification.foreground == "1") {
            var messageString = JSON.stringify(notification.message)
            ApplicationFactory.showUserMessage("",notification.message,"",3000)
            // Play custom audio if a sound specified.
            /*if (notification.sound) {
                var mediaSrc = $cordovaMedia.newMedia(notification.sound);
                mediaSrc.promise.then($cordovaMedia.play(mediaSrc.media));
            }
            ApplicationFactory.showUserMessage("",messageString,"",3000)
            if (notification.message && notification.messageFrom) {
                ApplicationFactory.showUserMessage("",messageString,"",3000)
            }
            else $cordovaDialogs.alert(notification.alert, "Push Notification Received");

            if (notification.badge) {
                $cordovaPush.setBadgeNumber(notification.badge).then(function (result) {
                    console.log("Set badge success " + result)
                }, function (err) {
                    console.log("Set badge error " + err)
                });
            }*/
        }
        else {
            ApplicationFactory.showUserMessage("",notification.alert,"",3000)
           /* if (notification.body && notification.messageFrom) {
                $cordovaDialogs.alert(notification.body, "(RECEIVED WHEN APP IN BACKGROUND) " + notification.messageFrom);
            }
            else $cordovaDialogs.alert(notification.alert, "(RECEIVED WHEN APP IN BACKGROUND) Push Notification Received");*/
        }
    }
    $scope.storeDeviceToken = function(type) {
        var user = getUser();
        var userData = {};
        userData.mobileNumber = user.userName;
        userData.genieId = user.genieId;
        userData.gcmKey = $scope.regId
        userData.userType = user.userType;
        userData.deviceType = type;
        if(!checkStringItemNull(gcmKey)){
            gcmKey = ""
        }
        userData.previousGCMKey = gcmKey;
        if(gapi.client.commonutil===undefined){
            gapi.client.load('commonutil', 'v1', function() {
                $scope.androidGCM(userData, user, type)
            },getGapiURL() + '/_ah/api')
        }else{
            $scope.androidGCM(userData, user, type)
        }
    }

    $scope.androidGCM = function(userObject){
        gapi.client.commonutil.storeUserGCMKeys(userObject).execute(function(resp) {
            if (!resp.code){
                localStorage.setItem("GenieApp.gcmKey", resp.gcmSubscriptionKey);
                localStorage.setItem("GenieApp.gcmEnabled", true);
            }else{
                ApplicationFactory.showUserMessage("",resp.message,"",3000)
            }
        })
    }

    // Removes the device token from the db via node-pushserver API unsubscribe (running locally in this case).
    // If you registered the same device with different userids, *ALL* will be removed. (It's recommended to register each
    // time the app opens which this currently does. However in many cases you will always receive the same device token as
    // previously so multiple userids will be created with the same token unless you add code to check).
    function removeDeviceToken() {
        var tkn = {"token": $scope.regId};
        $http.post('http://192.168.1.16:8000/unsubscribe', JSON.stringify(tkn))
            .success(function (data, status) {
                console.log("Token removed, device is successfully unsubscribed and will not receive push notifications.");
            })
            .error(function (data, status) {
                console.log("Error removing device token." + data + " " + status)
            }
        );
    }

    // Unregister - Unregister your device token from APNS or GCM
    // Not recommended:  See http://developer.android.com/google/gcm/adv.html#unreg-why
    //                   and https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIApplication_Class/index.html#//apple_ref/occ/instm/UIApplication/unregisterForRemoteNotifications
    //
    // ** Instead, just remove the device token from your db and stop sending notifications **
    $scope.unregister = function () {
        console.log("Unregister called");
        removeDeviceToken();
        $scope.registerDisabled=false;
        //need to define options here, not sure what that needs to be but this is not recommended anyway
        //$cordovaPush.unregister(options).then(function(result) {
        //  console.log("Unregister success " + result);//
        //}, function(err) {
        //  console.log("Unregister error " + err)
        //});
    }

    $ionicPlatform.ready(function(device) {
        $scope.registerGCM();
    })
}])

genieControllers.controller('EditActivityController',['$scope', 'ApplicationFactory', 'ionicDatePicker', '$rootScope', '$state', 
                '$ionicHistory', '$ionicPopup', '$timeout', '$cordovaCamera', '$cordovaFileTransfer', '$cordovaCapture', 'ngProgressFactory', '$cordovaNetwork',
         function($scope, ApplicationFactory, ionicDatePicker, $rootScope, $state, $ionicHistory, $ionicPopup, $timeout, $cordovaCamera, 
            $cordovaFileTransfer, $cordovaCapture, ngProgressFactory, $cordovaNetwork) {

    var dateOptions1 = null;
    var dateOptions2 = null;
    var userGroupsData = ApplicationFactory.getUserGroupsData();
    var userRoles = ApplicationFactory.getUserRolesData();
    $scope.userLoginData = ApplicationFactory.getUserLoginData();
    $scope.userGroupList = [];
    var editActivityItem = null;
    $scope.userAnswers = {'pollingAnswers':[], 'completed':false}
    $scope.deleteAttachmentsList = [];
    $scope.tempAttachments = []
    $scope.activitySelectGroups = [];
    angular.element(document).ready(function(){
        $scope.progressbar = ngProgressFactory.createInstance();
        $scope.progressbar.setColor('#fff');
        var progressParent = $('.progress-bar-loader')
        $scope.progressbar.setParent(progressParent[0])
    })   
    $scope.failedAttachmentItems = 0;
    $scope.checkInternetConnection = function(){
        if(window.cordova){
            $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                $scope.completeProgressBarAction()
            })
        }else{
            window.addEventListener('offline',  function(){
                $scope.completeProgressBarAction()
            });
        }
    }
    $scope.$on("$ionicView.leave", function(scopes, states) {
        $scope.completeProgressBarAction()
    })
    $scope.$on("$ionicView.enter", function(scopes, states) {
        ApplicationFactory.setAppState('edit-activity');
        $scope.checkInternetConnection()
        if(window.cordova && parseInt(ionic.Platform.version())>5 &&  ionic.Platform.isAndroid()){
                //For Android versions greater than 5 
            cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
                for (var permission in statuses){
                    switch(statuses[permission]){
                        case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                            //$scope.downloadDeviceAttachments(appspotsite,fileURL, options, trustHosts)
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                            //alert("Permission to use "+permission+" has not been requested yet");
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.DENIED:
                            //alert("Permission denied to use "+permission+" - ask again?");
                            break;
                    }
                }
            }, function(error){
                ApplicationFactory.showUserMessage("","The following error occurred: "+error,"",3000);
            },[
                cordova.plugins.diagnostic.runtimePermission.CAMERA,
                cordova.plugins.diagnostic.runtimePermission.WRITE_EXTERNAL_STORAGE,
                cordova.plugins.diagnostic.runtimePermission.READ_EXTERNAL_STORAGE
            ]);
        }
        ApplicationFactory.showIonicLoader(false)
        userGroupsData = ApplicationFactory.getUserGroupsData();
        userRoles = ApplicationFactory.getUserRolesData();
        $scope.userLoginData = ApplicationFactory.getUserLoginData();
        if(!checkStringItemNull($scope.userLoginData.userType)){
            ApplicationFactory.hideIonicLoader()
            $state.go('user-roles')
            return true;
        }
        $scope.userGroups = userGroupsData;
        $scope.initData()
        ApplicationFactory.setApplicationStates();
        editActivityItem = ApplicationFactory.getEditActivityDetails()
        $scope.deleteAttachmentsList = [];
        $scope.tempAttachments = []
        if(checkObjectItemNull(editActivityItem)){
            $scope.getActivityItemInDetail(editActivityItem)
        }else{
            $scope.gotoPreviousState()
        }
    });
    $scope.gotoPreviousState = function(){
        var activityState = ApplicationFactory.getApplicationStates()
        if(activityState.backView==null){
            $state.go('activity-stream')
        }else{
            $ionicHistory.forwardView().stateName
        }
    }
    $scope.getLogoUrl = function(fileName){
        var filePath = ""
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            filePath = cordova.file.applicationDirectory + 'www/img/logo/'
        }else{
            filePath = "img/logo/"
        }
        return filePath + fileName;
    }
    $scope.getActivityItemInDetail = function(activityItem){
        var item = {}
        item.createdBy = $scope.userLoginData.genieId;
        item.userType = $scope.userLoginData.userType;
        item.activityId = activityItem.id;
        gapi.client.activitycalendar.getCalendarActivity(item).execute(function(resp) {
            $scope.$apply(function(){
                if(!resp.code){
                    editActivityItem = resp.result;
                    $scope.editActivity = resp.result;
                    $scope.setUserGroups(resp.result.groupIdList)
                    if(resp.result.isPolling){
                        $scope.setPollingOptions(resp.result.pollOptions, resp.result.pollingType)
                    }
                    $scope.setActivityDate(resp.result.endDate);
                    $scope.activityType = resp.result.activityType;
                }else{
                    $scope.editActivity = editActivityItem;
                }
                ApplicationFactory.setEditActivityDetails($scope.editActivity)
                ApplicationFactory.hideIonicLoader()
            })
        })
    }
    $scope.initData = function(){
        $scope.attachments = []
        if(window.cordova && (ionic.Platform.isAndroid() || ionic.Platform.isIOS())){
            $scope.attachments.push({'class':'attach-icon','fileItem':null, 'fileAdded':false,'accept':'*', 'type':'file','device':true})
            $scope.attachments.push({'class':'image-icon','fileItem':null, 'fileAdded':false, 'accept':'image', 'type':'file','device':true})
            $scope.attachments.push({'class':'video-icon','fileItem':null, 'fileAdded':false, 'accept':'video', 'type':'file','device':true})
        }else{
            $scope.attachments.push({'class':'attach-icon','fileItem':null, 'fileAdded':false,'accept':'*', 'type':'file','device':false})
            $scope.attachments.push({'class':'image-icon','fileItem':null, 'fileAdded':false, 'accept':'image', 'type':'file','device':false})
            $scope.attachments.push({'class':'video-icon','fileItem':null, 'fileAdded':false, 'accept':'video', 'type':'file','device':false})
        }
        $scope.polling = {enable:false, pollType:'single',options:[]}
        $scope.tempAttachments = [];
        $scope.deleteAttachments = [];
        $scope.activitySelectGroups = [];
        $scope.activityType = "";
    }
    $scope.openCamera = function(type){
        if(window.cordova){
            if(type=='image'){
                var options = {
                  quality: 50,
                  destinationType: Camera.DestinationType.FILE_URI,
                  sourceType: Camera.PictureSourceType.CAMERA,
                  encodingType: Camera.EncodingType.JPEG,
                  popoverOptions: CameraPopoverOptions,
                  saveToPhotoAlbum: true,
                  correctOrientation:true,
                };
                $cordovaCamera.getPicture(options).then(function(imageData) {
                    //ApplicationFactory.showIonicLoader(false)
                    ApplicationFactory.showProgressBarLoader("Optimizing and Attaching ...")
                    $scope.progressbar.start()
                    window.resolveLocalFileSystemURL(imageData, function success(fileEntry) {
                        fileEntry.file(function (file) {
                            var fileItem = file;
                            var item = {}
                            item.fileAdded = true;
                            item.fileItem = file
                            if(item.fileItem.type===undefined || item.fileItem.type==null){
                                item.fileItem.type="image/jpeg"
                            }
                            item.device = true;
                            if($scope.checkApplyInProgress()==false){
                                $scope.$apply(function(){
                                    $scope.pushAttachmentItem(item)
                                })
                            }else{
                                $scope.pushAttachmentItem(item)
                            }
                            $scope.completeProgressBarAction()
                            //ApplicationFactory.hideIonicLoader()
                        });
                    }, function (err) {
                        //ApplicationFactory.showIonicLoader()
                        window.resolveLocalFileSystemURL(cordova.file.externalRootDirectory, function success(dirEntry) {
                            dirEntry.getFile(imageData.replace(/^.*[\\\/]/, ''), { create: true, exclusive: false }, function (fileEntry) {
                                fileEntry.file(function (file) {
                                    var fileItem = file;
                                    var item = {}
                                    item.fileAdded = true;
                                    item.fileItem = file
                                    if(item.fileItem.type===undefined || item.fileItem.type==null){
                                        item.fileItem.type="image/jpeg"
                                    }
                                    item.device = true;
                                    if($scope.checkApplyInProgress()==false){
                                        $scope.$apply(function(){
                                            $scope.pushAttachmentItem(item)
                                        })
                                    }else{
                                        $scope.pushAttachmentItem(item)
                                    }
                                    $scope.completeProgressBarAction()
                                    //ApplicationFactory.hideIonicLoader()
                                });
                            }, function(err){
                                $scope.completeProgressBarAction()
                                ApplicationFactory.showUserMessage("",err,"",3000)
                                //ApplicationFactory.hideIonicLoader()
                            });
                        }, function(err){
                            $scope.completeProgressBarAction()
                            ApplicationFactory.showUserMessage("",err,"",3000)
                            //ApplicationFactory.hideIonicLoader()
                        });
                    });
                }, function(err) {
                    ApplicationFactory.showUserMessage("",err,"",3000)
                    //ApplicationFactory.hideIonicLoader()
                    $scope.completeProgressBarAction()
                });
            }else if(type=='video'){
                var options = {'limit':1}
                $cordovaCapture.captureVideo(options).then(function(mediaFiles) {
                    //ApplicationFactory.showIonicLoader(false)
                    ApplicationFactory.showProgressBarLoader("Optimizing and Attaching ...")
                    $scope.progressbar.start()
                    $timeout(function(){
                        var file = mediaFiles[0];
                        var videoFileName = ""
                        var lastDotPosition = file.name.lastIndexOf(".");
                        if (lastDotPosition === -1){
                            videoFileName = file.name;
                        }else{ 
                            videoFileName =  file.name.substr(0, lastDotPosition);
                        }
                        var videoOptions = {}
                        videoOptions.fileUri = file.fullPath
                        videoOptions.outputFileName = new Date().getTime() + "_" + videoFileName
                        videoOptions.outputFileType = VideoEditorOptions.OutputFileType.MPEG4
                        videoOptions.optimizeForNetworkUse = VideoEditorOptions.OptimizeForNetworkUse.YES
                        videoOptions.saveToLibrary = true
                        videoOptions.maintainAspectRatio = true
                        videoOptions.width = 640
                        videoOptions.height = 640
                        videoOptions.videoBitrate = 1000000 // 1 megabit
                        videoOptions.audioChannels = 2
                        videoOptions.audioSampleRate = 44100
                        videoOptions.audioBitrate = 128000 // 128 kilobits
                        videoOptions.progress = function(info){
                            /*var item = $("#gn-app-loader")
                            if(checkStringItemNull(item)){
                                if($("#gn-app-loader").css("display").toLowerCase()=="none"){
                                    ApplicationFactory.showIonicLoader(false)
                                }
                            }*/
                            if($scope.progressbar.status()<=0){
                                $scope.progressbar.start() 
                            }
                        }
                        VideoEditor.transcodeVideo(function(result){
                            window.resolveLocalFileSystemURL("file://" + result, function success(fileEntry) {
                                fileEntry.file(function (file) {
                                    var fileItem = file;
                                    var item = {}
                                    item.fileAdded = true;
                                    item.fileItem = file
                                    item.device = true;
                                    if(item.fileItem.type===undefined || item.fileItem.type==null){
                                        item.fileItem.type="video/quicktime"
                                    }
                                    if($scope.checkApplyInProgress()==false){
                                        $scope.$apply(function(){
                                            $scope.pushAttachmentItem(item)
                                        })
                                    }else{
                                        $scope.pushAttachmentItem(item)
                                    }
                                    $scope.completeProgressBarAction()
                                    //ApplicationFactory.hideIonicLoader()
                                });
                            }, function (err) {
                                window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function success(dirEntry) {
                                    dirEntry.getFile(result.replace(/^.*[\\\/]/, ''), { create: true, exclusive: false }, function (fileEntry) {
                                        fileEntry.file(function (file) {
                                            var fileItem = file;
                                            var item = {}
                                            item.fileAdded = true;
                                            item.fileItem = file
                                            if(item.fileItem.type===undefined || item.fileItem.type==null){
                                                item.fileItem.type="video/quicktime"
                                            }
                                            item.device = true;
                                            if($scope.checkApplyInProgress()==false){
                                                $scope.$apply(function(){
                                                    $scope.pushAttachmentItem(item)
                                                })
                                            }else{
                                                $scope.pushAttachmentItem(item)
                                            }
                                            $scope.completeProgressBarAction()
                                            //ApplicationFactory.hideIonicLoader()
                                        });
                                    }, function(err){
                                        ApplicationFactory.showUserMessage("",err,"",3000)
                                        //ApplicationFactory.hideIonicLoader()
                                        $scope.completeProgressBarAction()
                                    })
                                }, function(err){
                                    ApplicationFactory.showUserMessage("",err,"",3000)
                                    //ApplicationFactory.hideIonicLoader()
                                    $scope.completeProgressBarAction()
                                });
                            });
                        }, function(err){
                            ApplicationFactory.showUserMessage("",err,"",3000)
                            //ApplicationFactory.hideIonicLoader()
                            $scope.completeProgressBarAction()
                        },videoOptions)
                    }, 200)
                }, function(err) {
                    ApplicationFactory.showUserMessage("", window.userMessages.VIDEO_MODE_NOT_AVAILABLE, "", 3000)
                    //ApplicationFactory.hideIonicLoader()
                    $scope.completeProgressBarAction()
                });
            }
        }
    }
    $scope.completeProgressBarAction = function(){
        if(!$scope.checkApplyInProgress()){
            $scope.$apply(function(){
                $scope.progressbar.complete()
                ApplicationFactory.hideProgressBarLoader()
            })
        }else{
            $scope.progressbar.complete()
            ApplicationFactory.hideProgressBarLoader()
        }
    }
    $scope.checkApplyInProgress = function(){
        var phase = $scope.$root.$$phase
        if(phase==null){
            return false
        }else{
            return true
        }
    }
    $scope.setAttachments = function(attachments){
        $scope.tempAttachments = attachments;
    }
    $scope.setActivityDate = function(date){
        dateOptions1 = {
            callback: function (val) {
                $scope.editActivity.endDate =  new Date(val);
            },
            disabledDates: [],
            disableWeekdays:[],
            closeOnSelect:true,
            from: new Date(), 
            inputDate: new Date(date),
            mondayFirst: true,          
            templateType: 'model'       
        };
    }
    $scope.setPollingOptions = function(pollOptions, pollType){
        $scope.polling.pollType = pollType;
        $scope.polling.enable = true;
        angular.forEach(pollOptions, function(option, key){
            $scope.polling.options.push({'key':key+1,'option':option})
        });
    }
    $scope.setUserGroups = function(groupIds){
        userGroupsData = ApplicationFactory.getUserGroupsData()
        userLoginData = ApplicationFactory.getUserLoginData()
        userRoles = ApplicationFactory.getUserRolesData()
        var writeGroupList = getLoggedUserWriteGroupList(userRoles, userLoginData.instituteId, userLoginData.userType)
        $scope.userGroupList = [];
        angular.forEach(writeGroupList, function(value, key){
            var group = _.findWhere(userGroupsData, {'id':value})
            if(checkObjectItemNull(group)){
                $scope.userGroupList.push(group)
            }
        });
        angular.forEach(groupIds, function(value, key){
            var group = _.findWhere(userGroupsData, {'id':value})
            if(checkObjectItemNull(group)){
                $scope.addGroups(group)
            }
        });
        return $scope.activitySelectGroups
    }
    $scope.addGroups = function(groupName){
        var group = _.findWhere($scope.activitySelectGroups,{'id':groupName.id})
        var flag = true;
        if(!checkObjectItemNull(group)){
            $scope.activitySelectGroups.push(groupName)
        }
    }
    $scope.getAvailableGroupList = function($query){
        $scope.setUserGroups();
        if(!checkStringItemNull($query)){
            return $scope.userGroupList;
        }
        return $scope.userGroupList.filter(function(item){
            return (item.displayGroupName).toLowerCase().indexOf($query.toLowerCase()) !==-1
        })
    }
    $scope.openStartDatePicker = function(){
        ionicDatePicker.openDatePicker(dateOptions1);
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.close();
        }
    };
    $scope.checkFormIsValid = function(){
        if(!checkStringItemNull($scope.editActivity)  || !checkStringItemNull($scope.editActivity.activityType) ||
                !checkStringItemNull($scope.editActivity.description) ||
                (!checkArrayItemNull($scope.activitySelectGroups) &&  !checkArrayItemNull($scope.editActivity.userIdsList))){
            return true;
        }else{
            return false;
        }
    }
    $scope.removeAttachment = function(item, index, state){
        var confirmPopup = $ionicPopup.confirm({
            template: window.userMessages.DELETE_ATTACHMENT
        });
        confirmPopup.then(function(res) {
            if(res) {
                if(state=='new'){
                    $scope.tempAttachments.splice(index, 1)
                }else if(state=='previous'){
                    $scope.deleteAttachmentsList.push(item.id);
                    $scope.editActivity.attachments.splice(index, 1)
                }
            }
        });
    }
    $scope.pushAttachmentItem = function(item){
        if(!checkObjectItemNull($scope.tempAttachments)){
            $scope.tempAttachments = [];
        }
        $scope.tempAttachments.push(item)
    }
    $scope.checkAttachmentExist = function(){
        return $scope.tempAttachments
    }
    $scope.isPollingOptionsExist = function(){
        var temp = [];
        for(var i=0; i<$scope.polling.options.length; i++){
            if(checkStringItemNull($scope.polling.options[i].option)){
                temp.push($scope.polling.options[i])
            }
        }
        return temp;
    }
    $scope.getActivityType = function(activityType){
        if(!checkStringItemNull($scope.editActivity)){
            return false
        }
        if($scope.editActivity.activityType==activityType){
            return true
        }else{
            return false
        }
    }
    $scope.setActivityType = function(activityType){
        $scope.editActivity.activityType = activityType;
        return activityType;
    }
      
    $scope.goPreviousState = function(){
        var activityState = ApplicationFactory.getApplicationStates()
        if(activityState.backView==null){
            $state.go('activity-stream', {'refresh':true})
        }else{
            $ionicHistory.forwardView().stateName
        }
    }
    $scope.showEndDate = function(){
        if($scope.editActivity.activityType=='assignment'){
            return 'flex-display'
        }else{
            return 'hide'
        }
    }
    $scope.enablePolling = function(){
        if($scope.polling.enable){
            $scope.polling.enable = false
            $scope.polling.options = []
            $scope.editActivity.activityType = ""
        }else{  
            $scope.polling.enable = true
            $scope.editActivity.activityType = "note"
            $scope.polling.options.push({'key':$scope.polling.options.length+1,'option':''})
        }
    }
    $scope.selectPollingType = function(pollType){
        $scope.polling.pollType = pollType
    }
    $scope.addMorePollingOptions = function(){
        $scope.polling.options.push({'key':$scope.polling.options.length+1,'option':''})
    }
    $scope.removePollingOption = function(option ,index){
        var confirmPopup = $ionicPopup.confirm({
            template: window.userMessages.DELETE_POLL
        });
        confirmPopup.then(function(res) {
            if(res) {
                if($scope.polling.options.length==1){
                    $scope.polling.options = [];
                    $scope.polling.pollType = "";
                    $scope.polling.enable = false;
                }else{
                    $scope.polling.options.splice(index, 1)
                }
            }
        });
    }
    $scope.confirmSave = function(){
        $scope.saveActivity();
    }
    $scope.getAttachmentUrl = function(attachmentURL){
        return ApplicationFactory.generateFileAttachment(attachmentURL);
    }
    $scope.downloadAttachment = function(attachment){
        downloadAttachment(attachment)
    }
    $scope.saveActivity = function(){
        ApplicationFactory.showIonicLoader(false)
        $scope.formSubmitted = true;
        var editActivity = {}
        editActivity.id = $scope.editActivity.id;
        var groupIds = [];
        if($scope.activitySelectGroups.length>0){
            angular.forEach($scope.activitySelectGroups, function(group){
                if(groupIds.indexOf(group.id)===-1){
                    groupIds.push(group.id)
                }
            })
        }
        $scope.tempAttachments = $scope.checkAttachmentExist()
        if($scope.tempAttachments.length>0){
            editActivity.isAttachments = true
        }else{
            editActivity.isAttachments = false
        }
        if(editActivity.isAttachments==false && $scope.editActivity.attachments){
            editActivity.isAttachments = true
        }
        $scope.pollOptions = $scope.isPollingOptionsExist()
        if($scope.pollOptions.length>0){
            editActivity.pollingOptions = []
            angular.forEach($scope.pollOptions, function(item){
                editActivity.pollingOptions.push(item.option)
            })
            editActivity.isPolling = true;
            editActivity.pollingType = $scope.polling.pollType;
        }else{
            editActivity.pollingOptions = [];
            editActivity.isPolling = false;
            editActivity.pollingType = '';
        }        
        $scope.editActivity.endDate = new Date(setEndDate($scope.editActivity.endDate))
        var tempDate = new Date(setEndDate(new Date()))
        if(tempDate.getTime()>$scope.editActivity.endDate.getTime()){
            ApplicationFactory.showUserMessage("", window.userMessages.END_DATE_SHORTER,"",3000)
            ApplicationFactory.hideIonicLoader()
            return true
        }
        $scope.editActivity.startDate = new Date(setStartDate($scope.editActivity.endDate))
        if(checkObjectItemNull($scope.editActivity.userIdsList)){
            editActivity.userIdsList = $scope.editActivity.userIdsList
        }else{
            editActivity.userIdsList = [];
        }
        if(checkObjectItemNull($scope.deleteAttachmentsList)){
            editActivity.deleteAttachments = $scope.deleteAttachmentsList;
        }else{
            editActivity.deleteAttachments = [];
        }  
        editActivity.activityOwner = $scope.userLoginData.genieId;
        editActivity.startDate = $scope.editActivity.startDate;
        editActivity.endDate = $scope.editActivity.endDate;
        editActivity.activityType = $scope.editActivity.activityType;
        editActivity.title = "";
        editActivity.action = "";
        editActivity.description = $scope.editActivity.description;
        editActivity.groupIdsList = _.uniq(groupIds);

        if(gapi.client.activitycalendar===undefined){
            gapi.client.load('activitycalendar', 'v1', function() {
                $scope.saveEntity(editActivity)
            }, getGapiURL() + '/_ah/api')
        }else{
            $scope.saveEntity(editActivity)
        }
    }   
    $scope.saveEntity = function(editActivity){
        gapi.client.activitycalendar.editActivity(editActivity).execute(function(resp) {
            $scope.$apply(function(){
                if(!resp.code){
                    $scope.activity = resp.result
                    if($scope.tempAttachments.length>0){
                        var index = 0;
                        $scope.uploadAttachment(resp.result.id, index)
                    }else{
                        ApplicationFactory.hideIonicLoader()
                        $scope.showConfirmMessage(resp.result)
                        var activityState = ApplicationFactory.getApplicationStates()
                        if(activityState.backView==null){
                            $state.go('activity-stream')
                        }else{
                            $ionicHistory.goBack()
                        }
                    }
                }else{
                    // show error message"",resp.message
                    ApplicationFactory.showUserMessage("",resp.message,"",3000)
                }
            })
        })
    }
    $scope.uploadAttachment = function(activityId, index){
        ApplicationFactory.showIonicLoader(false)
        $scope.index = index;
        $scope.activityId = activityId;
        if(!checkStringItemNull($scope.failedAttachmentItems)){
            $scope.failedAttachmentItems = 0
        }
        var data = {'fileName':$scope.tempAttachments[index].fileItem.name,'uploadAction':'upload'}
        gapi.client.activitycalendar.createUploadBlobUrl(data).execute(
            function(resp) {
                $scope.$apply(function(){
                    ApplicationFactory.hideIonicLoader()
                    ApplicationFactory.showProgressBarLoader("Sending ...")
                    if (!resp.code) {
                        if($scope.tempAttachments[index].device!=true){
                            var formData = new FormData();
                            formData.append('attachment',$scope.tempAttachments[index].fileItem);
                            formData.append('activityId',activityId);
                            $.ajax({
                                url: resp.url,
                                type: "POST",
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                xhr: function(){  // Custom XMLHttpRequest
                                    var myXhr = $.ajaxSettings.xhr();
                                    myXhr.upload.onprogress = function(e) {
                                        if (e.lengthComputable) {
                                            var uploadStatus = ((e.loaded / e.total)/($scope.tempAttachments.length - $scope.failedAttachmentItems)) * 100;
                                            $scope.progressbar.set(uploadStatus)
                                        }
                                    };
                                    return myXhr;
                                },
                            }).success(function(data) {
                                $scope.checkUploadCompleted(activityId)
                            }).error(function(data){
                                $scope.failedAttachmentItems += 1
                                $scope.checkUploadCompleted(activityId)
                            });
                        }else{
                            var server = resp.url
                            var filePath = $scope.tempAttachments[index].fileItem.localURL
                            var options = {'fileKey':'attachment',
                                            'params':{'activityId':activityId},
                                            'fileName':$scope.tempAttachments[index].fileItem.name,
                                            'mimeType':$scope.tempAttachments[index].fileItem.type}
                            $cordovaFileTransfer.upload(server, filePath, options).then(function(result) {
                                $scope.checkUploadCompleted(activityId)
                            }, function(err) {
                                $scope.failedAttachmentItems += 1
                                $scope.checkUploadCompleted(activityId)
                            }, function (progress) {
                                $timeout(function () {
                                    var uploadStatus = ((progress.loaded / progress.total)/($scope.tempAttachments.length - $scope.failedAttachmentItems)) * 100;
                                    $scope.progressbar.set(uploadStatus)
                                });
                            });

                        }
                    }else{
                        $scope.failedAttachmentItems += 1
                        $scope.checkUploadCompleted(activityId)
                    }
                })
            })      
    }
    $scope.checkApplyInProgress = function(){
        var phase = $scope.$root.$$phase
        if(phase==null){
            return false
        }else{
            return true
        }
    }
    $scope.showFileTypeImage = function(mimeType){
        return ApplicationFactory.showFileType(mimeType)
    }
    $scope.checkUploadCompleted = function(activityId){
        if($scope.index < $scope.tempAttachments.length - 1){
            $scope.index += 1;
            $scope.uploadAttachment($scope.activityId, $scope.index);
        }else{
            var activityState = ApplicationFactory.getApplicationStates()
            if($scope.failedAttachmentItems==$scope.tempAttachments.length){
                var tempData = {}
                tempData.id = activityId
                gapi.client.activitycalendar.uploadActivityAttachmentFailed(tempData).execute(function(resp) {
                    if (!resp.code){
                        ApplicationFactory.showUserMessage("",window.userMessages.ATTACHMENT_UPLOAD_FAILED,"",3000);
                    }else{
                        ApplicationFactory.showUserMessage("",window.userMessages.ATTACHMENT_UPLOAD_FAILED,"",3000);
                    }
                    $scope.completeProgressBarAction()
                    //ApplicationFactory.hideIonicLoader()
                    if(activityState.backView==null){
                        $state.go('activity-stream')
                    }else{
                        $ionicHistory.goBack()
                    }
                })
            }else{
                if(activityState.backView==null){
                    $state.go('activity-stream')
                }else{
                    $ionicHistory.goBack()
                }
                $scope.completeProgressBarAction()
                //ApplicationFactory.hideIonicLoader()
                $scope.showConfirmMessage($scope.activity)
            }
        }
    }
    $scope.showConfirmMessage = function(){
        var confirmPopup = $ionicPopup.confirm({
            template: window.userMessages.ACTIVITY_UPDATED,
            buttons: [{text: '<b>Edit</b>',type: 'button-positive',
                    onTap: function(e) {
                        if ($scope.activity) {
                            ApplicationFactory.setEditActivityDetails($scope.activity)
                            $state.go('edit-activity')
                        }
            }}]
        });
        $timeout(function() {
            confirmPopup.close()
        }, 3000)
    }
}])

genieControllers.controller('CreateActivityController',['$scope', 'ApplicationFactory', 'ionicDatePicker', '$rootScope', '$state', 
        '$ionicHistory', '$ionicPopup', '$timeout', '$cordovaCamera', '$cordovaFileTransfer', '$cordovaCapture', 'ngProgressFactory', '$cordovaNetwork',
         function($scope, ApplicationFactory, ionicDatePicker, $rootScope, $state, $ionicHistory, $ionicPopup, $timeout, $cordovaCamera,
             $cordovaFileTransfer, $cordovaCapture, ngProgressFactory, $cordovaNetwork) {
    var dateOptions1 = null;
    var dateOptions2 = null;
    var userGroupsData = ApplicationFactory.getUserGroupsData();
    var userRoles = ApplicationFactory.getUserRolesData();
    $scope.userLoginData = ApplicationFactory.getUserLoginData();
    $scope.userGroupList = [];
    $scope.activitySelectGroups = []
    angular.element(document).ready(function(){
        $scope.progressbar = ngProgressFactory.createInstance();
        $scope.progressbar.setColor('#fff');
        var progressParent = $('.progress-bar-loader')
        $scope.progressbar.setParent(progressParent[0])
    }) 
    $scope.checkInternetConnection = function(){
        if(window.cordova){
            $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                $scope.completeProgressBarAction()
            })
        }else{
            window.addEventListener('offline',  function(){
                $scope.completeProgressBarAction()
            });
        }
    }
    $scope.initData = function(){
        dateOptions1 = {
                callback: function (val) {
                    $scope.createActivity.endDate =  new Date(val);
                },
                disabledDates: [],
                disableWeekdays:[],
                closeOnSelect:true,
                from: new Date(), 
                inputDate: new Date(),      //Optional
                mondayFirst: true,          //Optional
                templateType: 'model'       //Optional
        };
        $scope.createActivity = {
                "id" : null,
                "activityOwner": "",
                "startDate": new Date(),
                "endDate": new Date(),
                "activityType": "note",
                "pollingOptions": [],
                "deleteAttachments": [],
                "action": "",
                "title": "",
                "description": "",
                "groupIdsList":[],
                "userIdsList":[],
                "isAttachments":false,
                "isPolling":false,
                "pollingType":'single'
        };
        $scope.activitySelectGroups = [];
        $scope.formSubmitted = false;
        $scope.attachments = []
        if(window.cordova && (ionic.Platform.isAndroid() || ionic.Platform.isIOS())){
            $scope.attachments.push({'class':'attach-icon','fileItem':null, 'fileAdded':false,'accept':'*', 'type':'file','device':true})
            $scope.attachments.push({'class':'image-icon','fileItem':null, 'fileAdded':false, 'accept':'image', 'type':'file','device':true})
            $scope.attachments.push({'class':'video-icon','fileItem':null, 'fileAdded':false, 'accept':'video', 'type':'file','device':true})
        }else{
            $scope.attachments.push({'class':'attach-icon','fileItem':null, 'fileAdded':false,'accept':'*', 'type':'file','device':false})
            $scope.attachments.push({'class':'image-icon','fileItem':null, 'fileAdded':false, 'accept':'image', 'type':'file','device':false})
            $scope.attachments.push({'class':'video-icon','fileItem':null, 'fileAdded':false, 'accept':'video', 'type':'file','device':false})
        }
        $scope.polling = {enable:false, pollType:'single',options:[]}
        $scope.tempAttachments = [];
    }
    $scope.editActivity = function(activityDetailItem){
        Application.setEditActivityDetails(activityDetailItem)
        $state.go('edit-activity')
    }
    $scope.getLogoUrl = function(fileName){
        var filePath = ""
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            filePath = cordova.file.applicationDirectory + 'www/img/logo/'
        }else{
            filePath = "img/logo/"
        }
        return filePath + fileName;
    }
    $scope.openCamera = function(type){
        if(window.cordova){
            if(type=='image'){
                var options = {
                  quality: 50,
                  destinationType: Camera.DestinationType.FILE_URI,
                  sourceType: Camera.PictureSourceType.CAMERA,
                  encodingType: Camera.EncodingType.JPEG,
                  popoverOptions: CameraPopoverOptions,
                  saveToPhotoAlbum: true,
                  correctOrientation:true,
                };
                $cordovaCamera.getPicture(options).then(function(imageData) {
                    //ApplicationFactory.showIonicLoader(false)
                    ApplicationFactory.showProgressBarLoader("Optimizing and Attaching ...")
                    $scope.progressbar.start()
                    window.resolveLocalFileSystemURL(imageData, function success(fileEntry) {
                        fileEntry.file(function (file) {
                            var fileItem = file;
                            var item = {}
                            item.fileAdded = true;
                            item.fileItem = file
                            if(item.fileItem.type===undefined || item.fileItem.type==null){
                                item.fileItem.type = "image/jpeg"
                            }
                            item.device = true;
                            if($scope.checkApplyInProgress()==false){
                                $scope.$apply(function(){
                                    $scope.pushAttachmentItem(item)
                                })
                            }else{
                                $scope.pushAttachmentItem(item)
                            }
                            $scope.completeProgressBarAction()
                            //ApplicationFactory.hideIonicLoader()
                        });
                    }, function (err) {
                        //ApplicationFactory.showIonicLoader()
                        window.resolveLocalFileSystemURL(cordova.file.cacheDirectory, function success(dirEntry) {
                            dirEntry.getFile(imageData.replace(/^.*[\\\/]/, ''), { create: true, exclusive: false }, function (fileEntry) {
                                fileEntry.file(function (file) {
                                    var fileItem = file;
                                    var item = {}
                                    item.fileAdded = true;
                                    item.fileItem = file
                                    if(item.fileItem.type===undefined || item.fileItem.type==null){
                                        item.fileItem.type = "image/jpeg"
                                    }
                                    item.device = true;
                                    if($scope.checkApplyInProgress()==false){
                                        $scope.$apply(function(){
                                            $scope.pushAttachmentItem(item)
                                        })
                                    }else{
                                        $scope.pushAttachmentItem(item)
                                    }
                                    $scope.completeProgressBarAction()
                                    //ApplicationFactory.hideIonicLoader()
                                });
                            }, function(err){
                                ApplicationFactory.showUserMessage("",err,"",3000)
                                $scope.completeProgressBarAction()
                                //ApplicationFactory.hideIonicLoader()
                            });
                        }, function(err){
                            ApplicationFactory.showUserMessage("",err,"",3000)
                            $scope.completeProgressBarAction()
                            //ApplicationFactory.hideIonicLoader()
                        });
                    });
                }, function(err) {
                    ApplicationFactory.showUserMessage("",err,"",3000)
                    $scope.completeProgressBarAction()
                    //ApplicationFactory.hideIonicLoader()
                });
            }else if(type=='video'){
                var options = {'limit':1}
                $cordovaCapture.captureVideo(options).then(function(mediaFiles) {
                    //ApplicationFactory.showIonicLoader(false)
                    ApplicationFactory.showProgressBarLoader("Optimizing and Attaching ...")
                    $scope.progressbar.start()
                    $timeout(function(){
                        var file = mediaFiles[0];
                        var videoFileName = ""
                        var lastDotPosition = file.name.lastIndexOf(".");
                        if (lastDotPosition === -1){
                            videoFileName = file.name;
                        }else{ 
                            videoFileName =  file.name.substr(0, lastDotPosition);
                        }
                        var videoOptions = {}
                        videoOptions.fileUri = file.fullPath
                        videoOptions.outputFileName = new Date().getTime() + "_" + videoFileName
                        videoOptions.outputFileType = VideoEditorOptions.OutputFileType.MPEG4
                        videoOptions.optimizeForNetworkUse = VideoEditorOptions.OptimizeForNetworkUse.YES
                        videoOptions.saveToLibrary = true
                        videoOptions.maintainAspectRatio = true
                        videoOptions.width = 640
                        videoOptions.height = 640
                        videoOptions.videoBitrate = 1000000 // 1 megabit
                        videoOptions.audioChannels = 2
                        videoOptions.audioSampleRate = 44100
                        videoOptions.audioBitrate = 128000 // 128 kilobits
                        videoOptions.progress = function(info){
                            /*var item = $("#gn-app-loader")
                            if(checkStringItemNull(item)){
                                if($("#gn-app-loader").css("display").toLowerCase()=="none"){
                                    ApplicationFactory.showIonicLoader(false)
                                }
                            }*/
                            if($scope.progressbar.status()<=0){
                                $scope.progressbar.start()
                            }
                        }
                        VideoEditor.transcodeVideo(function(result){
                            var path = "file://" + result
                            window.resolveLocalFileSystemURL(path, function success(fileEntry) {
                                fileEntry.file(function (file) {
                                    var fileItem = file;
                                    var item = {}
                                    item.fileAdded = true;
                                    item.fileItem = file
                                    if(item.fileItem.type===undefined || item.fileItem.type==null){
                                        item.fileItem.type = "video/quicktime"
                                    }
                                    item.device = true;
                                    if($scope.checkApplyInProgress()==false){
                                        $scope.$apply(function(){
                                            $scope.pushAttachmentItem(item)
                                        })
                                    }else{
                                        $scope.pushAttachmentItem(item)
                                    }
                                    $scope.completeProgressBarAction()
                                    //ApplicationFactory.hideIonicLoader()
                                });
                            }, function (err) {
                                var path = ""
                                if(ionic.Platform.isAndroid()){
                                    path = cordova.file.cacheDirectory
                                }else if(ionic.Platform.isIOS()){
                                    path = cordova.file.documentsDirectory
                                }
                                window.resolveLocalFileSystemURL(path, function success(dirEntry) {
                                    dirEntry.getFile(result.replace(/^.*[\\\/]/, ''), { create: true, exclusive: false }, function (fileEntry) {
                                        fileEntry.file(function (file) {
                                            var fileItem = file;
                                            var item = {}
                                            item.fileAdded = true;
                                            item.fileItem = file
                                            if(item.fileItem.type===undefined || item.fileItem.type==null){
                                                item.fileItem.type = "video/quicktime"
                                            }
                                            item.device = true;
                                            if($scope.checkApplyInProgress()==false){
                                                $scope.$apply(function(){
                                                    $scope.pushAttachmentItem(item)
                                                })
                                            }else{
                                                $scope.pushAttachmentItem(item)
                                            }
                                            $scope.completeProgressBarAction()
                                            //ApplicationFactory.hideIonicLoader()
                                        });
                                    }, function(err){
                                        ApplicationFactory.showUserMessage("",err,"",3000)
                                        $scope.completeProgressBarAction()
                                        //ApplicationFactory.hideIonicLoader()
                                    })
                                }, function(err){
                                    ApplicationFactory.showUserMessage("",err,"",3000)
                                    $scope.completeProgressBarAction()
                                    //ApplicationFactory.hideIonicLoader()
                                });
                            });
                        }, function(err){
                            ApplicationFactory.showUserMessage("",err,"",3000)
                            $scope.completeProgressBarAction()
                            //ApplicationFactory.hideIonicLoader()
                        },videoOptions)
                    }, 200)
                }, function(err) {
                    ApplicationFactory.showUserMessage("", window.userMessages.VIDEO_MODE_NOT_AVAILABLE, "", 3000)
                    $scope.completeProgressBarAction()
                    //ApplicationFactory.hideIonicLoader()
                });
            }
        }
    }
    $scope.checkApplyInProgress = function(){
        var phase = $scope.$root.$$phase
        if(phase==null){
            return false
        }else{
            return true
        }
    }
    $scope.enablePolling = function(){
        if($scope.polling.enable){
            $scope.polling.enable = false
            $scope.polling.options = []
            $scope.createActivity.activityType = ""
        }else{  
            $scope.polling.enable = true
            $scope.createActivity.activityType = "note"
            $scope.polling.options.push({'key':$scope.polling.options.length+1,'option':''})
        }
    }
    $scope.selectPollingType = function(pollType){
        $scope.polling.pollType = pollType
    }
    $scope.addMorePollingOptions = function(){
        $scope.polling.options.push({'key':$scope.polling.options.length+1,'option':''})
    }
    $scope.removePollingOption = function(option ,index){
        if($scope.polling.options.length==1){
            $scope.polling.options = [];
            $scope.polling.options.push({'key':$scope.polling.options.length+1,'option':''})
        }else{
            $scope.polling.options.splice(index, 1)
        }
    }
    $scope.initData()
    $scope.$on("$ionicView.leave", function(scopes, states) {
        $scope.completeProgressBarAction()
    })
    $scope.$on("$ionicView.enter", function(scopes, states) {
        //$scope.checkInternetConnection()
        if(window.cordova && parseInt(ionic.Platform.version())>5 &&  ionic.Platform.isAndroid()){
                //For Android versions greater than 5 
            cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
                for (var permission in statuses){
                    switch(statuses[permission]){
                        case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                            //$scope.downloadDeviceAttachments(appspotsite,fileURL, options, trustHosts)
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                            //alert("Permission to use "+permission+" has not been requested yet");
                            break;
                        case cordova.plugins.diagnostic.permissionStatus.DENIED:
                            //alert("Permission denied to use "+permission+" - ask again?");
                            break;
                    }
                }
            }, function(error){
                ApplicationFactory.showUserMessage("","The following error occurred: "+error,"",3000);
            },[
                cordova.plugins.diagnostic.runtimePermission.CAMERA,
                cordova.plugins.diagnostic.runtimePermission.READ_EXTERNAL_STORAGE,
                cordova.plugins.diagnostic.runtimePermission.WRITE_EXTERNAL_STORAGE
            ]);
        }
        ApplicationFactory.showIonicLoader(false)
        ApplicationFactory.setAppState('create-activity');
        userGroupsData = ApplicationFactory.getUserGroupsData();
        userRoles = ApplicationFactory.getUserRolesData();
        $scope.userLoginData = ApplicationFactory.getUserLoginData();
        if(!checkStringItemNull($scope.userLoginData.userType)){
            ApplicationFactory.hideIonicLoader()
            $state.go('user-roles')
            return true;
        }
        $scope.userGroups = userGroupsData;
        $scope.initData();
        $scope.setUserGroups()
        ApplicationFactory.setApplicationStates();
        ApplicationFactory.hideIonicLoader()
    });
    $scope.addGroups = function(groupName){
        var group = _.findWhere($scope.activitySelectGroups,{'id':groupName.id})
        var flag = true;
        if(checkObjectItemNull(group)){
            angular.forEach($scope.activitySelectGroups, function(value, key){
                if(value.id==group.id){
                    flag = false
                }
            });
        }
    }
    $scope.setUserGroups = function(){
        userGroupsData = ApplicationFactory.getUserGroupsData()
        $scope.userLoginData = ApplicationFactory.getUserLoginData()
        userRoles = ApplicationFactory.getUserRolesData();
        if($scope.userLoginData.userType=='parent'){
            if(checkObjectItemNull(ApplicationFactory.getChindrensData())){
                $scope.userLoginData.userGroupList = getChildGroups(ApplicationFactory.getChindrensData());
            }else{
                $scope.userLoginData.userGroupList = [];
            }
        }else{
            var userRolesData = ApplicationFactory.getUserRolesData()
            $scope.userLoginData.userGroupList = getLoggedUserWriteGroupList(userRoles, $scope.userLoginData.instituteId, $scope.userLoginData.userType)
        }
        $scope.userGroupList = [];
        angular.forEach($scope.userLoginData.userGroupList, function(value, key){
            var group = _.findWhere(userGroupsData, {'id':value})
            if(checkObjectItemNull(group)){
                $scope.userGroupList.push(group)
            }
        });
        return $scope.userGroupList;
    }
    $scope.getUserGroups = function($query){
        var items = $scope.setUserGroups();
        if(!checkStringItemNull($query)){
            return items;
        }
        return items.filter(function(item){
            return (item.displayGroupName).toLowerCase().indexOf($query.toLowerCase()) !==-1
        })
    }
    $scope.openStartDatePicker = function(){
        ionicDatePicker.openDatePicker(dateOptions1);
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.close();
        }
    };
    $scope.checkFormIsValid = function(){
        if(!checkStringItemNull($scope.createActivity)  || !checkStringItemNull($scope.createActivity.activityType) ||
                !checkStringItemNull($scope.createActivity.description) ||
                (!checkArrayItemNull($scope.activitySelectGroups) &&  !checkArrayItemNull($scope.createActivity.userIdsList))){
            return true;
        }else{
            return false;
        }
    }
    $scope.removeAttachment = function(item, index){
        var items = $scope.tempAttachments;
        items.splice(index, 1)
        $scope.tempAttachments = items
        return $scope.tempAttachments
    }
    $scope.pushAttachmentItem = function(item){
        if(!checkObjectItemNull($scope.tempAttachments)){
            $scope.tempAttachments = [];
        }
        $scope.tempAttachments.push(item)
    }
    $scope.checkAttachmentExist = function(){
        return $scope.tempAttachments
    }
    $scope.isPollingOptionsExist = function(){
        var temp = [];
        for(var i=0; i<$scope.polling.options.length; i++){
            if(checkStringItemNull($scope.polling.options[i].option)){
                temp.push($scope.polling.options[i])
            }
        }
        return temp;
    }
    $scope.confirmSave = function(){
        $scope.saveActivity();
    }
    $scope.saveActivity = function(){
        ApplicationFactory.showIonicLoader(false)
        $scope.formSubmitted = true;
        $scope.tempAttachments = $scope.checkAttachmentExist()
        $scope.pollOptions = $scope.isPollingOptionsExist()
        if($scope.activitySelectGroups.length>0){
            angular.forEach($scope.activitySelectGroups, function(group){
                $scope.createActivity.groupIdsList.push(group.id)
            })
        }
        if($scope.tempAttachments.length>0){
            $scope.createActivity.isAttachments = true
        }
        if($scope.pollOptions.length>0){
            angular.forEach($scope.pollOptions, function(item){
                $scope.createActivity.pollingOptions.push(item.option)
            })
            $scope.createActivity.isPolling = true;
            $scope.createActivity.pollingType = $scope.polling.pollType;
        }else{
            $scope.createActivity.pollingOptions = [];
            $scope.createActivity.isPolling = false;
            $scope.createActivity.pollingType = '';
        }        
        $scope.createActivity.groupIdsList = _.uniq($scope.createActivity.groupIdsList);
        $scope.createActivity.activityOwner = $scope.userLoginData.genieId;
        $scope.createActivity.endDate = new Date(setEndDate($scope.createActivity.endDate))
        $scope.createActivity.startDate = new Date(setStartDate($scope.createActivity.endDate))
        if(gapi.client.activitycalendar===undefined){
            gapi.client.load('activitycalendar', 'v1', function() {
                $scope.saveEntity()
            }, getGapiURL() + '/_ah/api')
        }else{
            $scope.saveEntity()
        }
    }   
    $scope.saveEntity = function(){
        gapi.client.activitycalendar.createActivity($scope.createActivity).execute(function(resp) {
            $scope.$apply(function(){
                if(!resp.code){
                    $scope.activity = resp.result
                    if($scope.tempAttachments.length>0){
                        var index = 0;
                        $scope.uploadAttachment(resp.result.id, index)
                    }else{
                        ApplicationFactory.hideIonicLoader()
                        $scope.showConfirmMessage(resp.result)
                        var activityState = ApplicationFactory.getApplicationStates()
                        if(activityState.backView==null){
                            $state.go('activity-stream')
                        }else{
                            $ionicHistory.goBack()
                        }
                    }
                }else{
                    ApplicationFactory.showUserMessage("",resp.message,"",3000)
                }
            })
        })
    }
    $scope.uploadAttachment = function(activityId, index){
        //ApplicationFactory.showIonicLoader(false)
        if(!checkStringItemNull($scope.failedAttachmentItems)){
            $scope.failedAttachmentItems = 0
        }
        $scope.index = index;
        $scope.activityId = activityId;
        var data = {'fileName':$scope.tempAttachments[index].fileItem.name,'uploadAction':'upload'}
        gapi.client.activitycalendar.createUploadBlobUrl(data).execute(
            function(resp) {
                $scope.$apply(function(){
                    ApplicationFactory.hideIonicLoader()
                    ApplicationFactory.showProgressBarLoader("Sending ...")
                    if (!resp.code) {
                        if($scope.tempAttachments[index].device!=true){
                            var formData = new FormData();
                            formData.append('attachment',$scope.tempAttachments[index].fileItem);
                            formData.append('activityId',activityId);
                            $.ajax({
                                url: resp.url,
                                type: "POST",
                                data: formData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                xhr: function() {  // Custom XMLHttpRequest
                                    var myXhr = $.ajaxSettings.xhr();
                                    myXhr.upload.onprogress = function(e) {
                                        if (e.lengthComputable) {
                                            var uploadStatus = ((e.loaded / e.total)/($scope.tempAttachments.length - $scope.failedAttachmentItems)) * 100;
                                            $scope.progressbar.set(uploadStatus)
                                        }
                                    };
                                    return myXhr;
                                },
                            }).success(function(data) {
                                $scope.checkUploadCompleted(activityId)
                            }).error(function(data){
                                $scope.failedAttachmentItems += 1
                                $scope.checkUploadCompleted(activityId)
                            });
                        }else{
                            var server = resp.url
                            var filePath = $scope.tempAttachments[index].fileItem.localURL
                            var options = {'fileKey':'attachment',
                                            'params':{'activityId':activityId},
                                            'fileName':$scope.tempAttachments[index].fileItem.name,
                                            'mimeType':$scope.tempAttachments[index].fileItem.type
                                    }
                            $cordovaFileTransfer.upload(server, filePath, options).then(function(result) {
                                $scope.checkUploadCompleted(activityId)
                            }, function(err) {
                                $scope.failedAttachmentItems += 1
                                $scope.checkUploadCompleted(activityId)
                            }, function (progress) {
                                $timeout(function () {
                                    var uploadStatus = ((progress.loaded / progress.total)/($scope.tempAttachments.length - $scope.failedAttachmentItems)) * 100;
                                    $scope.progressbar.set(uploadStatus)
                                });
                            });
                        }
                    }else{
                        $scope.failedAttachmentItems += 1
                        $scope.checkUploadCompleted(activityId)
                    }
                })
            })
    }

    $scope.showConfirmMessage = function(){
        var confirmPopup = $ionicPopup.confirm({
            template: window.userMessages.ACTIVITY_CREATED,
            buttons: [{text: '<b>Edit</b>',type: 'button-positive',
                    onTap: function(e) {
                        if ($scope.activity) {
                            ApplicationFactory.setEditActivityDetails($scope.activity)
                            $state.go('edit-activity')
                        }
            }}]
        });
        confirmPopup.then(function(res) {
            if(res){
            }else{
            }
        });
        $timeout(function() {
            confirmPopup.close()
        }, 3000)
    }
   
    $scope.checkUploadCompleted = function(activityId){
        if($scope.index < $scope.tempAttachments.length - 1){
            $scope.index += 1;
            $scope.uploadAttachment($scope.activityId, $scope.index);
        }else{
            var activityState = ApplicationFactory.getApplicationStates()
            if($scope.failedAttachmentItems==$scope.tempAttachments.length){
                var tempData = {}
                tempData.id = activityId
                gapi.client.activitycalendar.uploadActivityAttachmentFailed(tempData).execute(function(resp) {
                    if (!resp.code){
                        ApplicationFactory.showUserMessage("",window.userMessages.ATTACHMENT_UPLOAD_FAILED,"",3000);
                    }else{
                        ApplicationFactory.showUserMessage("",window.userMessages.ATTACHMENT_UPLOAD_FAILED,"",3000);
                    }
                    $scope.completeProgressBarAction()
                    //ApplicationFactory.hideIonicLoader()
                    if(activityState.backView==null){
                        $state.go('activity-stream')
                    }else{
                        $ionicHistory.goBack()
                    }
                })
            }else{
                $scope.completeProgressBarAction()
                //ApplicationFactory.hideIonicLoader()
                if(activityState.backView==null){
                    $state.go('activity-stream')
                }else{
                    $ionicHistory.goBack()
                }
                $scope.showConfirmMessage($scope.activity)
            }    
        }
    }
    $scope.completeProgressBarAction = function(){
        if($scope.checkApplyInProgress()){
            $scope.progressbar.complete()
            ApplicationFactory.hideProgressBarLoader()
        }else{
            $scope.$apply(function(){
                $scope.progressbar.complete()
                ApplicationFactory.hideProgressBarLoader()
            })
        }
    }
    $scope.getActivityType = function(activityType){
        if(!checkStringItemNull($scope.createActivity)){
            return false
        }
        if($scope.createActivity.activityType==activityType){
            return true
        }else{
            return false
        }
    }
    $scope.showFileTypeImage = function(mimeType){
        return ApplicationFactory.showFileType(mimeType)
    }
    $scope.setActivityType = function(activityType){
        $scope.createActivity.activityType = activityType;
        return activityType;
    }
      
    $scope.goPreviousState = function(){
        var activityState = ApplicationFactory.getApplicationStates()
        if(activityState.backView==null){
            $state.go('activity-stream')
        }else{
            $ionicHistory.goBack()
        }
    }
    $scope.showEndDate = function(){
        if($scope.createActivity.activityType=='assignment'){
            return 'flex-display'
        }else{
            return 'hide'
        }
    }
}])

genieControllers.controller('ActivityStreamController',['$scope', 'ApplicationFactory', '$state', '$stateParams', '$ionicPopup', '$ionicScrollDelegate',
        function($scope, ApplicationFactory, $state, $stateParams, $ionicPopup, $ionicScrollDelegate) {
    $scope.userLoginData = ApplicationFactory.getUserLoginData()
    var userGroupsData = ApplicationFactory.getUserGroupsData();
    var tempActivityStream = ApplicationFactory.getActivityStreamData();
    var userRoles = ApplicationFactory.getUserRolesData();
    var childrenData = ApplicationFactory.getChindrensData()
    $scope.selectedFilterGroups = [];
    $scope.tokenInitialized = false;
    $scope.activityLoaded = false;
    $scope.goToActivityDetails = function(activityData){
        ApplicationFactory.setActivityDetailItem(activityData)
        ApplicationFactory.showIonicLoader()
        $state.go('activity-details');
    }
    $scope.checkActivityLoaded = function(){
        return $scope.activityLoaded;   
    }
    $scope.groupsData = function(groupIds){
        var temp = [];
        angular.forEach(groupIds,function(value){
            var group = _.findWhere(userGroupsData,{'id':value})
            if(checkObjectItemNull(group)){
                temp.push(group)
            }
        })
        return temp;
    }
    $scope.showInstituteName = function(instituteNames, groupIds){
        var groupsData = $scope.groupsData(groupIds)
        var found = _.findWhere(groupsData, {'displayGroupName':instituteNames[0].displayName})
        if(checkObjectItemNull(found)){
            return false;
        }else{
            return true;
        }
    }
    $scope.showFileTypeImage = function(mimeType){
        return ApplicationFactory.showFileType(mimeType)
    }
    $scope.$on("$ionicView.leave", function( scopes, states ) {
        $scope.activityStreamContent = [];
        $scope.activityLoaded = false;
    })
    $scope.initializeDirectiveTokenInput = function(){
        if($scope.userLoginData.userType=='teacher'){
            if(checkObjectItemNull($scope.teacherInstituteId)){
                if($scope.teacherInstituteId!=$scope.userLoginData.instituteId){
                    $scope.emptySelectedFilterGroups()
                    $scope.teacherInstituteId = $scope.userLoginData.instituteId;
                }
            }else{
                $scope.teacherInstituteId = $scope.userLoginData.instituteId
            }
            $scope.availableUserGroupList =  $scope.getUserGroups()
        }else if($scope.userLoginData.userType=='parent'){
            $scope.parentFilters = $scope.createParentFilters();
        }
        $scope.prePopulateTokenInput()
    }
    $scope.$on("$ionicView.enter", function( scopes, states ) {
        $scope.activityLoaded = false;
        $scope.userLoginData = ApplicationFactory.getUserLoginData()
        if(!checkStringItemNull($scope.userLoginData.userType)){
            $state.go('user-roles')
            return true;
        }
        ApplicationFactory.showIonicLoader()
        ApplicationFactory.setAppState('activity-stream');
        $scope.availableUserGroupList = $scope.getUserGroups()
        tempActivityStream = sortCalendarEvents(ApplicationFactory.getActivityStreamData());
        $scope.activityStreamContent = getTodaysActivityStream(tempActivityStream,setStartDate(new Date().setDate(new Date().getDate()-ACTIVITY_DAYS_COUNT)),setEndDate(new Date()));
        if(window.userOnline || navigator.onLine){
            if(checkObjectItemNull($scope.activityStreamContent)){
                var lastItemTime = new Date($scope.activityStreamContent[0].updatedTime).getTime()
                if((new Date().getTime()-lastItemTime) > ACTIVITY_DIFFERENCE_IN_MILLISECONDS){
                    $scope.userLoginData.startDate = new Date(setStartDate(new Date().setDate(new Date().getDate()-ACTIVITY_DAYS_COUNT)))
                }else{
                    $scope.userLoginData.startDate = new Date(setStartDate(new Date($scope.activityStreamContent[0].updatedTime)))
                }
            }else{
                $scope.userLoginData.startDate = new Date(setStartDate(new Date().setDate(new Date().getDate()-ACTIVITY_DAYS_COUNT)))
            }
            $scope.userLoginData.endDate = new Date(setEndDate(new Date()))
            if(gapi.client.activitycalendar===undefined){
                gapi.client.load('activitycalendar', 'v1', function() {
                    $scope.getActivityStream($scope.userLoginData)
                },getGapiURL() + '/_ah/api');
            }else{
                $scope.getActivityStream($scope.userLoginData)
            }
        }else{
            $scope.activityLoaded = true;
            $ionicScrollDelegate.resize();
            ApplicationFactory.hideIonicLoader()
            $scope.$broadcast('scroll.refreshComplete');
        }
        $scope.parentAvailableFilters = []
        ApplicationFactory.setApplicationStates();
        $scope.initializeDirectiveTokenInput()
    });
    $scope.getUserGroups = function(){
        $scope.userLoginData = ApplicationFactory.getUserLoginData()
        userGroupsData = ApplicationFactory.getUserGroupsData();
        userRoles = ApplicationFactory.getUserRolesData()
        var groupIds = null;
        var groups = [];
        if($scope.userLoginData.userType=='parent'){
            groupIds = getChildGroups(userRoles)
        }else if($scope.userLoginData.userType=='teacher'){
            groupIds = getLoggedUserGroupList(userRoles, $scope.userLoginData.instituteId, $scope.userLoginData.userType)
            groups.push({'displayGroupName':'My Schedule', 'value':'all', 'id':'all'})
        }
        angular.forEach(groupIds, function(groupId, key){
            var group = _.findWhere(userGroupsData, {'id':groupId})
            if(checkObjectItemNull(group)){
                groups.push(group);
            }
        });
        return groups;
    }
    $scope.createParentFilters = function(){
        userGroupsData = ApplicationFactory.getUserGroupsData();
        childrenData = ApplicationFactory.getChindrensData();
        var parentChildFilters = createParentChildFilters(childrenData)
        if(!checkObjectItemNull($scope.selectedParentFilters)){
            $scope.selectedParentFilters = []
            //$scope.selectedParentFilters.push({'displayName':'All', 'value':'all', 'id':'all'})
        }
        $scope.parentAvailableFilters = []
        //$scope.parentAvailableFilters.push({'displayName':'All', 'value':'all', 'id':'all'})
        var items = createParentInstituteFilters(childrenData, userGroupsData, parentChildFilters)
        angular.forEach(items, function(value, key){
            var found = _.findWhere($scope.selectedParentFilters, {'id':value.id})
            if(!checkObjectItemNull(found)){
                $scope.parentAvailableFilters.push(value)
            }
        });
        return $scope.parentAvailableFilters;
    }
    $scope.loadTeacherFilters = function(query){
        var items = $scope.getUserGroups();
        if(!checkStringItemNull(query)){
            return items;
        }
        return items.filter(function(item){
            return (item.displayGroupName).toLowerCase().indexOf(query.toLowerCase()) !==-1
        })
    }
    $scope.loadParentFilters = function(query){
        var items = $scope.createParentFilters();
        if(!checkStringItemNull(query)){
            return items;
        }
        return items.filter(function(item){
            return (item.displayName).toLowerCase().indexOf(query.toLowerCase()) !==-1
        })
    }
    $scope.addParentGroup = function(group, index){
        var found = _.findWhere($scope.selectedParentFilters, {'id':group.id})
        if(!checkObjectItemNull(found)){
            $scope.selectedParentFilters.push(group)
            $ionicScrollDelegate.resize();
            //$ionicScrollDelegate.scrollTop();
            /*angular.forEach($scope.parentAvailableFilters, function(value, key){
                if(value.id==group.id){
                    $scope.parentAvailableFilters.splice(key, 1)
                }
            })*/
        }
    }
    $scope.removeParentGroup = function(group, index){
        var found = _.findWhere($scope.selectedParentFilters, {'id':group.id})
        if(checkObjectItemNull(found)){
            $ionicScrollDelegate.resize();
            //$ionicScrollDelegate.scrollTop();
            //$scope.parentAvailableFilters.push(group)
            
            //$scope.selectedParentFilters.splice(index, 1)
        }
    }
    $scope.removeFilterGroup = function(filterGroup){
        if($scope.userLoginData.userType=='parent'){
            if($scope.selectedParentFilters.length==0){
                $scope.prePopulateTokenInput()
            }
        }else if($scope.userLoginData.userType=='teacher'){
            if($scope.selectedFilterGroups.length==0){
                $scope.prePopulateTokenInput()
            }
        }
        $ionicScrollDelegate.resize();
    }
    $scope.addFilterGroup = function(filterGroup){
        if($scope.userLoginData.userType=='parent'){
            if(filterGroup.id=='all'){
                $scope.selectedParentFilters = [];
                $scope.selectedParentFilters.push(filterGroup)
            }
            if($scope.selectedParentFilters.length>1){
                var found = _.findWhere($scope.selectedParentFilters, {'id':'all'})
                if(checkObjectItemNull(found)){
                    for(var i=0; i<$scope.selectedParentFilters.length; i++){
                        if($scope.selectedParentFilters[i].id=='all'){
                            $scope.selectedParentFilters.splice(i, 1)                            
                            break;
                        }
                    }
                }
            }
        }else if($scope.userLoginData.userType=='teacher'){
            if(filterGroup.id=='all'){
                $scope.selectedFilterGroups = [];
                $scope.selectedFilterGroups.push(filterGroup)
            }
            if($scope.selectedFilterGroups.length>1){
                var found = _.findWhere($scope.selectedFilterGroups, {'id':'all'})
                if(checkObjectItemNull(found)){
                    for(var i=0; i<$scope.selectedFilterGroups.length; i++){
                        if($scope.selectedFilterGroups[i].id=='all'){
                            $scope.selectedFilterGroups.splice(i, 1)
                            break;
                        }
                    }
                }
            }
        }
        $ionicScrollDelegate.resize();
    }
    $scope.prePopulateTokenInput = function(){
        if(!checkObjectItemNull($scope.selectedFilterGroups) && $scope.userLoginData.userType=='teacher'){
            //$scope.selectedFilterGroups.push({'displayGroupName':'My Schedule', 'value':'all', 'id':'all'})
        }else if(!checkObjectItemNull($scope.selectedParentFilters) && $scope.userLoginData.userType=='parent'){
            /*angular.forEach($scope.parentFilters, function(group, key){
                $scope.selectedParentFilters.push(group)
            });*/
        }
    }
    $scope.emptySelectedFilterGroups = function(){
        $scope.selectedFilterGroups = []
        $scope.selectedParentFilters = []
    }
    $scope.checkImagesInActivity = function(attachments){
        var count = 0;
        angular.forEach(attachments, function(value, key){
            if(checkImageExist(value.fileType)){
                count = count + 1;
            }
        });
        return count;
    }
    $scope.isDueDateToday = function(event){
        if(!checkObjectItemNull(event)){
            return false;
        }
        return checkIsDueDateToday(event.endDate)
    }
    $scope.showDueDateFormat = function(event){
        if(!checkObjectItemNull(event)){
            return false;
        }
        return showDueDateFormat(event.endDate)
    }
    $scope.downloadAttachment = function(attachment){
        downloadAttachment(attachment)
    }
    $scope.getUserTokenGroups = function(){ 
        var groups = ""
        if(checkObjectItemNull($scope.userTokenInputGroups)){
            groups = $scope.userTokenInputGroups.join();
        }
        return groups;     
    }
    $scope.getAttachmentUrl = function(attachmentURL){
        return ApplicationFactory.generateFileAttachment(attachmentURL);
    }
    $scope.checkActivityOwner = function(event){
        if(!checkObjectItemNull(event) || !checkStringItemNull(event.activityOwner)){
            return false;
        }
        if(event.activityOwner==$scope.userLoginData.genieId){
            return true;
        }else{
            return false;
        }
    }
    $scope.getActivityChildData = function(activity){
        var childs = []
        if(!checkObjectItemNull(activity)){
            return childs;
        }
        return ApplicationFactory.getActivityChildrenNames(activity);
    }
    $scope.getActivityInstituteNames = function(activity){
        var inst = []
        if(!checkObjectItemNull(activity)){
            return inst;
        }
        if($scope.userLoginData.userType=='parent'){
            return ApplicationFactory.getActivityChildInstituteNames(activity)
        }else{
            return ApplicationFactory.getActivityTeacherInstituteNames(activity)
        }
    }
    $scope.doRefresh = function(){
        if(window.userOnline || navigator.onLine){
            $scope.getActivityStream($scope.userLoginData, 'forceRefresh');
        }else{
            $scope.activityLoaded = true;
            $ionicScrollDelegate.resize();
            ApplicationFactory.hideIonicLoader()
            $scope.$broadcast('scroll.refreshComplete');
        }
    }
    $scope.deleteActivity = function(event){
        var confirmPopup = $ionicPopup.confirm({
            template: window.userMessages.ACTIVITY_DELETE
        });
        confirmPopup.then(function(res) {
            if(res){
                var item = $scope.createAnnotationForm(event.id, event.groupIdList)
                ApplicationFactory.showIonicLoader();
                var promise = ApplicationFactory.deleteActivity(item);
                promise.then(function(result){
                    var phase = $scope.$root.$$phase
                    if(phase==null){
                        $scope.$apply(function(){
                            $scope.removeStreamItem(event.id)
                        })
                    }else{
                        $scope.removeStreamItem(event.id)
                    }
                    ApplicationFactory.showUserMessage("",window.userMessages.ACTIVITY_DELETE_SUCCESS,"",3000)
                },function(error){
                    ApplicationFactory.showUserMessage("",window.userMessages.ACTIVITY_DELETE_FAILURE,"",3000)
                })
                ApplicationFactory.hideIonicLoader();
            }
        });
    }
    $scope.removeStreamItem= function(eventId){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = deleteActivityCalendarItems(eventId, streamItems)
        ApplicationFactory.setActivityStreamData(streamItems)
        $scope.activityStreamContent = getTodaysActivityStream(streamItems, setStartDate(new Date().setDate(new Date().getDate()-5)), setEndDate(new Date()))
        calendarItems = deleteActivityCalendarItems(eventId, calendarItems)
        ApplicationFactory.setUserCalendarData(calendarItems)
    }
    $scope.editActivity = function(event){
        ApplicationFactory.setEditActivityDetails(event)
        $state.go('edit-activity')
    }
    $scope.likeUnlikeActivity = function(event, flag, $index){
        $scope.saveEventLike(event.id, flag)
        var item = $scope.createAnnotationForm(event.id, event.groupIdList)
        item.like = flag;
        var promise = ApplicationFactory.saveLikeUnlikeAnnotations(item)
        promise.then(function(result){
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.saveEventLike(event.id, flag)
                })
            }else{
                $scope.saveEventLike(event.id, flag)
            }
        },function(error){
            ApplicationFactory.showUserMessage("", window.userMessages.ACTIVITY_LIKE_UNLIKE_FAILURE, "", 3000)
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.saveEventLike(event.id, !flag)
                })
            }else{
                $scope.saveEventLike(event.id, !flag)
            }
        })
    }
    $scope.saveEventLike = function(eventId, flag){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = saveActivityStreamCalendarLikeUnlike(eventId, streamItems, flag)
        ApplicationFactory.setActivityStreamData(streamItems)
        $scope.activityStreamContent = getTodaysActivityStream(streamItems, setStartDate(new Date().setDate(new Date().getDate()-5)), setEndDate(new Date()))
        calendarItems = saveActivityStreamCalendarLikeUnlike(eventId, calendarItems, flag)
        ApplicationFactory.setUserCalendarData(calendarItems)
    }
    $scope.completeUncompleteActivity = function(event, flag){
        $scope.saveEventCompletion(event.id, flag)
        var item = $scope.createAnnotationForm(event.id, event.groupIdList)
        item.completed = flag;
        var promise = ApplicationFactory.saveCompleteUncompleteAnnotations(item)
        promise.then(function(result){
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.saveEventCompletion(event.id, flag)
                })
            }else{
                $scope.saveEventCompletion(event.id, flag)
            }
        },function(error){
            ApplicationFactory.showUserMessage("", window.userMessages.ACTIVITY_COMPLETE_UNCOMPLETE_FAILURE, "", 3000)
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.saveEventCompletion(event.id, !flag)
                })
            }else{
                $scope.saveEventCompletion(event.id, !flag)
            }
        })
    }
    $scope.saveEventCompletion = function(eventId, flag){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = saveActivityStreamCalendarCompletion(eventId, streamItems, flag)
        ApplicationFactory.setActivityStreamData(streamItems)
        $scope.activityStreamContent = getTodaysActivityStream(streamItems, setStartDate(new Date().setDate(new Date().getDate()-5)), setEndDate(new Date()))
        calendarItems = saveActivityStreamCalendarCompletion(eventId, calendarItems, flag)
        ApplicationFactory.setUserCalendarData(calendarItems)
    }
    $scope.createAnnotationForm = function(activityId, groupIds){
        userLoginData = ApplicationFactory.getUserLoginData()
        childData = ApplicationFactory.getChindrensData();
        var item = createAnnotationForm(activityId, groupIds, userLoginData, childData)
        return item;
    }
    $scope.createActivityFilters = function(activityStreamContent){
        var temp = []
        angular.forEach(activityStreamContent, function(item){
            item.activityChildData = $scope.getActivityChildData(item)
            item.instituteNames = $scope.getActivityInstituteNames(item)
            temp.push(item)
        })
        return temp;
    }
    $scope.getActivityStream = function(userLoginData, isForced){
        var currentDate = new Date().getTime();
       /* if($scope.userLoginData.activityRefreshTime!=null && isForced!="forceRefresh" && $scope.userLoginData.userType!='teacher' && 
                parseInt($scope.userLoginData.activityRefreshTime) - currentDate <= 1800){
            var items = ApplicationFactory.getActivityStreamData();
            $scope.activityStreamContent = getTodaysActivityStream(items, new Date(setStartDate(new Date().setDate(new Date().getDate()-5))), setEndDate(new Date));
            ApplicationFactory.hideIonicLoader()
            return true;
        }*/
        $scope.userLoginData.startDate = new Date(setStartDate(new Date().setDate(new Date().getDate()-5)))
        $scope.userLoginData.endDate = new Date(setEndDate(new Date()))

        var temp = [];
        if($scope.userLoginData.userType=='parent'){
            if(checkObjectItemNull(ApplicationFactory.getChindrensData())){
                $scope.userLoginData.userGroupList = getChildGroups(ApplicationFactory.getChindrensData());
            }else{
                $scope.userLoginData.userGroupList = temp;
            }
        }else{
            var userRolesData = ApplicationFactory.getUserRolesData()
            $scope.userLoginData.userGroupList = getLoggedUserGroupList(userRolesData, $scope.userLoginData.instituteId, $scope.userLoginData.userType)
        }
        gapi.client.activitycalendar.getlistOfActivity($scope.userLoginData).execute(function(resp) {
            $scope.$apply(function(){
                if (!resp.code){
                    $scope.userLoginData.activityRefreshTime = new Date().getTime();
                    ApplicationFactory.setUserLoginData($scope.userLoginData)
                    var items = pushItemsToArray(tempActivityStream, resp.result.items);
                    items = $scope.createActivityFilters(items)
                    $scope.activityStreamContent = getTodaysActivityStream(items,setStartDate(new Date().setDate(new Date().getDate()-5)),setEndDate(new Date()));
                    //$scope.activityStreamContent = items
                    ApplicationFactory.setActivityStreamData(items);
                    ApplicationFactory.setUserCalendarData(items);
                }
                $scope.activityLoaded = true;
                $ionicScrollDelegate.resize();
                ApplicationFactory.hideIonicLoader()
                $scope.$broadcast('scroll.refreshComplete');
            })
        })
    }
}])

genieControllers.controller('ActivityDetailsController', ['$scope', 'ApplicationFactory', '$stateParams','$state','$ionicHistory','$ionicPopup',
    '$cordovaFileTransfer', '$cordovaFileOpener2', '$timeout', 'ngProgressFactory', '$cordovaNetwork', '$rootScope',
            function($scope, ApplicationFactory, $stateParams, $state, $ionicHistory, $ionicPopup, $cordovaFileTransfer, $cordovaFileOpener2, 
                $timeout, ngProgressFactory, $cordovaNetwork, $rootScope) {
    var activityDetailItem = ApplicationFactory.getActivityDetailItem()
    var userGroupsData = ApplicationFactory.getUserGroupsData()
    $scope.userLoginData = ApplicationFactory.getUserLoginData()
    var userRolesData = ApplicationFactory.getUserRolesData()
    $scope.pieChart = {'show':false, 'type':"", labels:[],data:[]};
    $scope.pollingAnswers = [];
    $scope.activityLoaded = false;
    angular.element(document).ready(function(){
        $scope.progressbar = ngProgressFactory.createInstance();
        $scope.progressbar.setColor('#fff');
        var progressParent = $('.progress-bar-loader')
        $scope.progressbar.setParent(progressParent[0])
    })
    $scope.checkInternetConnection = function(){
        if(window.cordova){
            $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
                $scope.completeProgressBarAction()
            })
        }else{
            window.addEventListener('offline',  function(){
                $scope.completeProgressBarAction()
            });
        }
    }
    $scope.$on("$ionicView.enter", function( scopes, states ) {
        $scope.checkInternetConnection()
        $scope.activityLoaded = false;
        $scope.userLoginData = ApplicationFactory.getUserLoginData()
        if(!checkStringItemNull($scope.userLoginData.userType)){
            $state.go('user-roles')
            return true;
        }
        ApplicationFactory.showIonicLoader()
        ApplicationFactory.setAppState('activity-details');
        $scope.pieChart.show = false;
        activityDetailItem = ApplicationFactory.getActivityDetailItem()
        userGroupsData = ApplicationFactory.getUserGroupsData()
        $scope.getActivityItemInDetail(activityDetailItem)
        userRolesData = ApplicationFactory.getUserRolesData()
        $scope.userLoginData.genieId = getUserGenieId(userRolesData, $scope.userLoginData)
        ApplicationFactory.setApplicationStates();
    });
    $scope.refreshActivityDetailItem = function(){
        activityDetailItem = ApplicationFactory.getActivityDetailItem()
        $scope.getActivityItemInDetail(activityDetailItem)
    }
    $scope.checkActivityLoaded = function(){
        return $scope.activityLoaded;   
    }
    $scope.$on("$ionicView.leave", function( scopes, states ) {
        $scope.completeProgressBarAction()
        //ApplicationFactory.hideIonicLoader()
        $scope.activityDetailItem = null;
        $scope.activityLoaded = false;
    })
    $scope.groupsData = function(groupIds){
        var temp = [];
        angular.forEach(groupIds,function(value){
            var group = _.findWhere(userGroupsData,{'id':value})
            if(checkObjectItemNull(group)){
                temp.push(group)
            }
        })
        return temp;
    }
    $scope.showInstituteName = function(instituteNames, groupIds){
        var groupsData = $scope.groupsData(groupIds)
        var found = _.findWhere(groupsData, {'displayGroupName':instituteNames[0].displayName})
        if(checkObjectItemNull(found)){
            return false;
        }else{
            return true;
        }
    }
    $scope.goPreviousState = function(){
        var activityState = ApplicationFactory.getApplicationStates()
        if(activityState.backView==null){
            $state.go('activity-stream')
        }else{
            $ionicHistory.goBack()
        }
    }
    $scope.showFileTypeImage = function(mimeType){
        return ApplicationFactory.showFileType(mimeType)
    }
    $scope.checkImagesInActivity = function(){
        if(!checkObjectItemNull($scope.activityDetailItem)){
            return 0;
        }
        var count = 0;
        angular.forEach($scope.activityDetailItem.attachments, function(value, key){
            if(checkImageExist(value.fileType)){
                count = count + 1;
            }
        });
        return count;
    }
    $scope.getSentUserCount = function(totalUsers){
        var temp = 0;
        angular.forEach(totalUsers, function(value){
            if((value.userType=='parent' && value.unique==true) ||  value.userType!='parent'){
                temp = temp + parseInt(value.userCount);
            }
        })
        return temp;
    }
    $scope.getReadUserCount = function(readUsers){
        var temp = 0;
        angular.forEach(readUsers, function(value){
            temp = temp + parseInt(value.userCount);
        })
        return temp;
    }
    $scope.downloadAttachment = function(attachment){
        if(!window.userOnline || !navigator.onLine){
            ApplicationFactory.showUserMessage("",window.userMessages.USER_OFFLINE,"",3000);
            $scope.progressbar.complete()
            ApplicationFactory.hideProgressBarLoader()
            return true
        }
        if(!window.cordova){
            downloadAttachment(attachment)
        }else if(window.cordova && window.cordova.plugins && window.FileTransfer){
            ApplicationFactory.showIonicLoader(false)
            var appspotsite = getGapiURL() + "/downloadFile?blob-id="+attachment.id;
            var store = "";
            var fileURL = ""
            var fileTransfer = new FileTransfer();
            var options = {};
            var trustHosts = true
            if(ionic.Platform.isAndroid()){
                store = cordova.file.externalRootDirectory
                fileURL = store + 'Download/' + attachment.fileName;
                if(parseInt(ionic.Platform.version())>5 &&  ionic.Platform.isAndroid()){
                    //For Android versions greater than 5
                    cordova.plugins.diagnostic.requestRuntimePermissions(function(statuses){
                        for (var permission in statuses){
                            switch(statuses[permission]){
                                case cordova.plugins.diagnostic.permissionStatus.GRANTED:
                                        $scope.downloadDeviceAttachments(appspotsite,fileURL, options, trustHosts, attachment)
                                        break;
                                case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
                                        //alert("Permission to use "+permission+" has not been requested yet");
                                        break;
                                case cordova.plugins.diagnostic.permissionStatus.DENIED:
                                        //alert("Permission denied to use "+permission+" - ask again?");
                                        break;
                                case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
                                        ApplicationFactory.showUserMessage("",window.userMessages.DENIED_CORDOVA_STORAGE_PERMISSIONS,"",3000);
                                        break;
                            }
                        }
                    }, function(error){
                        ApplicationFactory.hideIonicLoader()
                        ApplicationFactory.showUserMessage("",error,"",3000);
                    },[
                        cordova.plugins.diagnostic.runtimePermission.WRITE_EXTERNAL_STORAGE,
                    ]);
                }else{
                    $scope.downloadDeviceAttachments(appspotsite, fileURL, options, trustHosts, attachment)
                }
            }else if(ionic.Platform.isIOS()){
                store = cordova.file.dataDirectory
                fileURL = store + attachment.fileName;
                window.resolveLocalFileSystemURL(fileURL, function success(fileEntry) {
                    fileEntry.file(function (file) {
                        $scope.downloadDeviceAttachments(appspotsite, file.localURL, options, trustHosts, attachment)
                   })
                }, function (err) {
                    window.resolveLocalFileSystemURL(cordova.file.documentsDirectory,
                        function success(dirEntry) {
                        dirEntry.getFile(attachment.fileName.replace(/^.*[\\\/]/, ''), { create: true, exclusive: false},
                            function (fileEntry) {
                                fileEntry.file(function (file) {
                                    $scope.downloadDeviceAttachments(appspotsite, file.localURL, options, trustHosts, attachment)
                            });
                        }, function(err){
                            ApplicationFactory.hideIonicLoader()
                            ApplicationFactory.showUserMessage("",err,"",3000)
                        });
                      }, function(err){
                        ApplicationFactory.hideIonicLoader()
                        ApplicationFactory.showUserMessage("",err,"",3000)
                     });
                });
            }
        }
    }
    $scope.checkApplyInProgress = function(){
        var phase = $scope.$root.$$phase
        if(phase==null){
            return false
        }else{
            return true
        }
    }
    $scope.downloadSuccess = function(){

    }
    $scope.downloadFail = function(){

    }
    $scope.downloadDeviceAttachments = function(appspotsite, fileURL, options, trustHosts, attachment){
        //ApplicationFactory.showIonicLoader(false)
        $scope.downloadSuccess = false
        ApplicationFactory.hideIonicLoader()
        ApplicationFactory.showProgressBarLoader("Downloading ...")
        $scope.progressbar.start()
        var ft = new FileTransfer();
        ft.onprogress = function (progress) {
            var downloadStatus = (progress.loaded / parseInt(attachment.fileSize)) * 100;
            if(isNaN(downloadStatus) || !checkStringItemNull(downloadStatus)){
                ft.abort();
                $scope.completeProgressBarAction()
            }else{
                $scope.progressbar.set(downloadStatus)
            } 
        };
        ft.download(appspotsite, 
                    fileURL, 
                    function(){
                        $scope.completeProgressBarAction()
                        if(ionic.Platform.isIOS()){
                            $scope.downloadByDeviceType(result.nativeURL, attachment.fileType)
                        }else if(ionic.Platform.isAndroid()){
                            $scope.downloadByDeviceType(fileURL, attachment.fileType)
                        }
                    }, 
                    function(){
                        ft.abort();
                        $scope.completeProgressBarAction()
                        ApplicationFactory.showUserMessage("",error.body,"",3000);
                    }, 
                    trustHosts, 
                    options
                );
        /*$cordovaFileTransfer.download(appspotsite,fileURL, options, trustHosts).then(function(result) {
            if($scope.checkApplyInProgress()){
                $scope.downloadSuccess = true
            }else{
                $scope.$apply(function(){
                    $scope.downloadSuccess = true
                })
            }
            $scope.completeProgressBarAction()
            if(ionic.Platform.isIOS()){
                $scope.downloadByDeviceType(result.nativeURL, attachment.fileType)
            }else if(ionic.Platform.isAndroid()){
                $scope.downloadByDeviceType(fileURL, attachment.fileType)
            }
        }, function(error) {
            transferObject.abort()
            $scope.completeProgressBarAction()
            ApplicationFactory.showUserMessage("",error.body,"",3000);
        }, function (progress) {
            $timeout(function () {
                if($scope.downloadSuccess){
                    $cordovaFileTransfer.abort()
                    $scope.completeProgressBarAction()
                }else{
                    var downloadStatus = (progress.loaded / parseInt(attachment.fileSize)) * 100;
                    if(isNaN(downloadStatus)){
                        $scope.completeProgressBarAction()
                    }else{
                        $scope.progressbar.set(downloadStatus)
                    } 
                }
            })
        });*/
    }
    $scope.downloadByDeviceType = function(fileLocation, fileType){
        $cordovaFileOpener2.open(fileLocation, fileType).then(function(result) {
            //$scope.completeProgressBarAction()
        }, function(err) {
            //$scope.completeProgressBarAction()
            ApplicationFactory.showUserMessage("",window.userMessages.NO_APPLICATION_FILE_OPEN,"",3000);
        });
    }
    $scope.completeProgressBarAction = function(){
        if($scope.checkApplyInProgress()){
            $scope.progressbar.complete()
            ApplicationFactory.hideProgressBarLoader()
        }else{
            $scope.$apply(function(){
                $scope.progressbar.complete()
                ApplicationFactory.hideProgressBarLoader()
            }) 
        }
    }
    $scope.getActivityChildData = function(){
        var childs = []
        if(!checkObjectItemNull($scope.activityDetailItem)){
            return childs;
        }
        $scope.activityChildData = ApplicationFactory.getActivityChildrenNames($scope.activityDetailItem);
        return $scope.activityChildData
    }
    $scope.getActivityInstituteNames = function(){
        var inst = []
        if(!checkObjectItemNull($scope.activityDetailItem)){
            return inst;
        }
        if($scope.userLoginData.userType=='parent'){
            $scope.activityInstituteNames =  ApplicationFactory.getActivityChildInstituteNames($scope.activityDetailItem)
        }else{
            $scope.activityInstituteNames = ApplicationFactory.getActivityTeacherInstituteNames($scope.activityDetailItem)
        }
        return $scope.activityInstituteNames;
    }
    $scope.checkActivityOwner = function(){
        if(!checkObjectItemNull($scope.activityDetailItem) || !checkStringItemNull($scope.activityDetailItem.activityOwner)){
            return false;
        }
        if($scope.activityDetailItem.activityOwner==$scope.userLoginData.genieId){
            return true;
        }else{
            return false;
        }
    }
    $scope.getReadUsersList = function(){
        $state.go('activity-read-users')
    }
    $scope.sendNotificationsToAll = function(){
        ApplicationFactory.showIonicLoader()
        var item = {}
        item.activityId = $scope.activityDetailItem.id
        item.sendType = "send-to-all"
        gapi.client.activitycalendar.sendNotificationToUsers(item).execute(function(resp) {
            $scope.$apply(function(){
                if(!resp.code){
                    console.log(resp)
                }else{
                    console.log(resp.message)
                }
                ApplicationFactory.hideIonicLoader()
            })
        })
    }
    $scope.sendToUnreadUsers = function(){
        ApplicationFactory.showIonicLoader()
        var item = {}
        item.activityId = $scope.activityDetailItem.id
        item.sendType = "unread-users-only"
        gapi.client.activitycalendar.sendNotificationToUsers(item).execute(function(resp) {
            $scope.$apply(function(){
                if(!resp.code){
                    console.log(resp)
                }else{
                    console.log(resp.message)
                }
                ApplicationFactory.hideIonicLoader()
            })
        })
    }
    $scope.deleteActivity = function(){
        var confirmPopup = $ionicPopup.confirm({
            template: window.userMessages.ACTIVITY_DELETE
        });
        confirmPopup.then(function(res) {
            if(res){
                var item = $scope.createAnnotationForm($scope.activityDetailItem.id, $scope.activityDetailItem.groupIdList)
                ApplicationFactory.showIonicLoader();
                var promise = ApplicationFactory.deleteActivity(item);
                promise.then(function(result){
                    var phase = $scope.$root.$$phase
                    if(phase==null){
                        $scope.$apply(function(){
                            $scope.removeStreamItem($scope.activityDetailItem.id)
                        })
                    }else{
                        $scope.removeStreamItem($scope.activityDetailItem.id)
                    }
                    ApplicationFactory.showUserMessage("",window.userMessages.ACTIVITY_DELETE_SUCCESS,"",3000)
                },function(error){
                    ApplicationFactory.showUserMessage("",window.userMessages.ACTIVITY_DELETE_FAILURE,"",3000)
                })
                ApplicationFactory.setEditActivityDetails(null)
                ApplicationFactory.hideIonicLoader();
            }
        });
    }
    $scope.removeStreamItem= function(eventId){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = deleteActivityCalendarItems(eventId, streamItems)
        ApplicationFactory.setActivityStreamData(streamItems)
        calendarItems = deleteActivityCalendarItems(eventId, calendarItems)
        ApplicationFactory.setUserCalendarData(calendarItems)
        $scope.goPreviousState();

    }
    $scope.editActivity = function(){
        ApplicationFactory.setEditActivityDetails($scope.activityDetailItem)
        $state.go('edit-activity')
    }
    $scope.likeUnlikeActivity = function(flag){
        $scope.saveEventLike($scope.activityDetailItem.id, flag)
        var item = $scope.createAnnotationForm($scope.activityDetailItem.id, $scope.activityDetailItem.groupIdList)
        item.like = flag;
        var promise = ApplicationFactory.saveLikeUnlikeAnnotations(item)
        promise.then(function(result){
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.activityDetailItem.like = flag
                    $scope.saveEventLike($scope.activityDetailItem.id, flag)
                })
            }else{
                $scope.activityDetailItem.like = flag
                $scope.saveEventLike($scope.activityDetailItem.id, flag)
            }
        },function(error){
            ApplicationFactory.showUserMessage("", window.userMessages.ACTIVITY_LIKE_UNLIKE_FAILURE, "", 3000)
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.activityDetailItem.like = !flag
                    $scope.saveEventLike($scope.activityDetailItem.id, !flag)
                })
            }else{
                $scope.activityDetailItem.like = !flag
                $scope.saveEventLike($scope.activityDetailItem.id, !flag)
            }
        })
    }
    $scope.saveEventLike = function(eventId, flag){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = saveActivityStreamCalendarLikeUnlike(eventId, streamItems, flag)
        ApplicationFactory.setActivityStreamData(streamItems)
        $scope.activityStreamContent = getTodaysActivityStream(streamItems, setStartDate(new Date), setEndDate(new Date))
        calendarItems = saveActivityStreamCalendarLikeUnlike(eventId, calendarItems, flag)
        ApplicationFactory.setUserCalendarData(calendarItems)
    }
    $scope.completeUncompleteActivity = function(flag){
        $scope.saveEventCompletion($scope.activityDetailItem.id, flag)
        var item = $scope.createAnnotationForm($scope.activityDetailItem.id, $scope.activityDetailItem.groupIdList)
        item.completed = flag;
        var promise = ApplicationFactory.saveCompleteUncompleteAnnotations(item)
        promise.then(function(result){
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.activityDetailItem.completed = flag
                    $scope.saveEventCompletion($scope.activityDetailItem.id, flag)
                })
            }else{
                $scope.activityDetailItem.completed = flag
                $scope.saveEventCompletion($scope.activityDetailItem.id, flag)
            }
        },function(error){
            ApplicationFactory.showUserMessage("", window.userMessages.ACTIVITY_COMPLETE_UNCOMPLETE_FAILURE, "", 3000)
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.activityDetailItem.completed = !flag
                    $scope.saveEventCompletion($scope.activityDetailItem.id, !flag)
                })
            }else{
                $scope.activityDetailItem.completed = flag
                $scope.saveEventCompletion($scope.activityDetailItem.id, !flag)
            }
        })
    }
    $scope.saveEventCompletion = function(eventId, flag){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = saveActivityStreamCalendarCompletion(eventId, streamItems, flag)
        ApplicationFactory.setActivityStreamData(streamItems)
        $scope.activityStreamContent = getTodaysActivityStream(streamItems, setStartDate(new Date), setEndDate(new Date))
        calendarItems = saveActivityStreamCalendarCompletion(eventId, calendarItems, flag)
        ApplicationFactory.setUserCalendarData(calendarItems)
    }
    $scope.createAnnotationForm = function(){
        $scope.userLoginData = ApplicationFactory.getUserLoginData()
        childData = ApplicationFactory.getChindrensData();
        var item = createAnnotationForm(activityDetailItem.id, activityDetailItem.groupIdList, $scope.userLoginData, childData)
        return item;
    }
    $scope.getAttachmentUrl = function(attachmentURL){
        return ApplicationFactory.generateFileAttachment(attachmentURL);
    }
    $scope.isDueDateToday = function(activityDetailItem){
        if(!checkObjectItemNull(activityDetailItem)){
            return false;
        }
        return checkIsDueDateToday(activityDetailItem.endDate)
    }
    $scope.showDueDateFormat = function(event){
        if(!checkObjectItemNull(event)){
            return false;
        }
        return showDueDateFormat(event.endDate)
    }
    $scope.goToEditActivity = function(activityDetailItem){
        ApplicationFactory.setEditActivityDetails(activityDetailItem)
        $state.go('edit-activity')
    }
    $scope.setPollSingleChoiceAnswer = function(pollAnswer){
        $scope.pollingAnswers = [];
        $scope.pollingAnswers.push(pollAnswer)
        $scope.saveUserAnswers()
    }
    $scope.setPollMultiChoiceAnswer = function(pollAnswer, index){
        if($scope.pollingAnswers.indexOf(pollAnswer)===-1){
            $scope.pollingAnswers.push(pollAnswer)
        }else{
            var temp = [];
            angular.forEach($scope.pollingAnswers, function(value){
                if(value!=pollAnswer){
                    temp.push(value)
                }
            })
            $scope.pollingAnswers = temp;
        }
        $scope.saveUserAnswers();
    }
    $scope.checkUserPolls = function(value){
        if(checkObjectItemNull($scope.activityDetailItem.pollingAnswers)){
            var i = $scope.activityDetailItem.pollingAnswers.indexOf(value)
            if(i!==-1){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
    $scope.setPollingAnswers = function(items){
        if($scope.activityDetailItem.pollType=='single'){
            $scope.setPollSingleChoiceAnswer()
        }else{
            angular.forEach(items, function(value, key){
                $scope.setPollMultiChoiceAnswer(value)
            });
        }
    }
    $scope.saveUserAnswers = function(){
        ApplicationFactory.showIonicLoader()
        var item = $scope.createAnnotationForm();
        item.pollingAnswers = null
        item.pollingAnswers = $scope.pollingAnswers
        gapi.client.activitycalendar.saveUserAnnotations(item).execute(function(resp) {
            $scope.$apply(function(){
                if(!resp.code){
                    $scope.activityDetailItem.pollingAnswers = resp.pollingAnswers
                    ApplicationFactory.setActivityDetailItem($scope.activityDetailItem)
                }else{
                    console.log(resp.message)
                }
                ApplicationFactory.hideIonicLoader()
            })
        })
    }
    $scope.stopRefresh = function(){
        $scope.$broadcast('scroll.refreshComplete');
    }
    $scope.getActivityItemInDetail = function(activityDetailItem){
        //activityDetailItem.groupIdsList = activityDetailItem.groupIdList
        var item = $scope.createAnnotationForm()
        gapi.client.activitycalendar.getActivityInDetail(item).execute(function(resp) {
            $scope.$apply(function(){
                if(!resp.code){
                    $scope.activityDetailItem = resp.result;
                    $scope.totalSentUsersCount = $scope.getSentUserCount(resp.result.totalUsers)
                    $scope.totalReadUsersCount = $scope.getReadUserCount(resp.result.totalReadUsers)
                }else{
                    $scope.activityDetailItem = activityDetailItem;
                    // show error message
                    console.log(resp.message)
                }
                if(checkObjectItemNull($scope.activityDetailItem.pollingAnswers)){
                    $scope.setPollingAnswers($scope.activityDetailItem.pollingAnswers)
                }
                $scope.activityLoaded = true;
                $scope.getActivityChildData()
                $scope.getActivityInstituteNames()
                $scope.pieChart.labels = ['Read Users', 'Unread Users'];
                $scope.pieChart.data = [$scope.totalReadUsersCount, $scope.totalSentUsersCount]
                $scope.pieChart.show = true;
                $scope.pieChart.type = "Doughnut"
                ApplicationFactory.setActivityDetailItem($scope.activityDetailItem)
                ApplicationFactory.hideIonicLoader()
                $scope.stopRefresh()
            })
        })
    }
}])

genieControllers.controller('ActivityReadUsersController', ['$scope','ApplicationFactory','$ionicScrollDelegate',
             function ($scope, ApplicationFactory, $ionicScrollDelegate) {
    $scope.usersLoaded = false;
    $scope.$on("$ionicView.enter", function( scopes, states ) {
        $scope.userLoginData = ApplicationFactory.getUserLoginData()
        if(!checkStringItemNull($scope.userLoginData.userType)){
            $state.go('user-roles')
            return true;
        }
        ApplicationFactory.showIonicLoader()
        $scope.usersLoaded = false
        ApplicationFactory.setAppState('activity-read-users');
        $scope.readUsersList = ApplicationFactory.getActivityReadUsersList();
        $scope.activityDetailItem = ApplicationFactory.getActivityDetailItem();
        $scope.readUsersList = {'id':$scope.activityDetailItem.id, 'userList':[]}
        if(!checkObjectItemNull($scope.readUsersList)){
            $scope.getActivityReadUsersList()
        }else if($scope.activityDetailItem.id!=$scope.readUsersList.id){
            $scope.getActivityReadUsersList()
        }else if($scope.readUsersList.userList.length<1){
            $scope.getActivityReadUsersList()
        }else{
            $scope.usersLoaded = true
        }
        ApplicationFactory.setApplicationStates();
    });
    $scope.$on("$ionicView.leave", function( scopes, states ) {
        $scope.readUsersList = [];
        $scope.activityDetailItem = null;
        $scope.usersLoaded = false
    })
    $scope.getActivityReadUsersList = function(){
        var item = {}
        item.activityId = $scope.activityDetailItem.id;
        item.createdBy = $scope.activityDetailItem.activityOwner;
        item.userType = $scope.userLoginData.userType;
        if(gapi.client.activitycalendar===undefined){
            gapi.client.load('activitycalendar', 'v1', function() {
                $scope.getUserList(item)
            },getGapiURL() + '/_ah/api');
        }else{
            $scope.getUserList(item)
        }
    }
    $scope.stopRefresh = function(){
        $scope.$broadcast('scroll.refreshComplete');
    }
    $scope.getUserList = function(data){
        gapi.client.activitycalendar.getReadUsersList(data).execute(function(resp) {
            $scope.$apply(function(){
                if (!resp.code){
                    if(checkObjectItemNull(resp.result.items)){
                        $scope.readUsersList.userList = resp.result.items
                    }
                    ApplicationFactory.setActivityReadUsersList($scope.readUsersList)
                }
                $ionicScrollDelegate.resize();
                $scope.usersLoaded = true;
                ApplicationFactory.hideIonicLoader()
                $scope.stopRefresh()
            })
        })
    }
    $scope.refreshReadUsers = function(){
        $scope.getActivityReadUsersList();
    }
}])

genieControllers.controller('UserCalendarController',['$scope', 'ApplicationFactory', 'calendarConfig', '$state', '$ionicPopup','$ionicScrollDelegate',
                    function($scope, ApplicationFactory, calendarConfig, $state, $ionicPopup, $ionicScrollDelegate){        
    $scope.userCalendarData = ApplicationFactory.getUserCalendarData();
    $scope.userLoginData = ApplicationFactory.getUserLoginData()
    $scope.selectedFilterGroups = [];
    $scope.selectedParentFilters = []
    var userGroupsData = ApplicationFactory.getUserGroupsData()
    $scope.calendar = {};
    calendarConfig.startingDayWeek = 1;
    calendarConfig.calendarMode = "day"
    calendarConfig.queryMode = "remote"
    calendarConfig.formatWeekViewDayHeader = "EEE, dd MMM"
    $scope.tokenInitialized = false;
    $scope.isLocked = false;
    $scope.activityLoaded = false;
    $scope.doRefreshOnce = false
    $scope.changeMode = function ($event, mode) {
        if($event.srcElement.className==='next-icon calendar-state-NB'){
            $scope.nextView()
        }else if($event.srcElement.className==='back-icon calendar-state-NB'){
            $scope.previousView()
        }else{
            if($scope.calendar.mode!='day' && mode=='day'){
                $scope.calendar.currentDate = new Date();
            }
            $scope.calendar.mode = mode;
        }
    };
    $scope.previousView = function($event){
        ApplicationFactory.navigateCalendar(-1)
    }
    $scope.nextView = function($event){
        ApplicationFactory.navigateCalendar(1)
    }
    $scope.onEventSelected = function (event) {
        var foundEvent = _.findWhere($scope.calendar.eventSource,{'id':event.id})
        if(checkObjectItemNull(foundEvent)){
            ApplicationFactory.setActivityDetailItem(foundEvent)
            $state.go('activity-details');
        }
    };
    $scope.onViewTitleChanged = function (title) {
        $scope.viewTitle = title;
    };
    $scope.today = function () {
        $scope.calendar.currentDate = new Date();
    };
    $scope.showDayOrToday = function(){
        var currDate = $scope.calendar.currentDate;
        var todayDate = new Date();
        if(todayDate.getDate()==currDate.getDate() && todayDate.getMonth()==currDate.getMonth() && 
                todayDate.getFullYear()==currDate.getFullYear()){
            return true
        }else{
            return false
        }
    };
    $scope.showFileTypeImage = function(mimeType){
        return ApplicationFactory.showFileType(mimeType)
    }
    $scope.isToday = function () {
        var today = new Date(), currentCalendarDate = new Date($scope.calendar.currentDate);
        today.setHours(0, 0, 0, 0);
        currentCalendarDate.setHours(0, 0, 0, 0);
        return today.getTime() === currentCalendarDate.getTime();
    };
    $scope.doRefresh = function(){
        $scope.getUserCalendarData($scope.userLoginData, 'forceRefresh')
    }
    $scope.isDueDateToday = function(event){
        if(!checkObjectItemNull(activityDetailItem)){
            return false;
        }
        return checkIsDueDateToday(event.endTime)
    }
    $scope.showDueDateFormat = function(event){
        if(!checkObjectItemNull(activityDetailItem)){
            return false;
        }
        return showDueDateFormat(event.endTime)
    }
    $scope.getAttachmentUrl = function(attachmentURL){
        return ApplicationFactory.generateFileAttachment(attachmentURL);
    }
    $scope.downloadAttachment = function(attachment){
        downloadAttachment(attachment)
    }
    $scope.reloadSource = function(startTime, endTime){
        $scope.setCurrentCalendarRange(startTime)
        $scope.userLoginData.calendarEndDate = endTime
        $scope.userLoginData.calendarStartDate = startTime
        if($scope.userLoginData.userType=='parent'){
            $scope.getUserCalendarData($scope.userLoginData, null, true)
        }else{
            $scope.getUserCalendarData($scope.userLoginData, "forceRefresh", true)
        }
    }
    $scope.getCurrentCalendarRange = function(){
        return $scope.calendarDateRange;
    }
    $scope.setCurrentCalendarRange = function(startTime){
        $scope.calendarDateRange = startTime;
    }
    $scope.setCalendarDates = function(userLoginData){
        var currentDate = $scope.calendar.currentDate;
        if($scope.calendar.mode=='month'){
            $scope.userLoginData.startDate = new Date(setStartDate(new Date(currentDate.getFullYear(), currentDate.getMonth(), 1)));
            $scope.userLoginData.endDate = new Date(setEndDate(new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0)));
        }else if($scope.calendar.mode=='week'){
            var first = currentDate.getDate() - currentDate.getDay(); // First day is the day of the month - the day of the week
            var last = first + 6; // last day is the first day + 6
            var date = currentDate
            $scope.userLoginData.startDate = new Date(setStartDate(new Date(date.setDate(first))))
            $scope.userLoginData.endDate = new Date(setEndDate(new Date(date.setDate(last))))
        }else if($scope.calendar.mode=='day'){
            var date = currentDate
            $scope.userLoginData.startDate = new Date(setStartDate(date))
            $scope.userLoginData.endDate = new Date(setEndDate(date))
        }else{
            //By default go to month view
            $scope.userLoginData.startDate = new Date(setStartDate(new Date(currentDate.getFullYear(), currentDate.getMonth(), 1)));
            $scope.userLoginData.endDate = new Date(setEndDate(new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0)));
        }
        return $scope.userLoginData
    }
    $scope.getUserCalendarData = function(userLoginData, isForced, rangeChanged){
        ApplicationFactory.showIonicLoader()
        if($scope.isLocked==true){
            return true;
        }
        var flag = false;
        var dates = {}
        dates.calendarStartDate = $scope.userLoginData.calendarStartDate
        dates.calendarEndDate = $scope.userLoginData.calendarEndDate
        $scope.firstDayOfMonth = getMonthStartDate($scope.calendar.currentDate)
        $scope.lastDayOfMonth = getMonthLastData($scope.calendar.currentDate)
        $scope.calendarFirstDate = new Date(dates.calendarStartDate)
        $scope.calendarEndDate = new Date(dates.calendarEndDate)
        var todaysDate = new Date(setStartDate(new Date()))
        var currentDate = new Date().getTime();
        if($scope.calendar.mode=='month'){
            $scope.userLoginData.startDate = $scope.firstDayOfMonth;
            $scope.userLoginData.endDate = $scope.lastDayOfMonth;
            flag = true;
        }else if($scope.calendar.mode=='week'){
            var weekStart =  setWeekStartDate(new Date($scope.calendar.currentDate).getTime())
            var weekEndDate = new Date(new Date())
            if($scope.calendarEndDate.getTime()<=weekEndDate.getTime() || isForced=="forceRefresh" 
                    || $scope.userLoginData.userType=='teacher' || $scope.userLoginData.userType=='parent'){
                $scope.userLoginData.startDate = $scope.userLoginData.calendarStartDate
                $scope.userLoginData.endDate = new Date(setEndDate($scope.userLoginData.calendarEndDate))
                flag = true
            }
            if($scope.userLoginData.weekRefreshTime!=null &&  isForced!="forceRefresh" && $scope.userLoginData.userType!='teacher' 
                    && parseInt($scope.userLoginData.weekRefreshTime) - currentDate <= 1800){
                flag = false;
            }
        }else if($scope.calendar.mode=='day'){
            var currentDayDate = $scope.calendar.currentDate
            if(currentDayDate.getDate()<new Date().getDate() && currentDayDate.getMonth()<=new Date().getMonth() 
                    && currentDayDate.getFullYear()<=new Date().getFullYear()  && $scope.doRefreshOnce!=true){
                $scope.getOfflineCalendarData()
                return true;
            }else{
                $scope.userLoginData.startDate = $scope.calendar.currentDate
                $scope.userLoginData.endDate = new Date(setEndDate($scope.calendar.currentDate))  
            }
        }
        $scope.userLoginData.calendarStartDate = dates.calendarStartDate
        $scope.userLoginData.calendarEndDate = dates.calendarEndDate
        flag = true;
        if(flag==false){
            var calendarData = ApplicationFactory.getUserCalendarData();
            $scope.userCalendarData = $scope.pushForCalendarEvents(calendarData);
            if($scope.userLoginData.userType=='teacher'){
                $scope.calendar.eventSource = filterTeacherCalendarEvents($scope.userCalendarData, $scope.selectedFilterGroups, $scope.availableUserGroupList, $scope.userLoginData)
            }else if($scope.userLoginData.userType=='parent'){
                $scope.calendar.eventSource = filterParentCalendarEvents($scope.userCalendarData, $scope.selectedParentFilters, $scope.availableParentGroups) 
            }
            $ionicScrollDelegate.resize();
            $scope.stopRefresh()
            ApplicationFactory.hideIonicLoader()
        }else{
            var temp = [];
            if($scope.userLoginData.userType=='parent'){
                if(checkObjectItemNull(ApplicationFactory.getChindrensData())){
                    $scope.userLoginData.userGroupList = getChildGroups(ApplicationFactory.getChindrensData());
                }else{
                    $scope.userLoginData.userGroupList = temp;
                }
            }else{
                var userRolesData = ApplicationFactory.getUserRolesData()
                $scope.userLoginData.userGroupList = getLoggedUserGroupList(userRolesData, $scope.userLoginData.instituteId, $scope.userLoginData.userType)
            }
            if((window.userOnline || navigator.onLine)&& checkStringItemNull(gapi)){
                gapi.client.activitycalendar.getlistOfCalendarActivity($scope.userLoginData).execute(function(resp) {
                    var phase = $scope.$root.$$phase
                    if(phase==null){
                        $scope.$apply(function(){
                            if(!resp.code && resp.code!=-1 && resp.result.items && resp.result.items.length>0){
                                $scope.setCalendarData(resp)
                            }else{
                               $scope.getOfflineCalendarData() 
                            }
                        })
                    }else{
                        if(resp.code && resp.code!=-1 && resp.result.items && resp.result.items.length>0){
                            $scope.setCalendarData(resp)
                        }else{
                            $scope.getOfflineCalendarData()
                        }
                    }
                })
            }else{
                $scope.getOfflineCalendarData()
            }
        }
    }
    $scope.getOfflineCalendarData = function(){
        var calendarData = ApplicationFactory.getUserCalendarData();
        $scope.userCalendarData = $scope.pushForCalendarEvents(calendarData);
        if($scope.userLoginData.userType=='teacher'){
            $scope.calendar.eventSource = filterTeacherCalendarEvents($scope.userCalendarData, $scope.selectedFilterGroups, $scope.availableUserGroupList, $scope.userLoginData)
        }else if($scope.userLoginData.userType=='parent'){
            $scope.calendar.eventSource = filterParentCalendarEvents($scope.userCalendarData, $scope.selectedParentFilters, $scope.availableParentGroups) 
        }
        $ionicScrollDelegate.resize();
        $scope.stopRefresh()
        ApplicationFactory.hideIonicLoader()
        if($scope.doRefreshOnce==false && checkObjectItemNull($scope.calendar.eventSource)){
            $scope.doRefreshOnce = true
            $scope.getUserCalendarData()
        }else{
            $scope.doRefreshOnce = false
        }
    }
    $scope.setCalendarData = function(resp){
        if (!resp.code){
            $scope.activityLoaded = true;
            currentDate = new Date().getTime() 
            if($scope.calendar.mode=='day'){
                $scope.userLoginData.dayRefreshTime = currentDate
            }else if($scope.calendar.mode=='month'){
                $scope.userLoginData.monthRefreshTime = currentDate
            }else if($scope.calendar.mode=='week'){
                $scope.userLoginData.weekRefreshTime = currentDate
            }
            var tempArray = pushItemsToArray($scope.userCalendarData, resp.result.items)
            $scope.userCalendarData = $scope.pushForCalendarEvents(tempArray);
            if($scope.userLoginData.userType=='teacher'){
                $scope.calendar.eventSource = filterTeacherCalendarEvents($scope.userCalendarData, $scope.selectedFilterGroups, $scope.availableUserGroupList, $scope.userLoginData)
            }else if($scope.userLoginData.userType=='parent'){
               $scope.calendar.eventSource = filterParentCalendarEvents($scope.userCalendarData, $scope.selectedParentFilters, $scope.availableParentGroups) 
            }
            ApplicationFactory.setUserCalendarData($scope.userCalendarData);
            ApplicationFactory.setActivityStreamData($scope.userCalendarData);
            ApplicationFactory.setUserLoginData($scope.userLoginData)
        }else{
            ApplicationFactory.showUserMessage("",resp.message,"",3000)
        }
        $scope.isLocked = false;
        $ionicScrollDelegate.resize();
        ApplicationFactory.hideIonicLoader()
        $scope.stopRefresh()
        if($scope.doRefreshOnce==true){
            $scope.doRefreshOnce = false
        }
    }
    $scope.pushForCalendarEvents = function(calendarEvents){
        var events = []
        angular.forEach(calendarEvents, function(value, index){
            var startDate = new Date(value.startDate || value.startTime)
            var endDate = new Date(value.endDate || value.endTime)
            var updatedDate = new Date(value.updatedTime)
            events.push({
                title: value.title || "Event - " + index,
                startTime: startDate,
                endTime: endDate,
                allDay: false,
                description: value.description,
                groupIdList: value.groupIdList,
                id:value.id,
                activityType:value.activityType,
                updatedTime:updatedDate,
                attachments:value.attachments,
                instituteId:value.instituteId,
                activityOwner: value.activityOwner,
                read:value.read,
                like:value.like,
                completed:value.completed,
                teacherName:value.teacherName,
                activityChildData:$scope.getActivityChildData(value),
                instituteNames:$scope.getActivityInstituteNames(value)
            });
        })
        return events;
    }
    $scope.goToDate = function(date){
        $scope.changeMode('day')
    }
    $scope.onTimeSelected = function (selectedTime) {
        $scope.calendar.currentDate = selectedTime
        $scope.calendar.mode = "day"
    };
    $scope.groupsData = function(groupIds){
        var temp = [];
        angular.forEach(groupIds,function(value){
            var group = _.findWhere(userGroupsData,{'id':value})
            if(checkObjectItemNull(group)){
                temp.push(group)
            }
        })
        return temp;
    }
    $scope.getUserGroupsList = function(){
        userGroupsData = ApplicationFactory.getUserGroupsData()
        if(!checkObjectItemNull(userGroupsData)){
            userGroupsData = [];
        }
        return userGroupsData
    }
    $scope.$on("$ionicView.leave", function( scopes, states ) {
        $scope.userCalendarData = [];
        $scope.activityLoaded = false;
    })
    $scope.$on("$ionicView.enter", function( scopes, states ) {
        $scope.activityLoaded = false;
        $scope.userLoginData = ApplicationFactory.getUserLoginData()
        if(!checkStringItemNull($scope.userLoginData.userType)){
            $state.go('user-roles')
            return true;
        }
        ApplicationFactory.setAppState('user-calendar');
        $scope.deviceOrientation = checkDeviceOrientation()
        $scope.userCalendarData = ApplicationFactory.getUserCalendarData();
        userGroupsData = ApplicationFactory.getUserGroupsData();
        if((window.userOnline || navigator.onLine) && checkStringItemNull(gapi)){
            if(gapi.client.activitycalendar===undefined){
                gapi.client.load('activitycalendar', 'v1', function() {
                    $scope.getUserCalendarData($scope.userLoginData)
                },getGapiURL() + '/_ah/api');
            }else{
                $scope.getUserCalendarData($scope.userLoginData)   
            }
        }
        $scope.parentAvailableFilters = []
        ApplicationFactory.setApplicationStates();
        $scope.initializeDirectiveTokenInput()
    })
    $scope.checkActivityLoaded = function(){
        return $scope.activityLoaded
    }
    $scope.getActivityChildData = function(activity){
        var childs = []
        if(!checkObjectItemNull(activity)){
            return childs;
        }
        return ApplicationFactory.getActivityChildrenNames(activity);
    }
    $scope.getActivityInstituteNames = function(activity){
        var inst = []
        if(!checkObjectItemNull(activity)){
            return inst;
        }
        if($scope.userLoginData.userType=='parent'){
            return ApplicationFactory.getActivityChildInstituteNames(activity)
        }else{
            return ApplicationFactory.getActivityTeacherInstituteNames(activity)
        }
    }
    $scope.deleteActivity = function(event){
        var confirmPopup = $ionicPopup.confirm({
            template: window.userMessages.ACTIVITY_DELETE
        });
        confirmPopup.then(function(res) {
            if(res){
                var item = $scope.createAnnotationForm(event.id, event.groupIdList)
                ApplicationFactory.showIonicLoader();
                var promise = ApplicationFactory.deleteActivity(item);
                promise.then(function(result){
                    var phase = $scope.$root.$$phase
                    if(phase==null){
                        $scope.$apply(function(){
                            $scope.removeStreamItem(event.id)
                        })
                    }else{
                        $scope.removeStreamItem(event.id)
                    }
                    ApplicationFactory.showUserMessage("",window.userMessages.ACTIVITY_DELETE_SUCCESS,"",3000)
                },function(error){
                    ApplicationFactory.showUserMessage("",window.userMessages.ACTIVITY_DELETE_FAILURE,"",3000)
                })
                ApplicationFactory.hideIonicLoader();
            }
        });
    }
    $scope.removeStreamItem= function(eventId){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = deleteActivityCalendarItems(eventId, streamItems)
        ApplicationFactory.setActivityStreamData(streamItems)
        calendarItems = deleteActivityCalendarItems(eventId, calendarItems)
        ApplicationFactory.setUserCalendarData(calendarItems)
        $scope.calendar.eventSource = $scope.pushForCalendarEvents(calendarItems);
    }
    $scope.editActivity = function(event){
        ApplicationFactory.setEditActivityDetails(event)
        $state.go('edit-activity')
    }
    $scope.likeUnlikeActivity = function(event, flag){
        //$scope.saveEventLike(event.id, flag)
        var item = $scope.createAnnotationForm(event.id, event.groupIdList)
        item.like = flag;
        var promise = ApplicationFactory.saveLikeUnlikeAnnotations(item)
        promise.then(function(result){
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.saveEventLike(event.id, flag)
                })
            }else{
                $scope.saveEventLike(event.id, flag)
            }
        },function(error){
            ApplicationFactory.showUserMessage("", window.userMessages.ACTIVITY_LIKE_UNLIKE_FAILURE, "", 3000)
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.saveEventLike(event.id, !flag)
                })
            }else{
                $scope.saveEventLike(event.id, !flag)
            }
        })
    }
    $scope.saveEventLike = function(eventId, flag){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = saveActivityStreamCalendarLikeUnlike(eventId, streamItems, flag)
        ApplicationFactory.setActivityStreamData(streamItems)
        $scope.activityStreamContent = getTodaysActivityStream(streamItems, setStartDate(new Date), setEndDate(new Date))
        calendarItems = saveActivityStreamCalendarLikeUnlike(eventId, calendarItems, flag)
        $scope.calendar.eventSource = $scope.pushForCalendarEvents(calendarItems);
        ApplicationFactory.setUserCalendarData(calendarItems)
    }
    $scope.completeUncompleteActivity = function(event, flag){
        var item = $scope.createAnnotationForm(event.id, event.groupIdList)
        item.completed = flag;
        var promise = ApplicationFactory.saveCompleteUncompleteAnnotations(item)
        promise.then(function(result){
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.saveEventCompletion(event.id, flag)
                })
            }else{
                $scope.saveEventCompletion(event.id, flag)
            }
        },function(error){
            ApplicationFactory.showUserMessage("", window.userMessages.ACTIVITY_COMPLETE_UNCOMPLETE_FAILURE, "", 3000)
            var phase = $scope.$root.$$phase
            if(phase==null){
                $scope.$apply(function(){
                    $scope.saveEventCompletion(event.id, !flag)
                })
            }else{
                $scope.saveEventCompletion(event.id, !flag)
            }
        })
    }
    $scope.saveEventCompletion = function(eventId, flag){
        var streamItems = ApplicationFactory.getActivityStreamData()
        var calendarItems = ApplicationFactory.getUserCalendarData()
        streamItems = saveActivityStreamCalendarCompletion(eventId, streamItems, flag)
        ApplicationFactory.setActivityStreamData(streamItems)
        $scope.activityStreamContent = getTodaysActivityStream(streamItems, setStartDate(new Date), setEndDate(new Date))
        calendarItems = saveActivityStreamCalendarCompletion(eventId, calendarItems, flag)
        $scope.calendar.eventSource = $scope.pushForCalendarEvents(calendarItems);
        ApplicationFactory.setUserCalendarData(calendarItems)
    }
    $scope.createAnnotationForm = function(activityId, groupIds){
        userLoginData = ApplicationFactory.getUserLoginData()
        childData = ApplicationFactory.getChindrensData();
        var item = createAnnotationForm(activityId, groupIds, userLoginData, childData)
        return item;
    }
    $scope.initializeDirectiveTokenInput = function(){
        if($scope.userLoginData.userType=='teacher'){
            if(checkStringItemNull($scope.teacherInstituteId)){
                if($scope.teacherInstituteId!=$scope.userLoginData.instituteId){
                    $scope.emptySelectedFilterGroups()
                    $scope.teacherInstituteId = $scope.userLoginData.instituteId;
                }
            }else{
                $scope.teacherInstituteId = $scope.userLoginData.instituteId
            }
            $scope.availableUserGroupList =  $scope.getUserGroups()
        }else if($scope.userLoginData.userType=='parent'){
            $scope.parentFilters = $scope.createParentFilters();
        }
        $scope.prePopulateTokenInput()
    }
    $scope.getUserGroups = function(){
        $scope.userLoginData = ApplicationFactory.getUserLoginData()
        userGroupsData = ApplicationFactory.getUserGroupsData();
        userRoles = ApplicationFactory.getUserRolesData()
        var groupIds = null;
        var groups = [];
        if($scope.userLoginData.userType=='parent'){
            groupIds = getChildGroups(userRoles)
        }else if($scope.userLoginData.userType=='teacher'){
            groupIds = getLoggedUserGroupList(userRoles, $scope.userLoginData.instituteId, $scope.userLoginData.userType)
            groups.push({'displayGroupName':'My Schedule', 'value':'all', 'id':'all'})
        }
        angular.forEach(groupIds, function(groupId, key){
            var group = _.findWhere(userGroupsData, {'id':groupId})
            if(checkObjectItemNull(group)){
                groups.push(group);
            }
        });
        return groups;
    }
    $scope.createParentFilters = function(){
        userGroupsData = ApplicationFactory.getUserGroupsData();
        childrenData = ApplicationFactory.getChindrensData();
        var parentChildFilters = createParentChildFilters(childrenData)
        if(!checkObjectItemNull($scope.selectedParentFilters)){
            $scope.selectedParentFilters = []
            //$scope.selectedParentFilters.push({'displayName':'All', 'value':'all', 'id':'all'})
        }
        $scope.parentAvailableFilters = []
        //$scope.parentAvailableFilters.push({'displayName':'All', 'value':'all', 'id':'all'})
        var items = createParentInstituteFilters(childrenData, userGroupsData, parentChildFilters)
        angular.forEach(items, function(value, key){
            $scope.parentAvailableFilters.push(value)
        });
        return $scope.parentAvailableFilters;
    }
    $scope.loadTeacherFilters = function(query){
        var items = $scope.getUserGroups();
        if(!checkStringItemNull(query)){
            return items;
        }
        return items.filter(function(item){
            return (item.displayGroupName).toLowerCase().indexOf(query.toLowerCase()) !==-1
        })
    }
    $scope.loadParentFilters = function(query){
        var items = $scope.createParentFilters();
        if(!checkStringItemNull(query)){
            return items;
        }
        return items.filter(function(item){
            return (item.displayName).toLowerCase().indexOf(query.toLowerCase()) !==-1
        })
    }
    $scope.addParentGroup = function(group, index){
        var found = _.findWhere($scope.selectedParentFilters, {'id':group.id})
        if(!checkObjectItemNull(found)){
            $scope.selectedParentFilters.push(group)
        }
        $scope.calendar.eventSource = filterParentCalendarEvents($scope.userCalendarData, $scope.selectedParentFilters, $scope.availableParentGroups) 
        $ionicScrollDelegate.resize();
        //$ionicScrollDelegate.scrollTop();
    }
    $scope.removeParentGroup = function(group, index){
        var found = _.findWhere($scope.selectedParentFilters, {'id':group.id})
        if(checkObjectItemNull(found)){
            for(var i=0; i<$scope.selectedParentFilters.length; i++){
                if($scope.selectedParentFilters[i].id==group.id){
                    $scope.selectedParentFilters.splice(i, 1)                            
                    break;
                }
            }
            //$scope.selectedParentFilters.splice(index, 1)
        }
        $scope.calendar.eventSource = filterParentCalendarEvents($scope.userCalendarData, $scope.selectedParentFilters, $scope.availableParentGroups) 
        $ionicScrollDelegate.resize();
        //$ionicScrollDelegate.scrollTop();
    }
    $scope.getUserLoginData = function(){
        return $scope.userLoginData
    }
    $scope.getAvailableGroupList = function(){
        return $scope.availableUserGroupList;
    }
    $scope.getSelectedFilterGroups = function(){
        return $scope.selectedFilterGroups;
    }
    $scope.getSelectedParentFilters = function(){
        if(!checkObjectItemNull($scope.parentAvailableFilters)){
            $scope.createParentFilters()
        }
        return $scope.selectedParentFilters;
    }
    $scope.removeFilterGroup = function(filterGroup){
        if($scope.userLoginData.userType=='parent'){
            if($scope.selectedParentFilters.length==0){
                //$scope.prePopulateTokenInput()
            }
        }else if($scope.userLoginData.userType=='teacher'){
            if($scope.selectedFilterGroups.length==0){
                //$scope.prePopulateTokenInput()
            }
        }
        if($scope.userLoginData.userType=='teacher'){
            $scope.calendar.eventSource = filterTeacherCalendarEvents($scope.userCalendarData, $scope.selectedFilterGroups, $scope.availableUserGroupList, $scope.userLoginData)
        }else if($scope.userLoginData.userType=='parent'){
            $scope.calendar.eventSource = filterParentCalendarEvents($scope.userCalendarData, $scope.selectedParentFilters, $scope.availableParentGroups) 
        }
        $ionicScrollDelegate.resize();
        //$ionicScrollDelegate.scrollTop();
    }
    $scope.addFilterGroup = function(filterGroup){
        if($scope.userLoginData.userType=='parent'){
            if(filterGroup.id=='all'){
                $scope.selectedParentFilters = [];
                $scope.selectedParentFilters.push(filterGroup)
            }
            if($scope.selectedParentFilters.length>1){
                var found = _.findWhere($scope.selectedParentFilters, {'id':'all'})
                if(checkObjectItemNull(found)){
                    for(var i=0; i<$scope.selectedParentFilters.length; i++){
                        if($scope.selectedParentFilters[i].id=='all'){
                            $scope.selectedParentFilters.splice(i, 1)                            
                            break;
                        }
                    }
                }
            }
        }else if($scope.userLoginData.userType=='teacher'){
            if(filterGroup.id=='all'){
                $scope.selectedFilterGroups = [];
                $scope.selectedFilterGroups.push(filterGroup)
            }
            if(filterGroup.id!='all'){
                var found = _.findWhere($scope.selectedFilterGroups, {'id':'all'})
                if(checkObjectItemNull(found)){
                    for(var i=0; i<$scope.selectedFilterGroups.length; i++){
                        if($scope.selectedFilterGroups[i].id=='all'){
                            $scope.selectedFilterGroups.splice(i, 1)
                            break;
                        }
                    }
                }
            }
        }
        if($scope.userLoginData.userType=='teacher'){
            $scope.calendar.eventSource = filterTeacherCalendarEvents($scope.userCalendarData, $scope.selectedFilterGroups, $scope.availableUserGroupList, $scope.userLoginData)
        }else if($scope.userLoginData.userType=='parent'){
            $scope.calendar.eventSource = filterParentCalendarEvents($scope.userCalendarData, $scope.selectedParentFilters, $scope.availableParentGroups) 
        }
        $ionicScrollDelegate.resize();
        //$ionicScrollDelegate.scrollTop();
    }
    $scope.prePopulateTokenInput = function(){
        if(!checkObjectItemNull($scope.selectedFilterGroups) && $scope.userLoginData.userType=='teacher'){
            $scope.selectedFilterGroups.push({'displayGroupName':'My Schedule', 'value':'all', 'id':'all'})
        }else if(!checkObjectItemNull($scope.selectedParentFilters) && $scope.userLoginData.userType=='parent'){
            /*angular.forEach($scope.parentFilters, function(group){
                $scope.selectedParentFilters.push(group)
            })*/
        }
    }
    $scope.emptySelectedFilterGroups = function(){
        $scope.selectedFilterGroups = []
        $scope.selectedParentFilters = []
    }
    $scope.stopRefresh = function(){
        $scope.$broadcast('scroll.refreshComplete');
    }
}]);

genieControllers.controller('ApplicationFooterController', function($scope, ApplicationFactory){
    $scope.$on("$ionicView.enter", function( scopes, states ) {
        ApplicationFactory.setAppState('user-roles');
    });
    $scope.showFooter = false;
    $scope.checkFooterData = function(){
        return $scope.showFooter
    }
    $scope.$on('CheckApplicationFooter', function(event, param){
        $scope.stateName = param.data;
        if(param.data=='user-login'){
            $scope.showFooter = true
        }else{
            $scope.showFooter = false 
        }
    })
});

genieControllers.controller('ApplicationLoaderController', function($scope, ApplicationFactory){
    $scope.getImageUrl = function(fileName){
        var filePath = ""
        if (window.cordova && window.cordova.plugins) {
            filePath = cordova.file.applicationDirectory + 'www/img/'
        }else{
            filePath = "/app/img/"
        }
        return filePath + fileName;
    }
});