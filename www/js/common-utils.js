window.loggedInUser = null;
window.isWebView = false;
window.isAndroid = false;
window.isIOS = false;
window.isIpad = false;
window.genieApplicationReady = false;
window.gcmRequestLock = false
window.isPushEnabled = false
window.userMessages = {};
initializeUserMessages()
setConstants()
function app_init(){
	gapi.client.load('activitycalendar', 'v1', function() {
		gapi.client.load('commonutil', 'v1', function() {
			gapi.client.load('usermaster', 'v1', function() {
				var genieAppData = localStorage.getItem('genieApp.appUserData')
				if(!checkStringItemNull(genieAppData)){
					setTimeout(function(){
						window.location.hash = "#/login"
						setAppReady()
					}, 500)
				}else{
					genieAppData = JSON.parse(genieAppData)
					genieAppData.userLoginData = setDayStartLoginDates(genieAppData.userCalendarData, genieAppData.userLoginData)
					genieAppData.userLoginData.startDate = convertToDate(genieAppData.userLoginData.startDate)
					genieAppData.userLoginData.endDate = convertToDate(genieAppData.userLoginData.endDate)
					if(genieAppData.userLoginData.userType=='parent'){
						genieAppData.userLoginData.userGroupList = getChildGroups(genieAppData.childrensData)
					}else{
						genieAppData.userLoginData.userGroupList = getLoggedUserGroupList(genieAppData.userRolesData, genieAppData.userLoginData.instituteId,
																										genieAppData.userLoginData.userType)
					}
					if(genieAppData.userLoginData.loginDate!=null && genieAppData.userLoginData.loginDate!=false && checkDateEqualsToday(genieAppData.userLoginData.loginDate)){
						localStorage.setItem('genieApp.appUserData', JSON.stringify(genieAppData))
						if(window.location.hash===undefined || window.location.hash==null || window.location.hash=="" || window.location.hash=="#/login"){
							window.location.hash = "#/activity-stream"
						}else{
							window.location.hash = window.location.hash
						}
						setTimeout(function(){
							setAppReady()
						},500)
					}else{
						gapi.client.usermaster.doUserLogin(genieAppData.userLoginData).execute(function(resp) {
							if (!resp.code){
								genieAppData.userLoginData.loginDate = new Date(setStartDate(new Date()))
								genieAppData.userLoginData.genieId = getUserGenieId(resp.result.userRolesData, genieAppData.userLoginData)
								genieAppData.userCalendarData = pushItemsToArray(genieAppData.userCalendarData, resp.result.calendarActivity)
								genieAppData.userActivityData = pushItemsToArray(genieAppData.userActivityData, resp.result.calendarActivity);
								genieAppData.userGroupsData = resp.result.groupMasterWrapper;
								genieAppData.userRolesData = resp.result.userRolesData;
								localStorage.setItem('genieApp.appUserData', JSON.stringify(genieAppData))
								setUser(genieAppData.userLoginData)
								if(isChromeBrowser()){
									subscribeToGCM()
								}
								if(window.location.hash===undefined || window.location.hash==null || window.location.hash=="" || window.location.hash=="#/login"){
									window.location.hash = "#/activity-stream"
								}else{
									window.location.hash = window.location.hash
								}
							}else{
								if(window.location.hash=='#/switch-roles' && checkStringItemNull(genieAppData.userLoginData.userName)){
									window.location.hash=='#/switch-roles'
								}else{
									removeLocalstorage()
									window.location.hash = "#/login"
								}
							}
							setTimeout(function(){
								setAppReady()
							},500)
						})
					}
				}
			},getGapiURL() + '/_ah/api');
		},getGapiURL() + '/_ah/api');
	},getGapiURL() + '/_ah/api');
}
function setAppReady(){
	angular.bootstrap(document,['genieApp']);
	window.genieApplicationReady = true;
}
function checkStringItemNull(param){
	if(param===undefined || param=="" || param==null || param=="null"){
		return false
	}else{
		return true
	}
}
function checkArrayItemNull(param){
	if(param===undefined || param==null){
		return false
	}
	if(param.length<1){
		return false
	}else{
		return true
	}
}
function checkObjectItemNull(param){
	if(param===undefined || param==null || param==""){
		return false
	}
	if(Object.keys(param).length<1){
		return false
	}else{
		return true
	}
}
function getGapiURL(icon){
	return "https://2-dot-wyze-prod.appspot.com"
	if(icon==true){
		if(window.location.host.search('wyze')!==-1 && (window.location.host=='www.wyze.in' || window.location.host=='wyze.in')){
			return "https://" + window.location.host
		}else{
			if(window.cordova){
				return "https://www.wyze.in"
			}else{
				var protocol = window.location.host.search('localhost')==-1 ? 'https://' : 'http://'
				return protocol + window.location.host
			}
		}
	}
	if(window.location.host.search('wyze')!==-1 || window.cordova){
		//return "https://1-dot-wyze-prod.appspot.com"
		return "https://2-dot-wyze-prod.appspot.com"
	}else if(window.location.host.search('192.168')!==-1){
		return "https://2-dot-wyze-prod.appspot.com"
	}else{
		var protocol = window.location.host.search('localhost')==-1 ? 'https://' : 'http://'
		return protocol + window.location.host;
	}
}

function removeLocalstorage(){
	localStorage.removeItem('genieApp.appUserData')
}

function setStartDate(startDate){
	startDate = convertToDate(startDate)
	startDate = new Date(startDate.getFullYear(), startDate.getMonth(), startDate.getDate()).setHours(0,0,0,0)
	return startDate;
}
function setEndDate(endDate){
	endDate = convertToDate(endDate)
	endDate = new Date(endDate.getFullYear(), endDate.getMonth(), endDate.getDate()).setHours(23,59,59,999)
	return endDate;
}
function setWeekStartDate(date){
	var endDate = new Date(date.getFullYear(), date.getMonth(), date.getDate()).setHours(23,59,59,999)
	return endDate;
}
function getEventsBetween(events, startDate, endDate){
	var temp = [];
	var startTime = startDate.getTime();
	var endTime = endDate.getTime();
	angular.forEach(events, function(event){
		if(event.startTime>=startTime && event.endTime<=endTime){
			temp.push(event)
		}
	})
	return temp;
}

function getEventsBetweenByDate(events, startDate, endDate){
	var temp = [];
	angular.forEach(events, function(event){
		if(new Date(event.startTime).getTime()>=startDate && new Date(event.endTime).getTime()<=endDate){
			temp.push(event)
		}
	})
	return temp;
}

function getTodaysActivityStream(events, startDate, endDate){
	var temp = [];
	angular.forEach(events, function(event){
		if(new Date(event.updatedTime).getTime()>=startDate && new Date(event.updatedTime).getTime()<=endDate){
			temp.push(event)
		}
	})
	return temp;
}

function checkUserOnline(){
	var flag = false;
	if(!window.cordova && navigator.onLine){
		flag = true
	}
	return flag;
}

function sortArrayByDate(events){
	events.sort(function(a,b){
		return new Date(b.updatedTime).getTime() - new Date(a.updatedTime).getTime()
	});
	return events;
}

function getMonthStartDate(date){
	if(typeof(date) != 'object'){
		date = new Date(date)
	}
	var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
	return firstDay;
}

function getMonthLastData(date){
	if(typeof(date) != 'object'){
		date = new Date(date)
	}
	var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
	return lastDay;
}

function setDayStartLoginDates(events, loginData){
	var calendarStartDate = new Date()
	if(checkObjectItemNull(events)){
		var events = sortArrayByDate(events)
		loginData.calendarStartDate = new Date(events[events.length-1].startDate);
		loginData.calendarEndDate = new Date(events[0].endDate).setHours(0,0,0,0)
		loginData.calendarEndDate = new Date(loginData.calendarEndDate);
	}else{
		loginData.calendarStartDate = new Date(new Date(calendarStartDate).setHours(0,0,0,0));
		loginData.calendarEndDate = new Date(new Date(calendarStartDate).setHours(0,0,0,0));
	}
	return loginData;
}

function checkDateEqualsToday(date){
	var todayDate = new Date(setStartDate(new Date()))
	if(new Date(date).getTime() == todayDate.getTime()){
		return true;
	}else{
		return false;
	}
}

function pushItemsToArray(oldActivityData, activityData){
	if(checkObjectItemNull(oldActivityData) && checkObjectItemNull(activityData)){
    	for(var i=0; i<activityData.length; i++){
			for(var j=0; j<oldActivityData.length; j++){
				var item = _.findWhere(oldActivityData, {'id':activityData[i].id})
				if(activityData[i].id == oldActivityData[j].id && checkObjectItemNull(item)){
					var update1 = "", update2 = "";
					if(checkObjectItemNull(activityData[i].updatedDate)){
						update1 = new Date(activityData[i].updatedDate).getTime();
					}else{
						update1 = new Date(activityData[i].updatedTime).getTime();
					}
					if(checkObjectItemNull(oldActivityData[i].updatedDate)){
						update2 = new Date(oldActivityData[i].updatedDate).getTime();
					}else{
						update2 = new Date(oldActivityData[i].updatedTime).getTime();
					}
					if(update1>update2){
						oldActivityData.splice(j, 1, activityData[i])
						continue;
					}
				}else if(!checkObjectItemNull(item)){
					oldActivityData.push(activityData[i])
				}
			}
			//oldActivityData.push(activityData[i])
		}
		return oldActivityData
    }else if(!checkObjectItemNull(oldActivityData) && checkObjectItemNull(activityData)){
    	return activityData
    }else if(!checkObjectItemNull(activityData) && checkObjectItemNull(oldActivityData)){
    	return oldActivityData
    }else if(!checkObjectItemNull(oldActivityData) && !checkObjectItemNull(activityData)){
    	var temp = [];
    	return temp;
    }
}

function convertToDate(date){
	if(typeof(date) != 'object'){
		date = new Date(date)
	}
	return date;
}

function getChildGroups(userRoles){
	var temp = [];
	angular.forEach(userRoles, function(userRole, key){
		if(checkObjectItemNull(userRole.readGroups)){
			angular.forEach(userRole.readGroups, function(groupId){
				temp.push(groupId)
			})
		}
		if(checkObjectItemNull(userRole.filterGroups)){
			angular.forEach(userRole.filterGroups, function(groupId){
				temp.push(groupId)
			})
		}
		if(checkObjectItemNull(userRole.writeGroups)){
			angular.forEach(userRole.writeGroups, function(groupId){
				temp.push(groupId)
			})
		}
	});
	temp = _.uniq(temp)
	return temp;
}

function createParentChildFilters(childrensData){
	var temp = [];
	angular.forEach(childrensData, function(children, key){
		var instituteIds = []
		instituteIds.push(children.instituteWrapper.id);
		var foundChild = _.findWhere(temp, {'displayName':children.firstName})
		if(!checkObjectItemNull(foundChild)){
			temp.push({'displayName':children.firstName, 'lastName':children.lastName, 'type':'student', 
								'readGroups':children.readGroups, 'instituteIds':instituteIds, 'id':temp.length+1})
		}else{
			readGroups = _.union(foundChild.readGroups,children.readGroups)
			instituteIds = _.union(foundChild.instituteIds,instituteIds)
			angular.forEach(temp, function(tempChildData, key){
				if(tempChildData.displayName==children.firstName){
					temp[key].readGroups = readGroups
					temp[key].instituteIds = instituteIds
				}
			});
		}
	});
	return temp;
}

function createParentInstituteFilters(childrensData, userGroups, childData){
	var temp = [];
	angular.forEach(childrensData, function(children, key){
		var groupIds = [];
		var foundInstitute = _.findWhere(temp, {'instituteId':children.instituteWrapper.id})
		if(!checkObjectItemNull(foundInstitute)){
			angular.forEach(userGroups, function(group, index){
				if(children.instituteWrapper.id==group.instituteId){
					groupIds.push(group.id)
				}
			})
			groupIds = _.uniq(groupIds)
			/*temp.push({'instituteId':children.instituteWrapper.id, 'type':'institute', 'displayName':children.instituteWrapper.displayName, 
					'instGroups':groupIds, 'lastName':'', 'id':childData.length+temp.length+1})*/
		}
	});
	angular.forEach(temp, function(value, key){
		childData.push(value)
	});
	return childData;
}

function getLoggedUserWriteGroupList(userRoles, instituteId, userType){
	var temp = [];
	angular.forEach(userRoles, function(userRole, key){
		if(userRole.instituteWrapper.id==instituteId && userRole.userRole==userType){
			if(checkObjectItemNull(userRole.writeGroups)){
				angular.forEach(userRole.writeGroups, function(groupId){
					temp.push(groupId)
				})
			}
			temp = _.uniq(temp)
		}
	});
	return temp;
}

function getLoggedUserGroupList(userRoles, instituteId, userType){
	var temp = [];
	angular.forEach(userRoles, function(userRole, key){
		if(userRole.userRole==userType){
			if(userRole.instituteWrapper.id==instituteId){
				if(checkObjectItemNull(userRole.readGroups)){
					angular.forEach(userRole.readGroups, function(groupId){
						temp.push(groupId)
					})
				}
				if(checkObjectItemNull(userRole.filterGroups)){
					angular.forEach(userRole.filterGroups, function(groupId){
						temp.push(groupId)
					})
				}
				if(checkObjectItemNull(userRole.writeGroups)){
					angular.forEach(userRole.writeGroups, function(groupId){
						temp.push(groupId)
					})
				}
				temp = _.uniq(temp)
			}
		}
	});
	return temp;
}

function getUserGenieId(userRolesData, userLoginData){
	var userId = "";
	if(userLoginData.userType=='teacher'){
		if(!checkStringItemNull(userLoginData.instituteId)){
			return userId;
		}else{
			angular.forEach(userRolesData, function(userRole, key){
				if(userLoginData.instituteId==userRole.instituteWrapper.id && userRole.userRole=='teacher'){
					userId = userRole.id
				}
			});
		}
	}else if(userLoginData.userType=='parent'){
		angular.forEach(userRolesData, function(userRole){
			if(userRole.userRole=='parent'){
				userId = userRole.id;
			}
		})
	}
	return userId;
}
function downloadAttachment(attachment){
	var protocol = 'http://';
	var appspotsite = protocol + window.location.host + "/downloadFile?blob-id="+attachment.id;
	var a = document.createElement('a');
	a.setAttribute("href",appspotsite);
	a.setAttribute("target", "_blank");
	var dispatch = document.createEvent("HTMLEvents");75
	dispatch.initEvent("click", true, true);
	a.dispatchEvent(dispatch);
	return false;
}

function setUser(user){
	window.loggedInUser = user
}

function getUser(){
	return window.loggedInUser;
}
function isAndroidBrowser(){
	if(navigator.userAgent.match(/Android/i)){return true;}
	else{return false;}
}
function isBlackBerryBrowser(){
	if(navigator.userAgent.match(/BlackBerry/i)){return true;}
	else{return false;}
} 
function isiOSBrowser(){
	if(navigator.userAgent.match(/iPhone|iPad|iPod/i)){return true;}
	else{return false;}
} 
function isOperaMiniBrowser(){
	 if(navigator.userAgent.match(/Opera Mini/i)){return true;}
	 else{return false;}
} 
function isWindowsBrowser(){
	if(navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i)){return true;}
	else{return false;}
}
 function isChromeBrowser(){
	if( navigator.userAgent.toLowerCase().indexOf('chrome') > -1 ){return true;}
	else{return false;}
}
function isFirefoxBrowser(){
	if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){return true;}
	else{return false;}
}
function isSafariBrowser(){
	if(navigator.userAgent.toLowerCase().indexOf("Safari") > -1){return true;}
	else{return false;}
}
function isIEBrowser(){
	if(navigator.userAgent.toLowerCase().indexOf("MSIE") > -1){return true;}
	else{return false;}
}
function isOperaBrowser(){
	if(navigator.userAgent.toLowerCase().indexOf("Opera") > -1){return true;}
	else{return false;}
}
function chromeOniOS(){
	if(navigator.userAgent.match('CriOS')){
		return true
	}else{
		return false
	}
}
function checkUserOnBrowser(){
	if(chromeOniOS() || isOperaBrowser() || isIEBrowser() || isSafariBrowser() || isFirefoxBrowser() 
		|| isChromeBrowser() || isWindowsBrowser() || isOperaMiniBrowser() || isiOSBrowser() || isBlackBerryBrowser() || isAndroidBrowser()){
		return true
	}else {
		return false
	}
}
function checkUserOnMobileBrowser(){
	if(chromeOniOS() || isWindowsBrowser() || isOperaMiniBrowser() || isiOSBrowser() || isBlackBerryBrowser() || isAndroidBrowser()){
		return true
	}else {
		return false
	}
}
function isGCMSupport(){
	if(isIEBrowser() || isBlackBerryBrowser() || isiOSBrowser() || isOperaMiniBrowser() || isWindowsBrowser()
			|| isSafariBrowser() || chromeOniOS()){
		return false 
	}else{
		return true
	}
}
function isGCMSubscribed(){
	var user = getUser();
	if(!checkObjectItemNull(user)){
		return false
	}
	if(user.loggedIn==false){
		return false
	}
	var gcmKey = localStorage.getItem("GenieApp.gcmKey");
	var gcmEnabled = localStorage.getItem("GenieApp.gcmEnabled");
	if(gcmEnabled=="true" && gcmKey!=""){
		return true;
	}else{
		return false;
	}	
}
function subscribeToGCM(){
	if(isChromeBrowser() || isAndroidBrowser()){
		if(!isGCMSubscribed()){
			subscribe();
		}
	}else{
		console.show("Error to user")
		//showUserDynamicMessages('gcmNotSupport')
	}	
}
function unsubScribeFromGCM(){
	if(isChromeBrowser() || isAndroidBrowser()){
			unsubscribe();			
		}else{
			console.show("Error to user")
		}
}
function findGroupIdsDeep(id, groups, addedGroups){
	angular.forEach(groupsFound, function(value){
		addedGroups.push(value.id)
		findGroupIdsDeep(value.id, groups, addedGroups)
	})
	return addedGroups
}
function createAnnotationForm(activityId, groupIdsList, userLoginData, childData){
	var item = {};
	item.activityId = activityId 	//activityDetailItem.id;
	item.userType = userLoginData.userType;
	item.createdBy = userLoginData.genieId;
	item.groupIdList = 	groupIdsList //	activityDetailItem.groupIdList;
	item.userIds = []
	if(item.userType=='parent'){
		var temp = [];
		if(checkObjectItemNull(childData)){
		    angular.forEach(childData, function(child, key){
			    var childGroups = [];
		        angular.forEach(child.readGroups, function(group, key){
		            childGroups.push(group)
		        });
		        childGroups = _.uniq(childGroups)
		        var foundGroups = _.intersection(groupIdsList, childGroups)
		        if(foundGroups.length>0){
		            temp.push(child.id)
		        }
		    });
		}
		item.userIds = temp
	}else if(item.userType=='teacher'){
		item.userIds.push(userLoginData.genieId)
	}
	return item;
}
function deleteActivityCalendarItems(eventId, activities){
	for(var i=activities.length; i>0; i--){
		if(activities[i-1].id==eventId){
			activities.splice(i-1, 1)
		}
	}
	return activities;
}
function saveActivityStreamCalendarLikeUnlike(eventId, activities, flag){
	for(var i=0; i<activities.length; i++){
		if(activities[i].id==eventId){
			activities[i].like = flag;
		}
	}
	return activities;
}
function saveActivityStreamCalendarCompletion(eventId, activities, flag){
	for(var i=0; i<activities.length; i++){
		if(activities[i].id==eventId){
			activities[i].completed = flag;
		}
	}
	return activities;
}
function checkIsDueDateToday(endTime){
	var todayDate = new Date().getTime();
    var eventEnd = new Date(endTime).getTime();
    if(eventEnd>=todayDate){
       	return true
    }else{
    	return false
    }
}
function showDueDateFormat(endTime){
	var todayDate = new Date()
    var eventEnd = new Date(endTime)
    if(todayDate.getDate()==eventEnd.getDate() && todayDate.getMonth()==eventEnd.getMonth() && todayDate.getFullYear()==eventEnd.getFullYear()){
       	return true
    }else{
    	return false
    }
}
function checkDeviceOrientation(){
	var mql = window.matchMedia("(orientation: portrait)");
	if(mql.matches) {  
    	return "portrait"
	} else {  
        return "landscape"
    }
    mql.addListener(function(m) {
    	if(m.matches) {
        	return "portrait"
    	}else {
            return "landscape"
        }
    });
}
function checkImageExist(fileMimeType){
	var type = fileMimeType.slice(0,6);
	if(type==='image/'){
		return true
	}else{
		return false
	}
}
function checkVideoExist(fileMimeType){
	var type = fileMimeType.slice(0,6);
	if(type==='video/'){
		return true
	}else{
		return false
	}
}
function getImageFileName(mimeType){
	if(mimeType.indexOf('video')!==-1 && mimeType!='video/x-flv'){
		return "attachment-video.svg"
	}else if(mimeType=="application/octet-stream"){
		return "attachment-psd.svg"
	}else if(mimeType.indexOf('audio')!==-1){
		return "attachment-mp3.svg"
	}else if(mimeType=="application/x-shockwave-flash" || mimeType=="image/vnd.rn-realflash"){
		return "attachment-flash.svg"
	}else if(mimeType=='application/pdf'){
		return "attachment-pdf.svg"
	}else if(mimeType=="application/x-indesign"){
		return "attachment-indesign.svg"
	}else if(mimeType.indexOf('font')!==-1){
		return "attachment-font.svg"
	}else if(mimeType.indexOf('excel')!==-1){
		return "attachment-excel.svg"
	}else if(mimeType=='text/html'){
		return "attachment-code.svg"
	}else if(mimeType=='application/postscript'){
		return "attachment-ai.svg"
	}else if(mimeType.indexOf('effects')!==-1){
		return "attachment-ae.svg"
	}else if(mimeType=='video/x-flv'){
		return "attachement-fla.svg"
	}else if(mimeType.indexOf('image')!==-1){
		return "attachment-img.svg"
	}else{
		return "attachment-pdf.svg"
	}
}

function filterTeacherCalendarEvents(events, selectedFilters, availableFilters, userLoginData){
	var temp = [];
	var flag = false;
	if(!checkObjectItemNull(events)){
		events = [];
		return events;
	}
	if(!checkObjectItemNull(selectedFilters)){
		var temp = sortCalendarEvents(events)
		return temp;
	}
	angular.forEach(selectedFilters, function(value){
		if(value.id=='all'){
			flag = true;
		}
	})
	if(flag==true){
		angular.forEach(events, function(event){
			if(event.activityOwner==userLoginData.genieId){
				temp.push(event)
			}
		})
		temp = sortCalendarEvents(temp)
		return temp
	}
	var availableUserGroupList = availableFilters
	var parentGroupIds = [];
	var level1GroupIds = [];
	var level2GroupIds = [];
	angular.forEach(selectedFilters, function(value){
		parentGroupIds.push(value.id)
	})
	angular.forEach(parentGroupIds, function(value){
		angular.forEach(availableUserGroupList, function(group, key){
			if(group.parentGroupId==value){
				level1GroupIds.push(group.id)
			}
		});
	})
	angular.forEach(level1GroupIds, function(value){
		angular.forEach(availableUserGroupList, function(group, key){
			if(group.parentGroupId==value){
				level2GroupIds.push(group.id)
			}
		});
	})
	parentGroupIds = _.union(parentGroupIds, level1GroupIds, level2GroupIds)
	angular.forEach(events, function(event, key){
		var foundGroups = _.intersection(event.groupIdList, parentGroupIds)
    	if(foundGroups.length>0){
    		temp.push(event)
    	}
	});
	temp = sortCalendarEvents(temp)
	return temp;
}

function filterParentCalendarEvents(events, selectedFilters, availableFilters){
	var temp = [];
	var flag = false;
	if (!checkObjectItemNull(events)) {
		events = []
		return events;
	}
	if(!checkObjectItemNull(selectedFilters)){
		var temp = sortCalendarEvents(events)
		return temp;
	}
	var selectedChilds = [];
	var selectedInstitutes = [];
	angular.forEach(selectedFilters, function(value){
		if(value.type=='student'){
			selectedChilds.push(value)
		}else if(value.type=='institute'){
			selectedInstitutes.push(value)
		}
	})
	angular.forEach(events, function(event, key){
		var flag = true;
		angular.forEach(selectedChilds, function(child, key){
			var commonGroups = _.intersection(child.readGroups, event.groupIdList)
			if(commonGroups.length>0){
				var found = _.findWhere(temp,{'id':event.id})
				if(!checkObjectItemNull()){
					temp.push(event)
				}
			}
		});
	});
	temp = sortCalendarEvents(temp)
	return temp;
}

function sortCalendarEvents(events){
	for(var i=0; i<events.length; i++){
		for(var j=i+1; j<events.length; j++){
			var t1 = new Date(events[i].updatedTime).getTime()
			var t2 = new Date(events[j].updatedTime).getTime()
			if(t1 < t2){
				var temp = events[i]
				events[i] = events[j]
				events[j] = temp;
			}
		}
	}
	return events
}
function setWeekStartDate(weekDate){
	var tempDate = weekDate
	tempDate = new Date(new Date(tempDate).setHours(0,0,0,0))
	return tempDate;
}
function setWeekEndDate(weekDate){
	var tempDate = weekDate
	tempDate = new Date(new Date(new Date(tempDate).setDate(new Date(tempDate).getDate() + 7)).setHours(23,59,59,999))
	return tempDate
}
function setConstants(){
	window.ACTIVITY_DAYS_COUNT = 5
	window.ACTIVITY_DIFFERENCE_IN_MILLISECONDS = 432000000
}
function initializeUserMessages(){
	window.userMessages['ACTIVITY_DELETE_SUCCESS'] = "Acivity deleted successfully"
	window.userMessages['ACTIVITY_DELETE_FAILURE'] = "Something went wrong. Not deleted activity"
	window.userMessages['ACTIVITY_LIKE_UNLIKE_FAILURE'] = "Your like is not saved"
	window.userMessages['ACTIVITY_COMPLETE_UNCOMPLETE_FAILURE'] = "Your completion mark is not saved"
	window.userMessages['DELETE_ATTACHMENT'] = "Are you sure you want to delete this?"
	window.userMessages['ACTIVITY_DELETE'] = "Are you sure you want to delete this?"
	window.userMessages['DELETE_POLL'] = "Are you sure you want to delete this?"
	window.userMessages['ACTIVITY_UPDATED'] = "Activity updated successfully"
	window.userMessages['ACTIVITY_CREATED'] = "Activity created successfully"
	window.userMessages['LOGOUT_CONFIRM'] = "Are you sure you want to logout?"
	window.userMessages['USER_ONLINE'] = "You are connected"
	window.userMessages['USER_OFFLINE'] = "The internet is disconnected on your device"
	window.userMessages['ONLY_IMAGES_ALLOWED'] = "The internet is disconnected on your device"
	window.userMessages['ONLY_VIDEOS_ALLOWED'] = "Only videos can be uploaded"
	window.userMessages['FILE_UPLOAD_LIMIT_EXCEED'] = "File upload limit cannot exceed 30MB in size"
	window.userMessages['MOBILE_NOT_REGISTERED'] = "Your Mobile number is not registered, please contact the institute admin"
	window.userMessages['PASSWORD_NOT_MATCHED'] = "Your password did not match, please verify and re-enter"
	window.userMessages['VIDEO_MODE_NOT_AVAILABLE'] = "Video mode not available"
	window.userMessages['DENIED_CORDOVA_STORAGE_PERMISSIONS'] = "You have denied permission to download attachments. To download please enable it in Settings->Apps->Wyze->Permissions->Storage"
	window.userMessages['END_DATE_SHORTER'] = "End date cannot be earlier than start date"
	window.userMessages['NO_APPLICATION_FILE_OPEN'] = "No application found to handle open file"
	window.userMessages['ATTACHMENT_UPLOAD_FAILED'] = "Activity created but attachment were not uploaded. Please try again later"
	window.userMessages['INCOMPATIBLE_FILETYPE'] = "File type incompatible with Wyze. Please try from another device."
}