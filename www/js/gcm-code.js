window.addEventListener('load', function() {  

  // Check that service workers are supported, if so, progressively  
  // enhance and add push messaging support, otherwise continue without it.  
  if ('serviceWorker' in navigator) {  
    navigator.serviceWorker.register('service-worker.js')  
    .then(initialiseState);  
  } else {  
    console.warn('Service workers aren\'t supported in this browser.');  
  };
  //subscribe();
});

//Once the service worker is registered set the initial state  
function initialiseState() {  
  // Are Notifications supported in the service worker?  
  if (!('showNotification' in ServiceWorkerRegistration.prototype)) {  
    console.warn('Notifications aren\'t supported.');  
    return;  
  }

  // Check the current Notification permission.  
  // If its denied, it's a permanent block until the  
  // user changes the permission  
  if (Notification.permission === 'denied') {  
    console.warn('The user has blocked notifications.');  
    return;  
  }

  // Check if push messaging is supported  
  if (!('PushManager' in window)) {  
    console.warn('Push messaging isn\'t supported.');  
    return;  
  }

  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {  
    serviceWorkerRegistration.pushManager.getSubscription()  
      .then(function(subscription) {  
        if (!subscription) { 
        	clearGCMSubscription()
    		subscribe();
          return;  
        }
        var endpoint_url = "https://android.googleapis.com/gcm/send";
		var subscription_id = subscription.endpoint.substr(endpoint_url.length+1,subscription.endpoint.length);
		globalSubscription = subscription_id;
        window.isPushEnabled = true;
        var gcmKey = localStorage.getItem("GenieApp.gcmKey");
		var gcmEnabled = localStorage.getItem("GenieApp.gcmEnabled");
        if(!checkStringItemNull(gcmKey) && !checkStringItemNull(gcmEnabled)){
        	clearGCMSubscription()
    		subscribe();
        }
        if(window.isPushEnabled==true && gcmKey!=subscription_id){
        	gcmKey = ""
        	localStorage.setItem("GenieApp.gcmKey", "");
        	return sendSubscriptionToServer(subscription);
        }	
      })["catch"](function(err) {  
        console.warn('Error during getSubscription()', err);  
      });  
  });  
}
function clearGCMSubscription(){
	localStorage.setItem("GenieApp.gcmKey", "");
	localStorage.setItem("GenieApp.gcmEnabled", false);
}
function subscribe() {
	  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) {  
	    serviceWorkerRegistration.pushManager.subscribe({userVisibleOnly: true})  
	      .then(function(subscription) {    
	        window.isPushEnabled = true;
	        var endpoint_url = "https://android.googleapis.com/gcm/send";
			var subscription_id = subscription.endpoint.substr(endpoint_url.length+1,subscription.endpoint.length);
			globalSubscription = subscription_id;
			window.gcmRequestLock = false;
	        return sendSubscriptionToServer(subscription);
	      })["catch"](function(e) {  
	        if (Notification.permission === 'denied') { 
	          console.warn('Permission for Notifications was denied');
	        } else {  
	          console.error('Unable to subscribe to push.', e); 
	        }
	        clearGCMSubscription();
	      });  
	  });  
	}

function unsubscribe() {  
	  navigator.serviceWorker.ready.then(function(serviceWorkerRegistration) { 
	    serviceWorkerRegistration.pushManager.getSubscription().then(  
	      function(pushSubscription) {  
	        // Check we have a subscription to unsubscribe  
	        if (!pushSubscription) {
	          window.isPushEnabled = false;  
	          return;  
	        }
	        pushSubscription.unsubscribe().then(function(successful) { 
	          window.isPushEnabled = false;  
	        })["catch"](function(e) { 
	          console.log('Unsubscription error: ', e);
	        });  
	      })["catch"](function(e) {  
	        console.error('Error thrown while unsubscribing from push messaging.', e);  
	      });  
	  });  
	}

function sendSubscriptionToServer(serviceWorkerRegistration){
		var user = getUser();
		if(!checkStringItemNull(user)){
			return true
		}
		var gcmKey = localStorage.getItem("GenieApp.gcmKey");
		var gcmEnabled = localStorage.getItem("GenieApp.gcmEnabled");
		if((window.isPushEnabled==false || user.loggedIn==false || gcmEnabled=="true" ) && gcmKey!=""){
			return true;
		}
		if(globalSubscription==""){
			subscribe();
			return true;
		}
		subscription = globalSubscription;
		var appspotsite = getGapiURL();
		if(window.gcmRequestLock==false){
			window.gcmRequestLock = true;
		}else{
			return true;
		}

		if(!checkStringItemNull(user.userType)){
    		return true;
    	}

    	var userData = {};
    	userData.mobileNumber = user.userName;
    	userData.genieId = user.genieId;
    	userData.gcmKey = subscription
    	userData.userType = user.userType;
    	userData.deviceType = "desktop";
    	if(!checkStringItemNull(gcmKey)){
            gcmKey = ""
        }
        userData.previousGCMKey = gcmKey;
		if(gapi.client.commonutil===undefined){
			gapi.client.load('commonutil', 'v1', function() {
				registerToGCM(userData, user)
			},appspotsite + '/_ah/api')
		}else{
			registerToGCM(userData, user)
		}
}

function registerToGCM(userObject, user, environment){
	if(environment===undefined || environment==null){
		gcmKey = localStorage.getItem("GenieApp.gcmKey");
		gcmEnabled = localStorage.getItem("GenieApp.gcmEnabled");
		if((window.isPushEnabled==false || user.loggedIn==false || gcmEnabled=="true" || window.gcmRequestLock==false) && gcmKey!=""){
			return true;
		}
	}
	gapi.client.commonutil.storeUserGCMKeys(userObject).execute(function(resp) {
		if (!resp.code){
		   	localStorage.setItem("GenieApp.gcmKey", subscription);
		   	localStorage.setItem("GenieApp.gcmEnabled", true);
		}else{
			console.log("Show some error to user")
		}
		window.gcmRequestLock = false;
    })
}